package cn.com.pingyee.sketch.sample.core.domain.jpa;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

/**
 * @author Yi Ping
 * @version 1.0
 * @date 2020/8/3 22:19
 */
@Getter
@Setter
@Entity
@Table(name = "t_person")
public class PersonPO  extends Person{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Override
    public Long getId() {
        return super.getId();
    }

    @Override
    public String getName() {
        return super.getName();
    }

    @Override
    public Long getCreateUser() {
        return super.getCreateUser();
    }

    @Override
    public Date getCreateDate() {
        return super.getCreateDate();
    }

    @Override
    public Long getLastModifiedUser() {
        return super.getLastModifiedUser();
    }

    @Override
    public Date getLastModifiedDate() {
        return super.getLastModifiedDate();
    }
}
