package cn.com.pingyee.sketch.sample.core.domain.jpa;

import cn.com.pingyee.sketch.core.domain.jpa.JpaCrudRepository;

/**
 * @author Yi Ping
 * @version 1.0
 * @date 2020/8/3 22:22
 */
public interface PersonRepository  extends JpaCrudRepository<PersonPO,Long> {
}
