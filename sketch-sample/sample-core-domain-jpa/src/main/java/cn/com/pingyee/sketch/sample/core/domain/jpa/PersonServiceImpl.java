package cn.com.pingyee.sketch.sample.core.domain.jpa;

import cn.com.pingyee.sketch.core.domain.jpa.AbstractJpaCrudService;
import cn.com.pingyee.sketch.core.domain.jpa.JpaCrudRepository;
import org.springframework.stereotype.Service;

/**
 * @author Yi Ping
 * @version 1.0
 * @date 2020/8/3 22:23
 */

@Service
public class PersonServiceImpl
    extends AbstractJpaCrudService<PersonPO, Person,Long,Person,
        Person, Person
        >
    implements PersonService
{
    public PersonServiceImpl(JpaCrudRepository<PersonPO, Long> jpaCrudRepository) {
        super(jpaCrudRepository);
    }
}
