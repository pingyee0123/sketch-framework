package cn.com.pingyee.sketch.sample.core.domain.jpa;

import cn.com.pingyee.sketch.core.domain.CrudService;

/**
 * @author Yi Ping
 * @version 1.0
 * @date 2020/8/3 22:12
 */
public interface PersonService extends CrudService<Person,
        Long,Person,Person,Person> {
}
