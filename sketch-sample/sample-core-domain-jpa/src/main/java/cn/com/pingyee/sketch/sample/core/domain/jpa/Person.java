package cn.com.pingyee.sketch.sample.core.domain.jpa;

import cn.com.pingyee.sketch.core.domain.AbstractEntity;
import cn.com.pingyee.sketch.core.domain.jpa.JpaQuery;
import com.fasterxml.jackson.databind.node.POJONode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.function.Function;

/**
 * @author Yi Ping
 * @version 1.0
 * @date 2020/8/3 22:10
 */

@Getter
@Setter
public class Person extends AbstractEntity implements
        JpaQuery<PersonPO>, Function<Person, PersonPO>
{
    private String name;

    @Override
    public long getPage() {
        return 0;
    }

    @Override
    public long getSize() {
        return 10;
    }

    @Override
    public String getSort() {
        return null;
    }

    @Override
    public boolean isSearchCount() {
        return false;
    }

    @Override
    public PersonPO apply(Person person) {
        return (PersonPO) person;
    }


    @Override
    public Predicate toPredicate(Root<PersonPO> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
        return null;
    }
}
