package cn.com.pingyee.sketch.sample.core.domain.jpa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SampleCoreDomainJpaApplication {

    public static void main(String[] args) {
        SpringApplication.run(SampleCoreDomainJpaApplication.class, args);
    }

}
