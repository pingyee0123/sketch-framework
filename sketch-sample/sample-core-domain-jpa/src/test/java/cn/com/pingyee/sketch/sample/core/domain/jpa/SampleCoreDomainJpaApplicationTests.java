package cn.com.pingyee.sketch.sample.core.domain.jpa;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@ActiveProfiles("dev")
@SpringBootTest
class SampleCoreDomainJpaApplicationTests {

    @Test
    void contextLoads() {
    }


    @Autowired
    PersonService personService;

    @Test
    public void test1(){
        Person p = new PersonPO();
        p.setName("zhangsan");
        personService.save(p);
    }
}
