package cn.com.pingyee.sketch.sample.ldap;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SketchSampleLdapApplication {

    public static void main(String[] args) {
        SpringApplication.run(SketchSampleLdapApplication.class, args);
    }

}
