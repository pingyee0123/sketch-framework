package cn.com.pingyee.sketch.sample.web2.test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;

/**
 * @author Yi Ping
 * @version 1.0
 * @date 2020/4/6 23:56
 */

//@Slf4j
public class UnsafePublish {
    private static final Logger log = LoggerFactory.getLogger(UnsafePublish.class);

    private String[] stats = {"a", "b","c"};
    public String[] getStates(){
        return  stats;
    }

    public static void main(String[] args){
        UnsafePublish publish = new UnsafePublish();
        log.info("{}", Arrays.toString(publish.getStates()));

        publish.getStates()[0] = "d";
        log.info("{}", Arrays.toString(publish.getStates()));
    }
}
