package cn.com.pingyee.sketch.sample.apiversion;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Yi Ping
 * @version 1.2
 * @date 2020/3/25 17:04
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ApiVersionApiResponse /*implements ApiResponse*/ {
    private String version;
}
