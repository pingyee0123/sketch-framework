package cn.com.pingyee.sketch.sample.apiversion;

import cn.com.pingyee.sketch.apiversion.starter.ApiVersion;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Yi Ping
 * @version 1.2
 * @date 2020/3/25 17:03
 */

@ApiVersion(1.0f)
@RestController
@RequestMapping("/test/versions")
public class ApiVersionController {

    @GetMapping
    public ApiVersionApiResponse getVersionDefault(){
        return  new ApiVersionApiResponse("1.0");
    }

    @GetMapping(/*params = "v=1.1.0"*/)
    @ApiVersion(value = 1.1f)
//    @ApiOperation(value = "get versions", tags = "1.1")
    public ApiVersionApiResponse getVersion110(){
        return  new ApiVersionApiResponse("1.1");
    }

    @GetMapping(/*params = "v=1.2.0"*/)
    @ApiVersion(1.2f)
//    @ApiOperation(value = "get versions 2 ", tags = "1.2")
    public ApiVersionApiResponse getVersion120(){
        return  new ApiVersionApiResponse("1.2");
    }
}
