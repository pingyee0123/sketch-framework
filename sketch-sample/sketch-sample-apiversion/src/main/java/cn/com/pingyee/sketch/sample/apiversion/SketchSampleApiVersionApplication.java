package cn.com.pingyee.sketch.sample.apiversion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SketchSampleApiVersionApplication {

    public static void main(String[] args) {
        SpringApplication.run(SketchSampleApiVersionApplication.class, args);
    }

}
