package cn.com.pingyee.sketch.sample.apiversion;

import org.junit.Test;
//import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
class SketchSampleApiVersionApplicationTests {

    @Test
    public void contextLoads() {
    }

}
