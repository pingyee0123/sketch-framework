package cn.com.pingyee.sketch.hello.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author Yi Ping
 * @version 1.0
 * @date 2020/5/3 9:06
 */


@Retention(RetentionPolicy.SOURCE)
@Target(ElementType.TYPE)
public @interface Hello {


}
