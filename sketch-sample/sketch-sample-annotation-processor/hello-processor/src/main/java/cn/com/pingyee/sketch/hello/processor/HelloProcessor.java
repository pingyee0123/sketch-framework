package cn.com.pingyee.sketch.hello.processor;

import cn.com.pingyee.sketch.hello.annotation.Hello;
import com.google.auto.service.AutoService;

import javax.annotation.processing.*;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import javax.tools.Diagnostic;
import java.io.IOException;
import java.io.Writer;
import java.util.Set;

/**
 * @author Yi Ping
 * @version 1.0
 * @date 2020/5/3 9:07
 */


@SupportedSourceVersion(SourceVersion.RELEASE_8)
@SupportedAnnotationTypes("cn.com.pingyee.sketch.hello.annotation.Hello")
@SupportedOptions("debug")
@AutoService(Processor.class)
public class HelloProcessor extends AbstractProcessor {
    private static final String HELLO_TEMPLATE =
            "package %1$s;\n\npublic class %2$sHello {\n  public static void sayHello() {\n    System.out.println(\"Hello %3$s\");\n  }\n}\n";

    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
        System.out.println("start process Hello" );
        System.out.println("annoation was " + annotations);
//        System.out.println("roundEnv was " + JSON.toJSONString(roundEnv));
        System.out.println("roundevn processingOver " + roundEnv.processingOver());
        System.out.println("eoundenv root elements " + roundEnv.getRootElements());
        System.out.println("roundenv annaotated hello elements " + roundEnv.getElementsAnnotatedWith(Hello.class));
        System.out.println("");
        for (Element element : roundEnv.getElementsAnnotatedWith(Hello.class)) {
            TypeElement typeElem = (TypeElement) element;
            String typeName = typeElem.getQualifiedName().toString();
            Filer filer = processingEnv.getFiler();
            try (Writer sw = filer.createSourceFile(typeName + "Hello").openWriter()) {
                log("Generating " + typeName + "Hello source code");
                int lastIndex = typeName.lastIndexOf('.');
                sw.write(String.format(HELLO_TEMPLATE, typeName.substring(0, lastIndex), typeName.substring(lastIndex + 1), typeName));
            } catch (IOException e) {
                processingEnv.getMessager().printMessage(Diagnostic.Kind.ERROR, e.getMessage());
            }
        }

        return true;
    }

    private void log(String msg) {
        if (processingEnv.getOptions().containsKey("debug")) {
            processingEnv.getMessager().printMessage(Diagnostic.Kind.NOTE, msg);
        }
    }
}
