package cn.com.pingyee.sketch.hello.test;

import cn.com.pingyee.sketch.hello.annotation.Hello;

/**
 * @author Yi Ping
 * @version 1.0
 * @date 2020/5/3 9:29
 */
@Hello
public class Main {

    public static  void main(String[] args){
        MainHello.sayHello();
    }
}
