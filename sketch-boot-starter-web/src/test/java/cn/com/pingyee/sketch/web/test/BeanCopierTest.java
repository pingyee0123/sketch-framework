package cn.com.pingyee.sketch.web.test;

import cn.com.pingyee.sketch.boot.web.wechat.BeanCopierUtils;
import cn.com.pingyee.sketch.core.util.ReflectUtils;
import lombok.Data;
import org.junit.Test;
import org.springframework.beans.BeanUtils;
import org.springframework.cglib.beans.BeanCopier;

import java.lang.reflect.InvocationTargetException;

/**
 * @author: Yi Ping
 * @date: 2019/11/18 0018 17:27
 * @since: 1.2.0
 */

public class BeanCopierTest  extends ExecuteTimeTest{


    private long count = 9990090L;


    @Test
    public void testHashCode(){
        System.out.println(Person.class.hashCode());
    }

    @Test
    public  void testBeanCopier(){
        BeanCopier beanCopier = BeanCopier.create(Person.class, PersonDTO.class,false);
        for(int i=0; i<count; i++) {
//            PersonDTO result = new PersonDTO();
//            result.setDescription("aa" + i);
            beanCopier.copy(new Person(1, "zhansn"), new PersonDTO(), null);
//            System.out.println(JSON.toJSONString(result));
        }
    }

    @Test
    public  void testBeanCopierUsingCacheUtils(){

            BeanCopier beanCopier = BeanCopierUtils.getBeanCopier(Person.class, PersonDTO.class,false);
        for(int i=0; i<count; i++) {
//            PersonDTO result = new PersonDTO();

//            beanCopier.copy(new Person(1, "zhansn"), result,null);
            BeanCopierUtils.copy(new Person(1,"zhansan"), new PersonDTO(), null);
//            System.out.println(JSON.toJSONString(result));
        }
    }

    @Test
    public void testHashMapGet(){
        for(int i=0; i<count;i++){
            BeanCopierUtils.getBeanCopier(Person.class, PersonDTO.class,false);
        }
    }

    @Test
    public void testHashMapGet2(){
        String key = BeanCopierUtils.generateKey(Person.class, PersonDTO.class,false);
        for(int i=0; i<count; i++) {
            if (!BeanCopierUtils.BEAN_COPIER_MAP.containsKey(key)) {
                BeanCopier copier = BeanCopier.create(Person.class, Person.class, false);
                BeanCopierUtils.BEAN_COPIER_MAP.putIfAbsent(key, copier);
            }
            BeanCopierUtils.BEAN_COPIER_MAP.get(key);
        }
    }

    @Test
    public void testGenerateKey(){
        for(int i=0; i<count; i++){
            BeanCopierUtils.generateKey(Person.class, PersonDTO.class,false);
        }
    }



//    @Test
    public void reflectiveBeanCopyTest(){
        for(int i=0; i< count; i++){
            ReflectUtils.copyProperties(new Person(), new Person(1,"zhansan"));
        }
    }

    @Test
    public void testSpringBeanCopy(){
        for(int i=0; i<count; i++) {
            BeanUtils.copyProperties(new Person(1, "zhansan"), new PersonDTO());
        }
    }

//    @Test
    public void testApachBeanCopy() throws InvocationTargetException, IllegalAccessException {
        for(int i=0; i<count; i++) {
            org.apache.commons.beanutils.BeanUtils.copyProperties(new Person(1, "zhansan"), new PersonDTO());
        }
    }

    @Data
    public static class Person{
        private Integer id;
        private String name;
        private String description;

        public Person() {
        }

        public Person(Integer id, String name) {
            this.id = id;
            this.name = name;
        }
    }

    @Data
    public static class PersonDTO extends Person{
//        private Integer id;
//        private String name;
//        private String description;
    }
}
