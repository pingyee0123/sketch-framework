package cn.com.pingyee.sketch.web.test;

import cn.com.pingyee.sketch.boot.web.wechat.BeanCopierUtils;
import org.junit.Test;
import org.springframework.cglib.beans.BeanCopier;

/**
 * @author: Yi Ping
 * @date: 2019/11/18 0018 19:28
 * @since: 1.2.0
 */
public class BeanCopierCacheVsCreateTest extends ExecuteTimeTest {



    @Test
    public void getClassNameRepeteTest(){
        repeteTest(()-> new StringBuilder().append(BeanCopierTest.Person.class.getName()).append( BeanCopierTest.PersonDTO.class.getName()));
    }

    @Test
    public void getClassHashCodeRepeteTest(){
        repeteTest(()-> new StringBuilder().append(BeanCopierTest.Person.class.hashCode()).append( BeanCopierTest.PersonDTO.class.hashCode()));
    }

    @Test
    public void getObjectToStringRepeteTest(){
        String person = new BeanCopierTest.Person().toString();

        String personDTO = new BeanCopierTest.PersonDTO().toString();
        repeteTest(()-> {
            new StringBuilder(person).append(personDTO);
        });
    }

    @Test
    public void getObjectToStringDoNotUsingStringBuilderRepeteTest(){
        String person = new BeanCopierTest.Person().toString();

        String personDTO = new BeanCopierTest.PersonDTO().toString();
        repeteTest(()-> {
            (person + personDTO).length();
           return;
        });
    }


    @Test
    public void cacheTest(){
        BeanCopierUtils.getBeanCopier(BeanCopierTest.Person.class,BeanCopierTest.PersonDTO.class, false);
    }

    @Test
    public void createTest(){
        BeanCopier.create(BeanCopierTest.Person.class, BeanCopierTest.PersonDTO.class, false);
    }

    @Test
    public void createTestRepete(){
        repeteTest(this::createTest);
    }

    @Test
    public void generateKeyRepeteTest(){
        repeteTest(()->BeanCopierUtils.generateKey(BeanCopierTest.Person.class, BeanCopierTest.PersonDTO.class, false));
    }

    @Test
    public void  cacheTestRepete(){
        String key = BeanCopierUtils.generateKey(BeanCopierTest.Person.class, BeanCopierTest.PersonDTO.class, false);
        BeanCopierUtils.getBeanCopier(BeanCopierTest.Person.class,BeanCopierTest.PersonDTO.class, false);
        repeteTest(()-> BeanCopierUtils.BEAN_COPIER_MAP.get(key));
    }

}
