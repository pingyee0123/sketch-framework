package cn.com.pingyee.sketch.web.test;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.springframework.util.AntPathMatcher;

/**
 * @author Yi Ping
 * @version 2.1
 * @description
 * @date 2020/12/19 13:25
 */

@Slf4j
public class AntPathMatcherTest {

    @Test
    public void test1(){
        AntPathMatcher matcher = new AntPathMatcher();
        boolean match = matcher.match("/api/{id}", "/api/test");
        log.info("{} match {}, {}", match?"":"Not", "/api/*", "/api/test");
    }
}
