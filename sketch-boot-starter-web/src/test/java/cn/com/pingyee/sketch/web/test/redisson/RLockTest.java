package cn.com.pingyee.sketch.web.test.redisson;

import lombok.extern.slf4j.Slf4j;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.redisson.Redisson;
import org.redisson.api.RLock;
import org.redisson.config.Config;

import java.util.concurrent.TimeUnit;

@Slf4j
public class RLockTest {

    private  Redisson redisson;

//    @Before
    public void setup(){
        Config config = new Config();
        config.useSingleServer().setAddress("redis://baiduyun.pingyee.com.cn:6379");
        config.useSingleServer().setPassword("Xia510987");
        this.redisson = (Redisson) Redisson.create(config);
    }

    @Test
    public void zhanwei(){

    }

    // todo
//    @Test
    public void reentrantLockTest() throws InterruptedException {
        RLock lock = redisson.getLock("anyLock");
//        redisson.set
        if(log.isInfoEnabled()){
            log.info("{} lock name was {}", "reentrant", "anyLock");
        }
        lock.lock();

        TimeUnit.SECONDS.sleep(10);
    }

//    @After
    public void close(){
        if(log.isInfoEnabled()){
            log.info("==== shutdown redis connection ====");
        }
        redisson.shutdown();
    }
}
