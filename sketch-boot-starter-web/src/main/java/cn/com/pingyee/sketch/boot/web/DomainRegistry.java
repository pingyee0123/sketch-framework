package cn.com.pingyee.sketch.boot.web;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author: Yi Ping
 * @date: 2019/5/21 0021 16:55
 * @since: 0.0.1-SNAPSHOT
 */


public class DomainRegistry implements ApplicationContextAware {
    private static ApplicationContext applicationContext;

    public DomainRegistry() {
    }

    private static void assertDomainRegistryInIOC(){
        if(null == applicationContext){
            throw  new RuntimeException("DomainRegistry not  in the ioc" );
        }
    }

    public static <T> T bean(Class<T> clazz) {
        assertDomainRegistryInIOC();
        Map beans = applicationContext.getBeansOfType(clazz);
        T t = (T) beans.values().stream().findFirst().orElse(null);
        if (t == null) {
            throw new RuntimeException("bean.not.exist");
        } else {
            return t;
        }
    }

    public static <T> T service(Class<T> clazz) {
        return bean(clazz);
    }

    public static <T> T repo(Class<T> clazz) {
        return bean(clazz);
    }

    public static <T> List<T> allBeans(Class<T> clazz) {
        assertDomainRegistryInIOC();
        Map<String, T> beans = applicationContext.getBeansOfType(clazz);
        List<T> t = beans.values().stream().collect(Collectors.toList());
        if (t == null) {
            throw new RuntimeException("bean.not.exist");
        } else {
            return t;
        }
    }

    public static <T> Map<String, T> beanMap(Class<T> clazz) {
        assertDomainRegistryInIOC();
        Map<String, T> beans = applicationContext.getBeansOfType(clazz);
        if (beans == null) {
            throw new RuntimeException("bean.not.exist");
        } else {
            return beans;
        }
    }

    /**
     * <p> 发送事件 </p>
     * @param event
     */
    public static void publishEvent(Object event){
        applicationContext.publishEvent(event);
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        if (DomainRegistry.applicationContext == null) {
            DomainRegistry.applicationContext = applicationContext;
        }

    }
}
