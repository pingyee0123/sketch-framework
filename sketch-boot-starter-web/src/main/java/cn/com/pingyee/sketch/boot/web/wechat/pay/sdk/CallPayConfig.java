package cn.com.pingyee.sketch.boot.web.wechat.pay.sdk;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author: YiPing
 * @create: 2018-09-06 14:51
 * @since: 1.0
 */
@Data
public class CallPayConfig {
    @ApiModelProperty(value = "微信开放平台审核通过的应用APPID", required = true)
    private  String appid;
    @ApiModelProperty(value = "微信支付分配的商户号, eg:1900000109", required = true)
    private String partnerid;
    @ApiModelProperty(value = "微信返回的支付交易会话ID, eg:WX1217752501201407033233368018", required = true)
    private String prepayid;
    @ApiModelProperty(value = "扩展字段 package,由于 package与JAVA关键字冲突,所以取名package_， 请前端调用支付接口时用package 替代package_。 eg:Sign=WXPay\t暂填写固定值Sign=WXPay")
    private String package_;
    @ApiModelProperty(value = "随机字符串，不长于32位", required = true)
    private String nonceStr;
    @ApiModelProperty(value = "时间戳, m", required = true)
    private  String timeStamp;
    @ApiModelProperty(value = "签名", required = true)
    private String sign;

    public CallPayConfig(String appid, String partnerid, String prepayid, WXPayConstants.SignType signType) {
        this.appid = appid;
        this.partnerid = partnerid;
        this.prepayid = prepayid;
        this.package_ = "Sign=WXPay";
        this.nonceStr = WXPayUtil.generateNonceStr();
        this.timeStamp = System.currentTimeMillis() /1000 + "";

    }
}
