package cn.com.pingyee.sketch.boot.web.file;

import cn.com.pingyee.sketch.core.service.FileService;
import cn.com.pingyee.sketch.core.service.LocalFileService;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author: Yi Ping
 * @date: 2019/11/12 0012 14:15
 * @since: 1.2.0
 */
@EnableConfigurationProperties(LocalFileProperty.class)
public class LocalFileConfigurerSupport {

    @Bean
    public LocalFileService localFileService(LocalFileProperty localFileProperty){
        return new LocalFileService(localFileProperty.getSavePath(), localFileProperty.getUrlPrefix());
    }

    @Bean
    public WebMvcConfigurer localFileWebMvcConfigurer(LocalFileProperty localFileProperty){
        return new WebMvcConfigurer() {
            @Override
            public void addResourceHandlers(ResourceHandlerRegistry registry) {
                registry.addResourceHandler("/" + localFileProperty.getUrlPrefix() +"**")
                        .addResourceLocations("file:///" + localFileProperty.getSavePath());
            }
        };
    }

    @Bean
    public FileController fileController(FileService fileService){
        return new FileController(fileService);
    }
}
