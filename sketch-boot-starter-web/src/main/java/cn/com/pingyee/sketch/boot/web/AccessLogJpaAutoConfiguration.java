package cn.com.pingyee.sketch.boot.web;

import cn.com.pingyee.ddd.log.api.AccessLogService;
import cn.com.pingyee.ddd.log.jpa.AccessLogRepository;
import cn.com.pingyee.ddd.log.jpa.AccessLogServiceImpl;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * @author Yi Ping
 * @version 2.1
 * @date 2020/9/24 10:49
 */

@Configuration
@ConditionalOnClass(cn.com.pingyee.ddd.log.jpa.AccessLogServiceImpl.class)
@EnableJpaRepositories(basePackages = "cn.com.pingyee.ddd.log.jpa")
//@ComponentScan(basePackages = "cn.com.pingyee.ddd.dict.jpa" )
@EntityScan(basePackages = "cn.com.pingyee.ddd.log")
//@EnableJpaAuditing
@AutoConfigureBefore(AccessLogAutoConfiguration.class)
public class AccessLogJpaAutoConfiguration {

    @Bean
    public AccessLogService accessLogService(AccessLogRepository accessLogRepository){
        return  new AccessLogServiceImpl(accessLogRepository);
    }

}
