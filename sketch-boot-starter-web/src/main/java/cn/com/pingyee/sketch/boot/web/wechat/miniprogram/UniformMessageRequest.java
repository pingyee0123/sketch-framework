package cn.com.pingyee.sketch.boot.web.wechat.miniprogram;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.HashMap;
import java.util.Map;

/**
 * @author: Yi Ping
 * @create: 2019-01-24 14:04
 * @since: 1.2.0
 */

@Data
public class UniformMessageRequest {

    @ApiModelProperty(value = "用户openid，可以是小程序的openid，也可以是mp_template_msg.appid对应的公众号的openid", required = true)
    private String touser;
    @ApiModelProperty(value = "小程序模板消息相关的信息，可以参考小程序模板消息接口; 有此节点则优先发送小程序模板消息")
    private WeappTemplateMsg weapp_template_msg;
    @ApiModelProperty(value = "公众号模板消息相关的信息，可以参考公众号模板消息接口；有此节点并且没有weapp_template_msg节点时，发送公众号模板消息", required = true)
    private MpTemplateMsg mp_template_msg;


    public UniformMessageRequest(String touser) {
        this.touser = touser;
    }

    @Data
    public static class WeappTemplateMsg{
        @ApiModelProperty(value = "所需下发的模板消息的id", required = true)
        private String template_id;
        @ApiModelProperty(value = "点击模板卡片后的跳转页面，仅限本小程序内的页面。支持带参数,（示例index?foo=bar）。该字段不填则模板无跳转。", required = true)
        private String page;
        @ApiModelProperty(value = "表单提交场景下，为 submit 事件带上的 formId；支付场景下，为本次支付的 prepay_id", required = true)
        private String form_id;
        @ApiModelProperty(value = "模板内容，不填则下发空模板。具体格式请参考示例。", required = true)
        private Map data = new HashMap();
        @ApiModelProperty(value = "模板需要放大的关键词，不填则默认无放大", required = true)
        private String emphasis_keyword;
    }

    @Data
    public static class MpTemplateMsg  {
        @ApiModelProperty(value = "公众号appid，要求与小程序有绑定且同主体", required = true)
        private String appid;
        @ApiModelProperty(value = "公众号模板id", required = true)
        private String template_id;
        @ApiModelProperty(value = "公众号模板消息所要跳转的url", required = true)
        private String url;
        @ApiModelProperty(value = "公众号模板消息所要跳转的小程序，小程序的必须与公众号具有绑定关系", required = true)
        private MiniProgram miniprogram;
        @ApiModelProperty(value = "公众号模板消息的数据", required = true)
        private Map data = new HashMap<>();

        public MpTemplateMsg(String appid, String template_id) {
            this.appid = appid;
            this.template_id = template_id;
        }

        public MpTemplateMsg  addTemplateValue(String key, String value){
            this.data.put(key, new TemplateValue(value));
            return this;
        }

        public MpTemplateMsg addTemplateValue(String key, String value, String color){
            this.data.put(key, new TemplateValue(value, color));
            return this;
        }

    }

    @Data
    public static class MiniProgram{
        @ApiModelProperty(value = "所需跳转到的小程序appid（该小程序appid必须与发模板消息的公众号是绑定关联关系，暂不支持小游戏）", required = true)
        private String appid;
        @ApiModelProperty(value = "所需跳转到小程序的具体页面路径，支持带参数,（示例index?foo=bar），暂不支持小游戏")
        private String pagepath;

        public MiniProgram(String appid) {
            this.appid = appid;
        }

        public MiniProgram(String appid, String pagepath) {
            this.appid = appid;
            this.pagepath = pagepath;
        }
    }

    /**
     * @author: Yi Ping
     * @create: 2019-01-24 15:33
     * @since: 1.2.0
     */
    @Data
    public static class TemplateValue {

        private String value;
        private String color;

        public TemplateValue(String value, String color) {
            this.value = value;
            this.color = color;
        }

        public TemplateValue(String value) {
            this.value = value;
        }
    }
}
