package cn.com.pingyee.sketch.boot.web.log;

import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * @author: Yi Ping
 * @date: 2019/9/21 0021 0:38
 * @since: 1.0.0
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
@Documented
@Import({AccessLogConfigureSupport.class})
public @interface EnableAccessLog {
}
