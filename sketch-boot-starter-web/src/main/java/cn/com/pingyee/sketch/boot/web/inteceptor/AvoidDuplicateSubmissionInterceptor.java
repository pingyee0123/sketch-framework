package cn.com.pingyee.sketch.boot.web.inteceptor;

import org.apache.commons.lang3.time.DateUtils;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;
import java.util.Date;

/**
 *
 * @author YiPing
 * @date 2019.11.11
 * @since 1.2
 *
 */

public class AvoidDuplicateSubmissionInterceptor implements HandlerInterceptor {
    private static String ATTR_PRE_AVOID_DUPLICATE_SUBMISSION = "AVOID_DUPLICATE_SUBMISSION_";

    // todo 可以配置防重间隔
    private int avoidDuplicateSubmissionIntervalInMilli;

    public AvoidDuplicateSubmissionInterceptor() {
        this(2000);
    }

    public AvoidDuplicateSubmissionInterceptor(int avoidDuplicateSubmissionIntervalInMilli) {
        this.avoidDuplicateSubmissionIntervalInMilli = avoidDuplicateSubmissionIntervalInMilli;
    }

    private String getAvoidDuplicateSubmissionAttributeName(HttpServletRequest request){
        return ATTR_PRE_AVOID_DUPLICATE_SUBMISSION + request.getRequestURI();
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        if(handler instanceof HandlerMethod){
            AvoidDuplicateSubmission submissionAnnotation = ((HandlerMethod) handler).getMethodAnnotation(AvoidDuplicateSubmission.class);
            if(null != submissionAnnotation){
                Date expiredDate = (Date) request.getSession().getAttribute(getAvoidDuplicateSubmissionAttributeName(request));
                submissionAnnotation.value();
                if(null == expiredDate /** 为第一次请求 **/ || new Date().after(expiredDate) /** 前一次请求已过期 **/){
                    setOrRefreshExpiredDate(request, getInterval(submissionAnnotation, avoidDuplicateSubmissionIntervalInMilli));
                }else {
                    setOrRefreshExpiredDate(request, getInterval(submissionAnnotation, avoidDuplicateSubmissionIntervalInMilli));
                    throw new DuplicateSubmissionException();
                }
            }
        }
        return true;

    }

    private int getInterval(AvoidDuplicateSubmission avoidDuplicateSubmissionAnnotation, int defaultIntervalValue){
        return avoidDuplicateSubmissionAnnotation.value()<=0?defaultIntervalValue:avoidDuplicateSubmissionAnnotation.value();
    }

    private    void setOrRefreshExpiredDate(HttpServletRequest request, int expiredIn) {
        request.getSession().setAttribute(getAvoidDuplicateSubmissionAttributeName(request), DateUtils.addMilliseconds(new Date(), expiredIn));
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {

    }
}
