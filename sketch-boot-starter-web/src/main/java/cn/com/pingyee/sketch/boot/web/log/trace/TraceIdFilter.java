package cn.com.pingyee.sketch.boot.web.log.trace;

import org.apache.commons.lang3.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static cn.com.pingyee.sketch.boot.web.log.AccessLog.TRACE_ID_ATTRIBUTE_NAME;
import static cn.com.pingyee.sketch.boot.web.log.AccessLog.TRACE_ID_HEADER;


/**
 * @author: Yi Ping
 * @date: 2019/6/12 0012 14:22
 * @since: 1.4.36
 */
public class TraceIdFilter extends OncePerRequestFilter {

    private boolean autoGenerateTraceId;
    private TraceIdGenerator traceIdGenerator;

    public TraceIdFilter(boolean autoGenerateTraceId, TraceIdGenerator traceIdGenerator) {
        this.autoGenerateTraceId = autoGenerateTraceId;
        this.traceIdGenerator = traceIdGenerator;
    }

    public TraceIdFilter() {
        this(true, new TraceIdGenerator.DefaultTraceIdGenerator());
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        String traceId = getTraceId(request);
        if(StringUtils.isBlank(traceId) && this.autoGenerateTraceId){
            traceId = traceIdGenerator.generate(request);
        }
        request.setAttribute(TRACE_ID_ATTRIBUTE_NAME, traceId);
        filterChain.doFilter(request, response);
    }


    protected String getTraceId(HttpServletRequest request){
        return request.getHeader(TRACE_ID_HEADER);
    }


}
