package cn.com.pingyee.sketch.boot.web.log;


import cn.com.pingyee.sketch.excel.core.annotation.ExcelColumn;
import cn.com.pingyee.sketch.excel.core.annotation.ExcelTitle;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * @author: Yi Ping
 * @create: 2019-03-07 18:12
 * @since: 1.4.3
 */

@Data
@ExcelTitle("日志")
public class AccessLog {

    public static String TRACE_ID_HEADER = "Trace-Id";
    public static String TRACE_ID_ATTRIBUTE_NAME = AccessLog.class.getName() + ".TRACE_ID";

    @ExcelColumn(name = "工程编号")
    @ApiModelProperty("工程编号")
    private int projectCode;

    @ExcelColumn(name = "工程名")
    @ApiModelProperty("工程名")
    private String projectName;

    @ExcelColumn(name = "会话Id")
    @ApiModelProperty("sessionId")
    private String sessionId;

    @ExcelColumn(name = "访问的用户类型")
    @ApiModelProperty("访问的用户类型")
    private String accessUserType;

    @ExcelColumn(name = "访问用户")
    @ApiModelProperty("当前请求的用户")
    private String accessUser;

    @ExcelColumn(name = "请求方法")
    @ApiModelProperty("请求方法， GET, POST...")
    private String method;

    @ExcelColumn(name = "请求全路径")
    @ApiModelProperty("请求全路径")
    private String path;

    @ExcelColumn(name = "请求uri")
    @ApiModelProperty("请求uri")
    private String uri;

    @ExcelColumn(name = "请求来源Ip")
    @ApiModelProperty("请求来源Ip")
    private String ip;

    @ExcelColumn(name = "请求时间")
    @ApiModelProperty("请求时间")
    private Date accessTime;

    @ExcelColumn(name = "浏览器")
    @ApiModelProperty("请求浏览器agent")
    private String agent;

    @ExcelColumn(name = "请求花费时间")
    @ApiModelProperty("请求花费时间")
    private long  requestTime;

    @ExcelColumn(name = "请求参数")
    @ApiModelProperty("请求参数")
    private String requestParameters;

    @ExcelColumn(name = "请求原始ip")
    @ApiModelProperty("请求原始ip")
    private String x_forwarded_for;

    @ExcelColumn(name = "响应状态码")
    @ApiModelProperty("响应状态码")
    private int responseStatus;

    @ExcelColumn(name = "响应内容")
    @ApiModelProperty("响应内容")
    private String responseContent;

    @ExcelColumn(name = "异常类型")
    @ApiModelProperty("异常类型")
    private String exceptionType;

    @ExcelColumn(name = "异常信息")
    @ApiModelProperty("异常信息")
    private String exception;

    @ExcelColumn(name = "日志链路Id")
    @ApiModelProperty("日志链路Id")
    private String traceId;





}
