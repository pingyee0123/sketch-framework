package cn.com.pingyee.sketch.boot.web.wechat.officialaccount;

/**
 * @author: Yi Ping
 * @date: 2019/9/12 0012 14:07
 * @since: 1.4.44
 */
public enum Scope {
    snsapi_base,
    snsapi_userinfo;
}
