package cn.com.pingyee.sketch.boot.web.log;

import cn.com.pingyee.sketch.core.response.Page;
import com.alibaba.fastjson.JSON;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.web.servlet.error.DefaultErrorAttributes;
import org.springframework.http.HttpHeaders;
import org.springframework.web.servlet.DispatcherServlet;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.Map;

/**
 * @author: Yi Ping
 * @create: 2019-03-07 18:22
 * @since: 1.4.8
 */
public interface AccessLogService {

    Logger log = LoggerFactory.getLogger(AccessLogService.class);

    /**
     * <p>保存日志， 日志持久化</p>
     * @param log
     */
    void save(AccessLog log);


    Page<AccessLog> getAccessLogs(AccessLogQuery query);
    /**
     * <p>创建日志 </p>
     * @param request
     * @param response
     * @return
     */
    default AccessLog createLog(ServletRequest request, ServletResponse response){
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse res = (HttpServletResponse ) response;

        AccessLog l = new AccessLog();
        l.setIp(StringUtils.isNotBlank(req.getRemoteAddr())?req.getRemoteAddr():req.getHeader("x-real-ip"));
        l.setX_forwarded_for(req.getHeader("x_forwarded_for"));
        l.setAccessTime(new Date());
        l.setAccessUserType(getAccessUserType());
        l.setAccessUser(getCurrentUser());
        l.setMethod(req.getMethod());
        l.setPath(req.getRequestURL().toString());
        l.setAgent(req.getHeader(HttpHeaders.USER_AGENT));
        l.setUri(req.getRequestURI());
        String sessionId = req.getRequestedSessionId();
        if(StringUtils.isBlank(sessionId)){
            sessionId = getAccessToken(req);
        }
        l.setSessionId(sessionId);
        String requestParam = /*encodeParameters(req);*/ JSON.toJSONString(request.getParameterMap());
//        l.setRequestParameters(StringUtils.substring(requestParam, 0, 512));
        try {
            l.setRequestParameters(EncryptorHelper.encrypt(requestParam));
        } catch (BadPaddingException | IllegalBlockSizeException e) {
            log.error(e.getMessage(),e);
        }

        l.setResponseStatus(res.getStatus());

        l.setProjectCode(getProjectCode());
        l.setProjectName(getProjectName());
        l.setTraceId((String)request.getAttribute(AccessLog.TRACE_ID_ATTRIBUTE_NAME));

        Exception e = (Exception) request.getAttribute(DispatcherServlet.EXCEPTION_ATTRIBUTE);
        if(null == e){
            e = (Exception) request.getAttribute(DefaultErrorAttributes.class.getName() + ".ERROR");
        }
        if(null != e){
            l.setExceptionType(e.getClass().getName());
            l.setException(ExceptionUtils.getStackTrace(e));
        }
        return l;
    }


    default String encodeParameters(HttpServletRequest request){
        String parameters = JSON.toJSONString(request.getParameterMap());
        try {
            return  EncryptorHelper.encrypt(parameters);
        } catch (Exception e) {
            return  "fail";
        }
    }

     @Deprecated
     static  String _hiddenPasswordAndGetParameters(HttpServletRequest request){
        // 非线程安全
        StringBuilder builder = new StringBuilder();
        Map<String, String[]> parameterMap = request.getParameterMap();
        if(org.apache.commons.collections4.MapUtils.isNotEmpty(parameterMap)){
            builder.append("{");
            parameterMap.forEach((k,v)->{
                // todo 不严谨, 如果程序使用其它字段做为密码， 密码同样会记录在日志中.
                if(k.equalsIgnoreCase("password") || k.equalsIgnoreCase("passwords")){

                }else {
                    builder.append("\"").append(k).append("\":");
                    if(null != v && v.length >0){
                        builder.append("[");
                        for(int i=0;i<v.length; i++){
                            builder.append("\"").append(v[i]).append("\"").append(",");
                        }
                        builder.replace(builder.lastIndexOf(","), builder.length(), "");
                        builder.append("]");
                    }else {
                        builder.append("[\"\"]");
                    }
                    builder.append(",");
                }
            });
            builder.replace(builder.lastIndexOf(","), builder.length(), "");
            builder.append("}");
        }

        return builder.toString();
     }

    /**
     * <p>获取当前访问的用户</p>
     * @return
     */
    String getCurrentUser();

    String getAccessUserType();

    /**
     * <p> 工程编码</p>
     * @return
     */
    int getProjectCode();


    /**
     * <p> 工程名 </p>
     * @return
     */
    String getProjectName();

    default String getAccessToken(HttpServletRequest request){
        String token = request.getHeader("access-token");
        if(StringUtils.isBlank(token)){
            token = request.getHeader("accesstoken");
        }
        if(StringUtils.isBlank(token)){
            token = request.getParameter("access-token");
        }
        return  token;
    }


}
