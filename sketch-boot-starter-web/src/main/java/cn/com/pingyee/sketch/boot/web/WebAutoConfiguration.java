package cn.com.pingyee.sketch.boot.web;

import cn.com.pingyee.sketch.boot.web.aop.ExecuteTimeStatisticAspectJ;
import cn.com.pingyee.sketch.boot.web.file.LocalFileAutoConfiguration;
import cn.com.pingyee.sketch.boot.web.filter.ApiRequestFilter;
import cn.com.pingyee.sketch.boot.web.inteceptor.AvoidDuplicateSubmissionInterceptor;
import cn.com.pingyee.sketch.boot.web.inteceptor.UserInterceptor;
import cn.com.pingyee.sketch.boot.web.wechat.WechatConfiguration;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.web.ServerProperties;
import org.springframework.boot.autoconfigure.web.servlet.error.ErrorMvcAutoConfiguration;
import org.springframework.boot.autoconfigure.web.servlet.error.ErrorViewResolver;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.error.ErrorAttributes;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.core.annotation.Order;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**spring.factories
 * @author: Yi Ping
 * @date: 2019/9/17 0017 22:39
 * @since: 1.0.0
 */

@Configuration
@AutoConfigureBefore(ErrorMvcAutoConfiguration.class)
@Import(WechatConfiguration.class)
@ImportAutoConfiguration(LocalFileAutoConfiguration.class)
@EnableConfigurationProperties(ApiRequestFilter.ApiProperty.class)
public class WebAutoConfiguration {

    private final Optional<SketchErrorControllerCostomizer> sketchErrorControllerCostomizer;

    public WebAutoConfiguration(Optional<SketchErrorControllerCostomizer> sketchErrorControllerCostomizer) {
        this.sketchErrorControllerCostomizer = sketchErrorControllerCostomizer;
    }

    @Bean
    @ConditionalOnMissingBean(StringToDateConverter.class)
    public StringToDateConverter stringToDateConverter(){
        return  new StringToDateConverter();
    }



    @Bean
    @ConditionalOnMissingBean(DomainRegistry.class)
    public DomainRegistry domainRegistry(){
        return  new DomainRegistry();
    }

    @Bean
    @ConditionalOnBean({ErrorAttributes.class, ServerProperties.class})
    @ConditionalOnMissingBean(ErrorController.class)
    public SketchErrorController sketchErrorController(ErrorAttributes errorAttributes, ServerProperties serverProperties, List<ErrorViewResolver> errorViewResolvers){
        SketchErrorController errorController =   new SketchErrorController(errorAttributes, serverProperties, errorViewResolvers, true);
        customizeErrorController(errorController);
        return errorController;
    }

    private void customizeErrorController(SketchErrorController errorController) {
        sketchErrorControllerCostomizer.ifPresent(c->c.customize(errorController));
    }

    //    @Bean
    @ConditionalOnMissingBean
    public AdviseExceptionHandler adviseExceptionHandler(){
        return  new AdviseExceptionHandler();
    }

    @Bean
    @ConditionalOnMissingBean
    public GlobalExceptionHandler globalExceptionHandler(){
        return  new GlobalExceptionHandler();
    }

    @Bean
    @ConditionalOnMissingBean(ObjectMapper.class)
    public ObjectMapper objectMapper(Jackson2ObjectMapperBuilder builder) {
        ObjectMapper objectMapper = builder.build();
        objectMapper
                .enable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
                .disable(SerializationFeature.FAIL_ON_EMPTY_BEANS)
//                .registerModule(new JavaTimeModule())
                .disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES)
        ;

        /** 解决long 序列化精度丢失的问题　**/
        SimpleModule simpleModule = new SimpleModule();
        simpleModule.addSerializer(Long.class, ToStringSerializer.instance);
        simpleModule.addSerializer(Long.TYPE, ToStringSerializer.instance);
        objectMapper.registerModule(simpleModule);

        return objectMapper;
    }


    @Bean
    @ConditionalOnMissingBean
    public ErrorCodesAndApiStateController errorCodesController(){
        return  new ErrorCodesAndApiStateController();
    }



    @Bean
    @ConditionalOnClass(name="org.aspectj.lang.annotation.Pointcut")
    public ExecuteTimeStatisticAspectJ executeTimeStatisticAspectJ(){
        return  new ExecuteTimeStatisticAspectJ();
    }

    @Bean
    @Order(1)
    @ConditionalOnMissingBean(name = "avoidDuplicateSubmissionWebMvcConfigurer")
    public WebMvcConfigurer avoidDuplicateSubmissionWebMvcConfigurer(){
        return new WebMvcConfigurer() {
            @Override
            public void addInterceptors(InterceptorRegistry registry) {
                registry.addInterceptor(new AvoidDuplicateSubmissionInterceptor()).addPathPatterns("/**");
            }
        };
    }


    @Bean
    @Order(2)
    @ConditionalOnBean(UserInterceptor.class)
    @ConditionalOnMissingBean(name = "operatorInterceptorWebMvcConfigurer")
    public WebMvcConfigurer operatorInterceptorWebMvcConfigurer(){
        return new WebMvcConfigurer() {
            @Autowired
            private UserInterceptor operatorInterceptor;

            @Override
            public void addInterceptors(InterceptorRegistry registry) {
                registry.addInterceptor(operatorInterceptor).addPathPatterns("/**");
            }
        };
    }


    @Bean
//    @ConditionalOnBean(ApiRequestFilter.ApiProperty.class)
    @ConditionalOnMissingBean(name = "apiFilterRegistrationBean")
    public FilterRegistrationBean apiFilterRegistrationBean(ApiRequestFilter.ApiProperty apiProperty){
        ApiRequestFilter filter = new ApiRequestFilter(apiProperty);
        FilterRegistrationBean b = new  FilterRegistrationBean(filter);
        b.setName("apiRequestFilter");
        b.setOrder(filter.getOrder());
        b.setUrlPatterns(Stream.of(apiProperty.getPrefix() + "/*").collect(Collectors.toList()));
        return  b;
    }

}
