package cn.com.pingyee.sketch.boot.web.wechat.pay;


import cn.com.pingyee.sketch.boot.web.wechat.pay.sdk.IWXPayDomain;
import cn.com.pingyee.sketch.boot.web.wechat.pay.sdk.WXPayConfig;
import cn.com.pingyee.sketch.core.util.Assert;
import lombok.Getter;

import java.io.InputStream;

//import org.springframework.boot.context.properties.ConfigurationProperties;


/**
 * @author: YiPing
 * @create: 2018-09-05 20:36
 * @since: 1.0
 */

@Getter
public class PayProperty extends WXPayConfig {

    private final String appID;
    private final String mchID;
    private final String key;
    private IWXPayDomain WXPayDomain = new IWXPayDomain.DefaultWXPayDomain();
    private InputStream certStream;
    private int httpReadTimeoutMs = 60000;
    private int httpConnectTimeoutMs = 60000;
    private boolean shouldAutoReport = true;
    private int reportWorkerNum = 1;
    private int reportQueueMaxSize = 10;
    private int reportBatchSize  = 10;

    public PayProperty(String appID, String mchID, String key) {
        Assert.isNotBlank(appID, "appId should not be blank");
        Assert.isNotBlank(mchID, "mchID should not be blank");
        Assert.isNotBlank(key, "key should not be blank");

        this.appID = appID;
        this.mchID = mchID;
        this.key = key;
    }

}
