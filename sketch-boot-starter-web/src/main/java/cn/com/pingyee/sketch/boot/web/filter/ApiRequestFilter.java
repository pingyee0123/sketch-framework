package cn.com.pingyee.sketch.boot.web.filter;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author: Yi Ping
 * @date: 2019/11/4 0004 14:08
 * @since: 0.0.1-SNAPSHOT
 */

//@Component
@Slf4j
@Order(Integer.MIN_VALUE)
//@WebFilter(filterName = "apiRequestFilter", urlPatterns = {"/api/**"})
public class ApiRequestFilter extends OncePerRequestFilter implements Ordered {

    private ApiProperty apiProperty;

    public ApiRequestFilter(ApiProperty apiProperty) {
        this.apiProperty = apiProperty;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        if(log.isDebugEnabled()){
            log.debug("{} enter api request filter ", request.getRequestURI());
        }
        if(request.getRequestURI().startsWith(apiProperty.getPrefix())) {
            ApiRequestWrapper request1 = new ApiRequestWrapper(request, apiProperty.getPrefix());
            if(log.isDebugEnabled()){
                log.debug("servlet path " + request1.getServletPath());
            }
            filterChain.doFilter(request1, response);
        }else {
            filterChain.doFilter(request,response);
        }
    }

    @Override
    public int getOrder() {
        return -1000;
    }

    /**
     * @author: Yi Ping
     * @date: 2019/11/4 0004 14:03
     * @since: 0.0.1-SNAPSHOT
     */
    public static class ApiRequestWrapper  extends HttpServletRequestWrapper {

        private String prefix;

        public ApiRequestWrapper(HttpServletRequest request, String prefix) {
            super(request);
            this.prefix = prefix;
        }

//        public ApiRequestWrapper(HttpServletRequest request) {
//            super(request);
//
//        }

        @Override
        public String getRequestURI() {
            return super.getRequestURI().replace(prefix,"");
        }

        @Override
        public String getServletPath() {
            return super.getServletPath().replace(prefix,"");
        }
    }


    @Data
    @ConfigurationProperties(prefix = "sketch.web.api")
    public static class ApiProperty{
        private String prefix = "/api";
        private int order = -1000;
    }
}
