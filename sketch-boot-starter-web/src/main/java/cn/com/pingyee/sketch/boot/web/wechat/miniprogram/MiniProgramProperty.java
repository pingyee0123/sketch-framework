package cn.com.pingyee.sketch.boot.web.wechat.miniprogram;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author: Yi Ping
 * @create: 2018-12-17 18:31
 * @since: 1.0.0
 */

@Data
@ConfigurationProperties(prefix = "wechat.miniprogram")
public class MiniProgramProperty {

    @ApiModelProperty("小程序 appId")
    private String appid;
    @ApiModelProperty("小程序 appSecret")
    private String secret;
}
