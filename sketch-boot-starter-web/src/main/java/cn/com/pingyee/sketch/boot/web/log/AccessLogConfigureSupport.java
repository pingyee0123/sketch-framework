package cn.com.pingyee.sketch.boot.web.log;


import cn.com.pingyee.sketch.boot.web.log.service.AccessLogProperty;
import cn.com.pingyee.sketch.boot.web.log.service.RemoteAccessLogService;
import cn.com.pingyee.sketch.boot.web.log.trace.TraceIdGenerator;
import cn.com.pingyee.sketch.boot.web.log.trace.TraceIdInteceptor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.core.annotation.Order;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author: Yi Ping
 * @date: 2019/6/12 0012 15:05
 * @since: 1.4.36
 */

@EnableConfigurationProperties(AccessLogProperty.class)
public class AccessLogConfigureSupport {


    @Bean
    @ConditionalOnMissingBean(value = RestTemplate.class)
    public RestTemplate restTemplate(){
        return  new RestTemplate();
    }
    
//    @Bean
//    @ConditionalOnMissingBean(value = Executor.class)
//    public Executor executor(){
//        return  new ThreadPoolExecutor(5,20, 1, TimeUnit.MINUTES, new LinkedBlockingQueue<>(1024)
//              ,new CustomizableThreadFactory("access-log-"),new ThreadPoolExecutor.AbortPolicy());
//    }

    @Bean
    @ConditionalOnMissingBean(AccessLogService.class)
    public AccessLogService accessLogService(AccessLogProperty accessLogServiceProperty, RestTemplate restTemplate){
        return  new RemoteAccessLogService(accessLogServiceProperty, restTemplate);
    }
    

//    @Bean
//    @ConditionalOnClass(value = feign.RequestInterceptor.class)
//    TraceIdFeignInterceptor traceIdFeignInterceptor(){
//        return  new TraceIdFeignInterceptor();
//    }

    @Bean
    @ConditionalOnMissingBean(value = TraceIdGenerator.class)
    public TraceIdGenerator traceIdGenerator(){
        return  new TraceIdGenerator.DefaultTraceIdGenerator();
    }

    @Bean
    @ConditionalOnMissingBean(value = TraceIdInteceptor.class)
    public TraceIdInteceptor traceIdInteceptor(){
        return  new TraceIdInteceptor(true,traceIdGenerator());
    }

    @Bean
    @Order
    public WebMvcConfigurer accessLogWebMvcConfigurer(AccessLogService accessLogService){

        return  new WebMvcConfigurer() {

            @Override
            public void addInterceptors(InterceptorRegistry registry) {
                registry.addInterceptor(traceIdInteceptor()).addPathPatterns("/**");
                registry.addInterceptor(new AccessLogInteceptor(accessLogService)).addPathPatterns("/**");
            }
        };
    }

}
