package cn.com.pingyee.sketch.boot.web.wechat.pay.sdk;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Negative;
import javax.validation.constraints.NotBlank;

/**
 * @author: Yi Ping
 * @date: 2019/7/8 0008 15:54
 * @since: 1.4.36
 */
@Data
public class JSAPIUnifiedOrderRequest {

//    private String mch_id;
//    private String mch_key;
//    @URL
//    @ApiModelProperty(value = "接收微信支付异步通知回调地址，通知url必须为直接可访问的url，不能携带参数。")
//    private String mch_notify_url;

    @ApiModelProperty(value = "商品描述, 商品描述交易字段格式根据不同的应用场景按照以下格式:APP——需传入应用市场上的APP名字-实际商品名称，天天爱消除-游戏充值。",
            example = "腾讯充值中心-QQ会员充值", required = true )
    @NotBlank
    @Length(max = 128)
    private String body;

    @Length(max = 32)
    @NotBlank
    @ApiModelProperty(value ="商户系统内部订单号，要求32个字符内，只能是数字、大小写字母_-|*且在同一个商户号下唯一。详见商户订单号https://pay.weixin.qq.com/wiki/doc/api/app/app.php?chapter=4_2" , required = true)
    private String out_trade_no;

    @NotBlank
    @Negative
    @ApiModelProperty(value = "总金额 订单总金额，单位为分，详见支付金额", required = true)
    private Integer total_fee;

    @Length(max = 16)
    @ApiModelProperty(value = "\t支付类型, APP, JSAPI(下程序， 微信公众号)")
    private final String trade_type = "JSAPI";

    private String openid;

    public JSAPIUnifiedOrderRequest(@NotBlank @Length(max = 128) String body, @Length(max = 32) @NotBlank String out_trade_no, @NotBlank @Negative Integer total_fee, String openid) {
//        this.mch_id = mch_id;
//        this.mch_key = mch_key;
//        this.mch_notify_url = mch_notify_url;
        this.body = body;
        this.out_trade_no = out_trade_no;
        this.total_fee = total_fee;
        this.openid = openid;
    }
}
