package cn.com.pingyee.sketch.boot.web.wechat.pay.sdk;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.URL;

import javax.validation.constraints.Negative;
import javax.validation.constraints.NotBlank;

/**
 * <p>  https://pay.weixin.qq.com/wiki/doc/api/app/app.php?chapter=9_1 </p>
 * @author: Yi Ping
 * @date: 2019/7/8 0008 15:28
 * @since: 1.4.36
 */
@Data
public class UnifiedOrderRequest {

    @ApiModelProperty(value = "商品描述, 商品描述交易字段格式根据不同的应用场景按照以下格式:APP——需传入应用市场上的APP名字-实际商品名称，天天爱消除-游戏充值。",
            example = "腾讯充值中心-QQ会员充值", required = true )
    @NotBlank
    @Length(max = 128)
    private String body;

    @Length(max = 8192)
    @ApiModelProperty("商品详细描述，对于使用单品优惠的商户，该字段必须按照规范上传，详见“单品优惠参数说明 https://pay.weixin.qq.com/wiki/doc/api/danpin.php?chapter=9_102&index=2”")
    private String detail;

    @Length(max = 127)
    @ApiModelProperty("附加数据，在查询API和支付通知中原样返回，该字段主要用于商户携带订单的自定义数据")
    private String attach;

    @Length(max = 32)
    @NotBlank
    @ApiModelProperty(value ="商户系统内部订单号，要求32个字符内，只能是数字、大小写字母_-|*且在同一个商户号下唯一。详见商户订单号https://pay.weixin.qq.com/wiki/doc/api/app/app.php?chapter=4_2" , required = true)
    private String out_trade_no;

    @ApiModelProperty(value = "货币类型：符合ISO 4217标准的三位字母代码，默认人民币：CNY，其他值列表详见货币类型")
    private String fee_type;

    @NotBlank
    @Negative
    @ApiModelProperty(value = "总金额 订单总金额，单位为分，详见支付金额", required = true)
    private Integer total_fee;

    @NotBlank
    @Length(max = 64)
    @ApiModelProperty(value = "终端IP,支持IPV4和IPV6两种格式的IP地址。调用微信支付API的机器IP",required = true)
    private String spbill_create_ip;

    @Length(max = 14)
    @ApiModelProperty(value = "订单生成时间，格式为yyyyMMddHHmmss，如2009年12月25日9点10分10秒表示为20091225091010。其他详见时间规则")
    private String time_start;

    @Length(max = 14)
    @ApiModelProperty(value = "订单失效时间，格式为yyyyMMddHHmmss，如2009年12月27日9点10分10秒表示为20091227091010。订单失效时间是针对订单号而言的，" +
            "由于在请求支付的时候有一个必传参数prepay_id只有两小时的有效期，所以在重入时间超过2小时的时候需要重新请求下单接口获取新的prepay_id。其他详见时间规则" +
            "建议：最短失效时间间隔大于1分钟" )
    private String time_expire;

    @Length(max = 32)
    @ApiModelProperty(value = "订单优惠标记，代金券或立减优惠功能的参数，说明详见代金券或立减优惠")
    private String goods_tag;

    @URL
    @ApiModelProperty(value = "接收微信支付异步通知回调地址，通知url必须为直接可访问的url，不能携带参数。")
    private String notify_url;

    @Length(max = 16)
    @ApiModelProperty(value = "\t支付类型, APP, JSAPI(下程序， 微信公众号)")
    private String trade_type;

    @Length(max = 32)
    @ApiModelProperty(value = "no_credit--指定不能使用信用卡支付")
    private String limit_pay;

    @Length(max = 8)
    @ApiModelProperty(value = "Y，传入Y时，支付成功消息和支付详情页将出现开票入口。需要在微信支付商户平台或微信公众平台开通电子发票功能，传此字段才可生效")
    private String receipt;
}
