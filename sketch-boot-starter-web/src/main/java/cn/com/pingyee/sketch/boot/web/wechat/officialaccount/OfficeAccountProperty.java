package cn.com.pingyee.sketch.boot.web.wechat.officialaccount;

import lombok.Data;

/**
 * <p> 微信公众号设置 </p>
 * @author: YiPing
 * @create: 2018-08-28 19:06
 * @since: 1.0
 */
@Data
public class OfficeAccountProperty {
    private String appid;
    private String secret;

    public String getAppid() {
        return appid;
    }

}
