package cn.com.pingyee.sketch.boot.web.wechat.officialaccount;

import lombok.Data;

import java.io.Serializable;

/**
 * @author: YiPing
 * @create: 2018-08-28 17:54
 * @since: 1.0
 */
@Data
public class UserInfo implements Serializable {

    private String openId;
    private String nickname;
    private String sex;
    private String province;
    private String city;
    private String county;
    private String headimgurl;
    private String privilege;
    private String unionid;


}
