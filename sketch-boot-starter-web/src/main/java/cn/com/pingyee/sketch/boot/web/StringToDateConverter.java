package cn.com.pingyee.sketch.boot.web;

import org.apache.commons.lang3.StringUtils;
import org.springframework.core.convert.converter.Converter;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author: YiPing
 * @create: 2018-08-28 13:54
 * @since: 1.0
 */
public class StringToDateConverter implements Converter<String,Date> {

//    private static final SimpleDateFormat SDF = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
    private static final SimpleDateFormat SDFyyyyMMdd = new SimpleDateFormat("yyyy/MM/dd");
    private static final  String  dashyyyyMMddHHmmss= "^\\d{4}-\\d{2}-\\d{2} \\d{2}:\\d{2}:\\d{2}$";
    private static final  String  dashyyyyMMdd= "^\\d{4}-\\d{2}-\\d{2}$";
    private static final  String  slashyyyyMMddHHmmss= "^\\d{4}/\\d{2}/\\d{2} \\d{2}:\\d{2}:\\d{2}$";
    private static final  String  slashyyyyMMdd= "^\\d{4}/\\d{2}/\\d{2}$";
    private static final  String timestamp = "^[+|-]?\\d+$";


    @Deprecated
    private static Map<String, SimpleDateFormat> dateFormatMap = new HashMap<>();

    static {
        dateFormatMap.put(dashyyyyMMdd, new SimpleDateFormat("yyyy-MM-dd"));
        dateFormatMap.put(dashyyyyMMddHHmmss, new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"));
        dateFormatMap.put(slashyyyyMMdd, new SimpleDateFormat("yyyy/MM/dd"));
        dateFormatMap.put(slashyyyyMMddHHmmss, new SimpleDateFormat("yyyy/MM/dd HH:mm:ss"));
    }


    @Override
    public Date convert(String source) {
        Date date;
        try {
            if(StringUtils.isBlank(source)) {
               date = null;
            }else if(source.matches(timestamp)){
                date = new Date();
                date.setTime(Long.valueOf(source));
            }else if(source.matches(dashyyyyMMdd)){
                // 避免 SimpleDateFormat 的并发安全问题　
                date = new SimpleDateFormat("yyyy-MM-dd")/* dateFormatMap.get(dashyyyyMMdd)*/.parse(source);
            }else if(source.matches(dashyyyyMMddHHmmss)){
                date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")/* dateFormatMap.get(dashyyyyMMddHHmmss)*/.parse(source);
            }else if(source.matches(slashyyyyMMdd)){
                date = /*dateFormatMap.get(slashyyyyMMdd)*/ new SimpleDateFormat("yyyy/MM/dd").parse(source);
            }else if (source.matches(slashyyyyMMddHHmmss)){
                date = /*dateFormatMap.get(slashyyyyMMddHHmmss)*/ new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").parse(source);
            }else {
                throw new Exception("不支持日期格式: " + source);
            }
        }catch (Exception e){
            throw new RuntimeException(e);
        }


        return  date;
    }
}
