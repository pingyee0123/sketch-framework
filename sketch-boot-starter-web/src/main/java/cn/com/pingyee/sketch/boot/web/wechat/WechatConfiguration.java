package cn.com.pingyee.sketch.boot.web.wechat;

import cn.com.pingyee.sketch.boot.web.wechat.miniprogram.MiniProgramManager;
import cn.com.pingyee.sketch.boot.web.wechat.officialaccount.OfficialAccountService;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author: Yi Ping
 * @date: 2019/9/25 0025 17:02
 * @since: 1.0.0
 */

@Configuration
@EnableConfigurationProperties(WechatProperty.class)
public class WechatConfiguration {

    @Bean
    @ConditionalOnMissingBean(OfficialAccountService.class)
    public OfficialAccountService officialAccountService(WechatProperty wechatProperty){
        return  new OfficialAccountService(wechatProperty);
    }

    @Bean
    @ConditionalOnMissingBean(MiniProgramManager.class)
    public MiniProgramManager miniProgramManager(WechatProperty wechatProperty){
        return  new MiniProgramManager(wechatProperty.getMiniProgram());
    }
}
