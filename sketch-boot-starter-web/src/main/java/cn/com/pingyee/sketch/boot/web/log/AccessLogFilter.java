package cn.com.pingyee.sketch.boot.web.log;


import cn.com.pingyee.sketch.boot.web.log.trace.TraceIdGenerator;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.concurrent.BasicThreadFactory;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;
import java.util.concurrent.Executor;
import java.util.concurrent.ScheduledThreadPoolExecutor;

import static cn.com.pingyee.sketch.boot.web.log.AccessLog.TRACE_ID_HEADER;

/**
 * <p> 保存用户访问日志  </p>
 * @author: Yi Ping
 * @create: 2018-11-24 17:59
 * @since: 0.0.1-snapshot
 */
public class AccessLogFilter extends OncePerRequestFilter {

    /**
     * <p>  access log 服务</p>
     */
    private AccessLogService accessLogService;
    /**
     *  <p> 线程池</p>
     */
    private final  Executor executor;
    private TraceIdGenerator traceIdGenerator = new TraceIdGenerator.DefaultTraceIdGenerator();



    public AccessLogFilter(AccessLogService accessLogService, Executor executor) {
        this.accessLogService = accessLogService;
        this.executor = executor;
    }

    public AccessLogFilter(AccessLogService accessLogService) {
        this(accessLogService, new ScheduledThreadPoolExecutor(
                3,
                new BasicThreadFactory.Builder().namingPattern("access-filter-").daemon(true).build()
        ));
    }


    @Override
    protected void  doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws ServletException, IOException {

        _setTraceIdAttribute(request);
        final long start = System.currentTimeMillis();
        final Date accessTime = new Date();
        try {
            chain.doFilter(request, response);
        }catch (Exception e1){
            throw e1;
        }finally {
            final AccessLog log = accessLogService.createLog(request, response);
            log.setAccessTime(accessTime);
            log.setRequestTime(System.currentTimeMillis() - start);
            /** 异步存储访问日志 **/
            executor.execute(()-> accessLogService.save(log));
        }

    }

    protected String getTraceId(HttpServletRequest request){
        return request.getHeader(TRACE_ID_HEADER);
    }

    private void _setTraceIdAttribute(HttpServletRequest request){
        String trace_id = getTraceId(request);
        if(StringUtils.isBlank(trace_id)){
            trace_id = traceIdGenerator.generate(request);
        }
        request.setAttribute(AccessLog.TRACE_ID_ATTRIBUTE_NAME, trace_id);
    }
}
