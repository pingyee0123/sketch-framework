package cn.com.pingyee.sketch.boot.web;

import cn.com.pingyee.sketch.core.exception.BusinessException;
import cn.com.pingyee.sketch.core.response.ApiJson;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.DispatcherServlet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

/**
 * @author: Yi Ping
 * @create: 2019-03-18 20:50
 * @since: 1.4.9
 */


@Deprecated
@Slf4j
@ControllerAdvice
public  class AdviseExceptionHandler {

    protected Map<Class, SketchErrorController.ExceptionHandler> exceptionHandlers = new HashMap<>();

    public AdviseExceptionHandler(){
         exceptionHandlers.putAll(SketchErrorController.initExceptionHandlers());
    }

    @org.springframework.web.bind.annotation.ExceptionHandler(value = Throwable.class)
    public ApiJson handleThrowable(Throwable e, HttpServletRequest r, HttpServletResponse response){

        if(BusinessException.class.isAssignableFrom(e.getClass())){
            r.removeAttribute(DispatcherServlet.EXCEPTION_ATTRIBUTE);
        }
        /** 记录异常日志 **/
        else if(RuntimeException.class.isAssignableFrom(e.getClass())
                && !exceptionHandlers.containsKey(e.getClass())){
            log.error("request " + r.getRequestURL().toString() + " error: " +  e.getLocalizedMessage(), e);
        }

        return SketchErrorController.findHandler((Class<? extends Exception>) e.getClass(), exceptionHandlers).handle(e);
    }





}
