package cn.com.pingyee.sketch.boot.web.wechat.miniprogram;

import cn.com.pingyee.sketch.boot.web.wechat.Utils;
import cn.com.pingyee.sketch.boot.web.wechat.WechatReponse;
import cn.com.pingyee.sketch.core.exception.ApiException;
import cn.com.pingyee.sketch.core.exception.BusinessException;
import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.BufferedImageHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import javax.validation.constraints.NotBlank;
import java.awt.image.BufferedImage;
import java.util.HashMap;
import java.util.Map;

/**
 * @author: Yi Ping
 * @create: 2018-12-17 18:36
 * @since: 1.0.0
 */
@Slf4j
public class MiniProgramManager {

    private static final String   LOGIN_URL = "https://api.weixin.qq.com/sns/jscode2session?appid={APPID}&secret={SECRET}&js_code={JSCODE}&grant_type=authorization_code";
    private static final String   URL_ACCESS_TOKEN = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid={APPID}&secret={APPSECRET}";
    private static final String   URL_SEND_TEMPLATE_MESSAGE="https://api.weixin.qq.com/cgi-bin/message/wxopen/template/send?access_token={ACCESS_TOKEN}";
    private static final String   URL_SEND_UNIFORM_MESSAGE = "https://api.weixin.qq.com/cgi-bin/message/wxopen/template/uniform_send?access_token={ACCESS_TOKEN}";
    private static final String  URL_CREATE_QRCODE = "https://api.weixin.qq.com/cgi-bin/wxaapp/createwxaqrcode?access_token={ACCESS_TOKEN}";
    private static final String  URL_WXACODE_UNLIMIT = "https://api.weixin.qq.com/wxa/getwxacodeunlimit?access_token={ACCESS_TOKEN}";

    protected  final RestTemplate restTemplate;
    private static AccessTokenResponse accessToken;

    private  MiniProgramProperty property;

    private MiniProgramManager() {
        restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new BufferedImageHttpMessageConverter());
    }

    public MiniProgramManager(MiniProgramProperty property) {
        this();
        this.property = property;
    }


    /**
     * <p>小程序登录</p>
     * @param code
     * @return
     * @throws 100031, 100032, 100033  100044 100045异常
     */
    public LoginResponse login(String code) throws ApiException {
        ResponseEntity<String> entity = restTemplate.getForEntity(LOGIN_URL, String.class, property.getAppid(), property.getSecret(), code);
        LoginResponse response = JSON.parseObject(entity.getBody(), LoginResponse.class);
        switch (response.getErrcode()){
            case -1:
                throw new BusinessException("100031");
            case 40029:
                throw new BusinessException("100032");
            case 45011:
                throw new BusinessException("100033");
            case 40163:
                throw  new BusinessException("100044");
            case 0:
                return  response;
            default:
                throw  new BusinessException("100045", response.getErrmsg());
        }
    }

    protected   void cacheAccessToken(AccessTokenResponse token){
        accessToken = token;
    }

    protected  AccessTokenResponse getAccessTokenFromCache(){ return  accessToken;}

    /**
     * <p> 获取访问小程序资源的 access_token</p>
     * @return
     */
    public String getAccessToken(){
        AccessTokenResponse token = getAccessTokenFromCache();
        if(token == null || token.isExpired()){
            token = _getAccessToken();
            cacheAccessToken(token);
        }
        return token.getAccess_token();
    }

    private AccessTokenResponse _getAccessToken(){
        AccessTokenResponse token = restTemplate.getForObject(URL_ACCESS_TOKEN, AccessTokenResponse.class, property.getAppid(), property.getSecret());
        if(token.getErrcode() ==0){
            return accessToken;
        }else {
            throw new BusinessException("100046", token.getErrmsg());
        }
    }




    public void sendTemplateMessage(TemplateMessageRequest request){
        WechatReponse response = restTemplate.postForObject(URL_SEND_TEMPLATE_MESSAGE, request, WechatReponse.class, getAccessToken());
        if(!response.isSuccess()){
            if(log.isWarnEnabled()){
                log.warn("send template message  fail:" + response.getErrmsg());
            }
        }
    }


    public void sentUniformMessage(UniformMessageRequest request){
        WechatReponse response = restTemplate.postForObject(URL_SEND_UNIFORM_MESSAGE, Utils.wrapToApplicationJsonRequest(request), WechatReponse.class, getAccessToken());
        if(!response.isSuccess()){
            if(log.isWarnEnabled()){
                log.warn("send template message  fail:" + response.getErrmsg());
            }
        }
    }

    /**
     * <p> 获取小程序普通二维码 </p>
     * @param path
     * @param width
     * @return
     */
    public BufferedImage createQrCode(String path, int width){
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON_UTF8);

        Map<String,String > postData= new HashMap<>(16);
        postData.put("path", path);
        postData.put("width", width + "");

        HttpEntity<Map<String, String>> request = new HttpEntity<>(postData, headers);

        BufferedImage bufferedImage = restTemplate.postForObject(URL_CREATE_QRCODE + getAccessToken(), request, BufferedImage.class, getAccessToken());
        return bufferedImage;
    }

    /**
     *  <p> 获取小程序二维码 </p>
     * @param scene  必传 最大32个可见字符，只支持数字，大小写英文以及部分特殊字符：!#$&'()*+,/:;=?@-._~，其它字符请自行编码为合法字符（因不支持%，中文无法使用 urlencode 处理，请使用其他编码方式）
     * @param page 必须是已经发布的小程序存在的页面（否则报错），例如 pages/index/index, 根路径前不要填加 /,不能携带参数（参数请放在scene字段里），如果不填写这个字段，默认跳主页面
     * @param width 二维码的宽度，单位 px，最小 280px，最大 1280px
     * @param auto_color 自动配置线条颜色，如果颜色依然是黑色，则说明不建议配置主色调，默认 false
     * @param line_color auto_color 为 false 时生效，使用 rgb 设置颜色 例如 {"r":"xxx","g":"xxx","b":"xxx"} 十进制表示
     * @param is_hyaline 是否需要透明底色，为 true 时，生成透明底色的小程序
     * @return
     */
    public Resource getWxacodeUnlimit(@NotBlank String scene,
                                      String page,
                                      int width,
                                      boolean auto_color,
                                      Map<String,Integer> line_color,
                                      boolean is_hyaline){


        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON_UTF8);

        Map<String,Object > map = new HashMap<>(16);
        map.put("scene", scene);
        if(StringUtils.isNotBlank(page)) {
            map.put("page", page);
        }
        map.put("width", width + "");
        map.put("auto_color", auto_color);
//            map.put("line_color", line_color);
        map.put("is_hyaline", is_hyaline);

        HttpEntity<Map<String, String>> request = new HttpEntity(map, headers);
        try {
            Resource image = restTemplate.postForObject(URL_WXACODE_UNLIMIT, request, Resource.class, getAccessToken());
            return image;
        }catch (Exception e){
            e.printStackTrace();
        }

        return  null;
    }




}
