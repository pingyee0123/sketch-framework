package cn.com.pingyee.sketch.boot.web.log;

import cn.com.pingyee.sketch.core.request.DefaultPageRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * @author: Yi Ping
 * @date: 2019/9/21 0021 0:26
 * @since: 1.0.0
 */

@Getter
@Setter
public class AccessLogQuery extends DefaultPageRequest {

    @ApiModelProperty("工程编号")
    private int projectCode;

    @ApiModelProperty("工程名")
    private String projectName;

    @ApiModelProperty("当前请求的用户")
    private String accessUser;

    @ApiModelProperty("请求uri")
    private String uri;

    @ApiModelProperty("请求时间(查询开始)")
    private Date startAccessTime;

    @ApiModelProperty("请求时间(查询结束)")
    private Date endAccessTime;
}
