package cn.com.pingyee.sketch.boot.web;

import org.springframework.validation.BindingResult;

/**
 * @author Yi Ping
 * @version 2.1
 * @date 2020/8/2 23:17
 */
public abstract class ExceptionHandlerHelper {

    public static String beautifyBindingResultErrorMessage(BindingResult result){
        StringBuilder m = new StringBuilder();
        if(result.getFieldErrorCount()>0){
            result.getFieldErrors().stream().forEach(
                    e-> m/*.append(e.getObjectName()).append(".")*/.append(e.getField()).append(" ").append(e.getDefaultMessage()).append(";"));
        }else {
            result.getAllErrors().stream().forEach(e->
                    m./*append(e.getObjectName()).*/append(e.getArguments()).append(" ").append(e.getDefaultMessage()).append(";"));
        }

        return m.toString();
    }
}
