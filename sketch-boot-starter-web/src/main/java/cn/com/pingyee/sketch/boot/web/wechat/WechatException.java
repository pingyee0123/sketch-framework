package cn.com.pingyee.sketch.boot.web.wechat;

/**
 * @author: YiPing
 * @create: 2018-09-07 15:31
 * @since: 1.0
 */
public class WechatException extends RuntimeException{
    public WechatException(String message) {
        super(message);
    }

    public WechatException(String message, Throwable cause) {
        super(message, cause);
    }

    public WechatException(Throwable cause) {
        super(cause);
    }

    public WechatException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
