package cn.com.pingyee.sketch.boot.web;

import cn.com.pingyee.ddd.log.api.AccessLog;
import cn.com.pingyee.ddd.log.api.AccessLogQuery;
import cn.com.pingyee.ddd.log.api.AccessLogService;
import cn.com.pingyee.ddd.log.api.web.AccessLogFilter;
import cn.com.pingyee.ddd.log.api.web.AccessLogInterceptor;
import cn.com.pingyee.ddd.log.api.web.WebAccessLogService;
import cn.com.pingyee.ddd.log.api.web.trace.TraceIdFilter;
import cn.com.pingyee.ddd.log.api.web.trace.TraceIdGenerator;
import cn.com.pingyee.ddd.log.api.web.trace.TraceIdInteceptor;
import cn.com.pingyee.sketch.core.domain.PageResult;
import cn.com.pingyee.sketch.core.request.UserContextHolder;
import cn.com.pingyee.sketch.core.util.Assert;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.web.servlet.ConditionalOnMissingFilterBean;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.Optional;

/**
 * @author Yi Ping
 * @version 2.1
 * @date 2020/9/23 15:13
 */

@Configuration
@ConditionalOnClass(WebAccessLogService.class)
@EnableConfigurationProperties(AccessLogAutoConfiguration.AccessLogProperty.class)
//@EnableAutoConfiguration
@Import(AccessLogAutoConfiguration.AccessLogFilterConfiguration.class)
public class AccessLogAutoConfiguration {

    @Bean
    @ConditionalOnMissingBean(RestTemplate.class)
    public RestTemplate restTemplate(){
        return  new RestTemplate();
    }

    @Bean
    @ConditionalOnMissingBean(value = AccessLogService.class, name="accessLogService")
    public AccessLogService accessLogService(AccessLogProperty accessLogServiceProperty, RestTemplate restTemplate){
        return  new RemoteWebAccessLogService(accessLogServiceProperty, restTemplate);
    }

    @Bean
    public WebAccessLogService webAccessLogService(AccessLogService accessLogService, AccessLogProperty accessLogProperty){
        return  new DelegateWebAccessLogService(accessLogService, accessLogProperty.getProject());
    }


//    @Bean
//    @ConditionalOnClass(value = feign.RequestInterceptor.class)
//    TraceIdFeignInterceptor traceIdFeignInterceptor(){
//        return  new TraceIdFeignInterceptor();
//    }

    @Bean
    @ConditionalOnMissingBean(value = TraceIdGenerator.class)
    public TraceIdGenerator traceIdGenerator(){
        return  new TraceIdGenerator.DefaultTraceIdGenerator();
    }

    @Bean
    @ConditionalOnMissingBean(value = TraceIdInteceptor.class)
    public TraceIdInteceptor traceIdInteceptor(){
        return  new TraceIdInteceptor(true,traceIdGenerator());
    }



    @Bean
    @Order
    @ConditionalOnMissingFilterBean(AccessLogFilter.class)
    public WebMvcConfigurer accessLogWebMvcConfigurer(WebAccessLogService webAccessLogService){
        return  new WebMvcConfigurer() {
            @Override
            public void addInterceptors(InterceptorRegistry registry) {
                registry.addInterceptor(traceIdInteceptor()).addPathPatterns("/**");
                registry.addInterceptor(new AccessLogInterceptor(webAccessLogService)).addPathPatterns("/**");
            }
        };
    }


    @Configuration
    @AutoConfigureAfter(
            AccessLogAutoConfiguration.class
    )
//    @ConditionalOnBean(WebAccessLogService.class)
    public static class AccessLogFilterConfiguration {

        @Autowired
        private AccessLogProperty accessLogProperty;

        @Bean
        @ConditionalOnMissingBean(value = TraceIdFilter.class)
        public TraceIdFilter traceIdFilter(){
            TraceIdFilter f =   new TraceIdFilter();
            f.setOrder(accessLogProperty.getOrder()-1);
            return  f;
        }

        @Bean
        @ConditionalOnMissingBean(value = AccessLogFilter.class)
        public AccessLogFilter accessLogFilter(WebAccessLogService webAccessLogService){
            AccessLogFilter filter =   new AccessLogFilter(webAccessLogService);
            filter.setOrder(accessLogProperty.getOrder());
            return  filter;
        }

        @Bean
//        @ConditionalOnMissingFilterBean(AccessLogFilter.class)
        public FilterRegistrationBean<AccessLogFilter> accessLogFilterFilterRegistrationBean(AccessLogFilter accessLogFilter){
            FilterRegistrationBean<AccessLogFilter> f = new FilterRegistrationBean(accessLogFilter);
            f.setName("accessLogFilter");
            f.setOrder(accessLogFilter.getOrder());
            return  f;
        }

        @Bean
//        @ConditionalOnBean(name = {"accessLogFilterFilterRegistrationBean"}, value = {TraceIdFilter.class})
//        @ConditionalOnMissingFilterBean(TraceIdFilter.class)
        public FilterRegistrationBean<TraceIdFilter> traceIdFilterFilterRegistrationBean(TraceIdFilter traceIdFilter){
            FilterRegistrationBean b =  new FilterRegistrationBean<>(traceIdFilter);
            b.setName("traceIdFilter");
            b.setOrder(traceIdFilter.getOrder());
            return  b;
        }

    }

    @Data
    @ConfigurationProperties("sketch.access-log")
    public static class AccessLogProperty{
        private String project;
        private int order = -201;

        @NestedConfigurationProperty
        private RemoteProperty remote;

    }

    @Data
    public static class RemoteProperty{
        String url = "http://accesslog.pingyee.com.cn";
        String saveUri = "/logs";
        String getUri = "/logs";
    }

    public static class RemoteWebAccessLogService implements AccessLogService{
        private AccessLogProperty accessLogProperty;
        private RestTemplate restTemplate;

        private  final  HttpHeaders applicationHttpHeaders;

        public RemoteWebAccessLogService(AccessLogProperty accessLogServiceProperty, RestTemplate restTemplate) {
            this.accessLogProperty = accessLogServiceProperty;
            this.restTemplate = restTemplate;

            this.applicationHttpHeaders = new HttpHeaders();
            applicationHttpHeaders.setContentType(MediaType.APPLICATION_JSON);
        }


        @Override
        public cn.com.pingyee.ddd.log.api.AccessLog save(cn.com.pingyee.ddd.log.api.AccessLog accessLog) {
           return restTemplate.postForObject(accessLogProperty.getRemote().url + accessLogProperty.getRemote().getSaveUri(),
                    new HttpEntity<>(accessLog,applicationHttpHeaders), AccessLog.class);
        }

        @Override
        public PageResult<? extends cn.com.pingyee.ddd.log.api.AccessLog> findAll(cn.com.pingyee.ddd.log.api.AccessLogQuery query) {
            Assert.isNotNull(query, "access log query can not be null");
            return restTemplate.getForObject(accessLogProperty.getRemote().url + accessLogProperty.getRemote().getGetUri(), PageResult.class, MapUtils.objectToMap(query));
        }

//        @Override
//        public String getCurrentUser() {
//            return Optional.ofNullable(UserContextHolder.getCurrentUser()).map(u->u.getId()).map(id->id.toString())
//                    .orElse(null);
//        }
//
//        @Override
//        public String getProject() {
//            return accessLogProperty.getProject();
//        }
    }

    public static class DelegateWebAccessLogService implements WebAccessLogService{
        private AccessLogService accessLogService;
        private  String project;

        public DelegateWebAccessLogService(AccessLogService accessLogService, String project) {
            this.accessLogService = accessLogService;
            this.project = project;
        }

        @Override
        public AccessLog save(AccessLog accessLog) {
            return accessLogService.save(accessLog);
        }

        @Override
        public PageResult<? extends AccessLog> findAll(AccessLogQuery accessLogQuery) {
            return accessLogService.findAll(accessLogQuery);
        }

        @Override
        public String getCurrentUser() {
            return Optional.ofNullable(UserContextHolder.getCurrentUser()).map(u->u.getId()).map(id->id.toString())
                    .orElse(null);
        }

        @Override
        public String getProject() {
            return project;
        }
    }
}
