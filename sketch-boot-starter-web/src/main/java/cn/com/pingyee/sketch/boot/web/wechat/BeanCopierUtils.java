package cn.com.pingyee.sketch.boot.web.wechat;

import cn.com.pingyee.sketch.core.util.Assert;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.cglib.core.Converter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author: Yi Ping
 * @date: 2019/11/18 0018 18:30
 * @since: 1.2.0
 */
public abstract class BeanCopierUtils {

    public final static Map<String, BeanCopier> BEAN_COPIER_MAP = new HashMap<>();

    public static void copy(Object source, Object target, Converter converter){
        Assert.isNotNull(source, "source can not be null");
        Assert.isNotNull(target, "target can not be null");

        boolean useconverter = null!=converter;
        BeanCopier copier = getBeanCopier(source.getClass(), target.getClass(),useconverter);
        copier.copy(source, target, converter);
    }

    public static void copy(Object source, Object target){
        copy(source, target, null);
    }

    public static BeanCopier getBeanCopier(Class<?> source, Class<?> target, boolean useConverter){
        String key = generateKey(source, target, useConverter);
        if(!BEAN_COPIER_MAP.containsKey(key)){
            BeanCopier copier = BeanCopier.create(source, target, useConverter);
            BEAN_COPIER_MAP.putIfAbsent(key, copier);
        }
        return BEAN_COPIER_MAP.get(key);
    }

    public static String generateKey(Class<?> source, Class<?> target, boolean useConverter){
//        return source.getName() + "->" + target.getName() + useConverter;
        return source.hashCode() + "->" + target.hashCode() + useConverter;
    }
}
