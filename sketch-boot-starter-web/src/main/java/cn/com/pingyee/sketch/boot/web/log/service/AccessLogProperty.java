package cn.com.pingyee.sketch.boot.web.log.service;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;

/**
 * @author: Yi Ping
 * @date: 2019/9/20 0020 23:18
 * @since: 1.0.0
 */

@Data
@ConfigurationProperties("sketch.accesslog")
public class AccessLogProperty {

    private int applicationCode;
    private String applicationName;

    @NestedConfigurationProperty
    private RemoteProperty remote;


    @Data
    public static class RemoteProperty{
        String url;
        String saveUri = "/logs";
        String getUri = "/logs";
    }
}
