package cn.com.pingyee.sketch.boot.web.wechat.pay;


import cn.com.pingyee.sketch.boot.web.wechat.pay.sdk.WXPay;

import java.util.HashMap;
import java.util.Map;

/**
 * @author: YiPing
 * @create: 2018-09-05 20:34
 * @since: 1.0
 */
public class WechatPayDemo {


    public static final  void main(String[] args){
        PayProperty config = new PayProperty("wx7eead13b24253eae", "1510778461", "8e15a3150e134a31bc3f9c97eabb943c");
        WXPay pay;
        try {
            pay = new WXPay(config);
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }

        Map<String, String> map = new HashMap<>(16);
        map.put("body", "会员充值10元");
        map.put("out_trade_no", "1234555333343adb");
        map.put("total_fee", 1+ "");
//        map.put("spbill_create_ip", "123.12.12.123");
        map.put("notify_url", "url");
        map.put("trade_type","JSAPI");
        map.put("openid", "ougJO5QwNDEe8paz1215Hrm0pp-A");
        try {
            Map<String, String> map1 = pay.unifiedOrder(map);
            System.out.println("returned codeStr was " + map1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
