package cn.com.pingyee.sketch.boot.web.wechat.miniprogram;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.apache.commons.lang3.time.DateUtils;

import java.io.Serializable;
import java.util.Date;

/**
 * @author: Yi Ping
 * @create: 2019-01-24 13:15
 * @since: 1.2.0
 */
@Data
public class AccessTokenResponse implements Serializable {

    @ApiModelProperty(value = "获取到的凭证")
    private String access_token;
    @ApiModelProperty(value = "凭证有效时间，单位：秒。目前是7200秒之内的值。")
    private long expires_in;
    @ApiModelProperty(value = "错误码")
    private long errcode;
    @ApiModelProperty(value = "错误信息")
    private String errmsg;
    @ApiModelProperty("失效时间")
    private Date expiresDate;

    public void setExpires_in(long expires_in) {
        this.expires_in = expires_in;
        this.expiresDate = DateUtils.addSeconds(new Date(), (int)expires_in);
    }

    public final boolean isExpired(){
        return  new Date().after(expiresDate);
    }
}
