package cn.com.pingyee.sketch.boot.web.log.trace;

import javax.servlet.http.HttpServletRequest;
import java.util.UUID;

/**
 * @author: Yi Ping
 * @date: 2019/6/12 0012 14:50
 * @since: 1.4.36
 */
public interface TraceIdGenerator {
    /**
     * <p> 生成新的 trace id</p>
     *
     * @param request
     * @return
     */
    String generate(HttpServletRequest request);

    class  DefaultTraceIdGenerator implements TraceIdGenerator{

        @Override
        public String generate(HttpServletRequest request) {
            return UUID.randomUUID().toString();
        }
    }
}
