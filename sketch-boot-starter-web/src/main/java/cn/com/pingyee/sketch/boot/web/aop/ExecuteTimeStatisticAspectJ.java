package cn.com.pingyee.sketch.boot.web.aop;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;

/**
 * @author: Yi Ping
 * @date: 2019/9/25 0025 18:11
 * @since: 1.0.0
 */

@Aspect
@Slf4j
public class ExecuteTimeStatisticAspectJ {

    @Pointcut(value = "@annotation(cn.com.pingyee.sketch.boot.web.aop.ExecuteTime)")
    public void pointcut(){

    }


    @Around(value = "pointcut()")
    public  Object pjp(ProceedingJoinPoint pjp) throws Throwable {
        long start = System.currentTimeMillis();
        Object result = null;
        try {
            result = pjp.proceed();
        } catch (Throwable throwable) {
            throw  throwable;
        }finally {
            if(log.isInfoEnabled()){
                long end = System.currentTimeMillis();
                log.info("execute method + " + pjp.getSignature().getName() + " cost " + (end- start) +"ms" );
            }
        }

        return  result;

    }

}
