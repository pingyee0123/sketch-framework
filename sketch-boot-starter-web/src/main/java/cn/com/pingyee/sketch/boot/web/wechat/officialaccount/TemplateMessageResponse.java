package cn.com.pingyee.sketch.boot.web.wechat.officialaccount;

import cn.com.pingyee.sketch.boot.web.wechat.WechatReponse;
import lombok.Data;

/**
 * <p>  模板消息响应 </p>
 * @author: Yi Ping
 * @create: 2019-01-24 15:45
 * @since: 1.2.0
 */

@Data
public class TemplateMessageResponse extends WechatReponse {

    private String msgid;
}
