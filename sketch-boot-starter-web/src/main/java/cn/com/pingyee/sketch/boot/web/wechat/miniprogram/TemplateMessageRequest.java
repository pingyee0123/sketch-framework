package cn.com.pingyee.sketch.boot.web.wechat.miniprogram;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.HashMap;
import java.util.Map;

/**
 * @author: Yi Ping
 * @create: 2019-01-24 14:03
 * @since: 1.2.0
 */
@Data
public class TemplateMessageRequest {
    @ApiModelProperty(value = "接收者（用户）的 openid", required = true)
    private String touser;
    @ApiModelProperty(value = "所需下发的模板消息的id", required = true)
    private String template_id;
    @ApiModelProperty(value = "点击模板卡片后的跳转页面，仅限本小程序内的页面。支持带参数,（示例index?foo=bar）。该字段不填则模板无跳转。")
    private String page;
    @ApiModelProperty(value = "表单提交场景下，为 submit 事件带上的 formId；支付场景下，为本次支付的 prepay_id", required = true)
    private String form_id;
    @ApiModelProperty(value = "模板内容，不填则下发空模板。具体格式请参考示例。")
    private Map data = new HashMap();
    @ApiModelProperty(value = "模板需要放大的关键词，不填则默认无放大")
    private String emphasis_keyword;

}
