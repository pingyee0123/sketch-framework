package cn.com.pingyee.sketch.boot.web.log.service;

import cn.com.pingyee.sketch.boot.web.MapUtils;
import cn.com.pingyee.sketch.boot.web.log.AccessLog;
import cn.com.pingyee.sketch.boot.web.log.AccessLogQuery;
import cn.com.pingyee.sketch.boot.web.log.AccessLogService;
import cn.com.pingyee.sketch.core.request.UserContextHolder;
import cn.com.pingyee.sketch.core.response.Page;
import cn.com.pingyee.sketch.core.util.Assert;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;

import java.util.Optional;

/**
 * @author: Yi Ping
 * @date: 2019/9/20 0020 23:17
 * @since: 1.0.0
 */

public class RemoteAccessLogService implements AccessLogService {

    private AccessLogProperty accessLogServiceProperty;
    private RestTemplate restTemplate;

    private HttpHeaders applicationHttpHeaders;

    public RemoteAccessLogService(AccessLogProperty accessLogServiceProperty, RestTemplate restTemplate) {
        this.accessLogServiceProperty = accessLogServiceProperty;
        this.restTemplate = restTemplate;
        initApplicatonHttpHeaders();
    }

    private void initApplicatonHttpHeaders() {
        this.applicationHttpHeaders = new HttpHeaders();
        applicationHttpHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8);
    }

    @Override
    public void save(AccessLog log) {
          restTemplate.postForEntity(accessLogServiceProperty.getRemote().url + accessLogServiceProperty.getRemote().getSaveUri(), new HttpEntity<>(log,applicationHttpHeaders),String.class);
    }

    @Override
    public Page<AccessLog> getAccessLogs(AccessLogQuery query) {
        // todo
        Assert.isNotNull(query, "access log query can not be null");
        return restTemplate.getForObject(accessLogServiceProperty.getRemote().url + accessLogServiceProperty.getRemote().getGetUri(),
                Page.class, MapUtils.objectToMap(query));
    }

    @Override
    public String getCurrentUser() {
        return Optional.ofNullable(UserContextHolder.getCurrentUser()).map(u->u.getId()).map(id->id.toString())
                .orElse(null);
    }

    @Override
    public String getAccessUserType() {
       return Optional.ofNullable(UserContextHolder.getCurrentUser()).map(u->u.getClass()).map(s->s.getName()).orElse(null);
    }

    @Override
    public int getProjectCode() {
        return accessLogServiceProperty.getApplicationCode();
    }

    @Override
    public String getProjectName() {
        return accessLogServiceProperty.getApplicationName();
    }
}
