package cn.com.pingyee.sketch.boot.web.wechat;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

/**
 * @author: Yi Ping
 * @create: 2019-01-24 18:07
 * @since: 1.2.0
 */
public abstract class Utils {

    public static HttpEntity wrapToApplicationJsonRequest(Object requestData){
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
        return  new HttpEntity(requestData, headers);
    }
}
