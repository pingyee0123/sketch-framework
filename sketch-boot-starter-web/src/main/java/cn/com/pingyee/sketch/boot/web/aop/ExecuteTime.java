package cn.com.pingyee.sketch.boot.web.aop;

import java.lang.annotation.*;

/**
 * @author: Yi Ping
 * @date: 2019/9/25 0025 18:19
 * @since: 1.0.0
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.TYPE})
public @interface ExecuteTime {

}
