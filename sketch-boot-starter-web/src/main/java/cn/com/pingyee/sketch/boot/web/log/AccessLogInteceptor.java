package cn.com.pingyee.sketch.boot.web.log;

import org.apache.commons.lang3.concurrent.BasicThreadFactory;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.concurrent.Executor;
import java.util.concurrent.ScheduledThreadPoolExecutor;

/**
 * @author: Yi Ping
 * @create: 2019-03-09 14:09
 * @since: 1.4.8
 */
public class AccessLogInteceptor implements HandlerInterceptor {

    private static final String START = "ACCESS_LOG_START";
    private static final String ACCESS_TIME = "ACCESS_LOG_ACCESS_TIME";

    /**
     * <p>  access log 服务</p>
     */
    private AccessLogService accessLogService;
    /**
     *  <p> 线程池</p>
     */
    private final Executor executor;


    public AccessLogInteceptor(AccessLogService accessLogService) {
        this(accessLogService, new ScheduledThreadPoolExecutor(
                3,
                new BasicThreadFactory.Builder().namingPattern("access-interceptor-").daemon(true).build() ));
    }

    public AccessLogInteceptor(AccessLogService accessLogService, Executor executor) {
        this.accessLogService = accessLogService;
        this.executor = executor;
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        request.setAttribute(START, System.currentTimeMillis());
        request.setAttribute(ACCESS_TIME, new Date());
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        AccessLog log = accessLogService.createLog(request,response);
        log.setAccessTime((Date) request.getAttribute(ACCESS_TIME));
        log.setRequestTime(System.currentTimeMillis() - (long) request.getAttribute(START));

        executor.execute(()-> accessLogService.save(log));

        request.removeAttribute(START);
        request.removeAttribute(ACCESS_TIME);
    }
}
