package cn.com.pingyee.sketch.boot.web;


import org.apache.commons.beanutils.BeanUtils;

import javax.validation.constraints.NotNull;
import java.util.Map;

/**
 * @author: YiPing
 * @create: 2018-09-04 11:57
 * @since: 1.0
 */
public class MapUtils {

    public static <T> T mapToObject(Map<String, String> map, @NotNull Class<T> beanClass) throws Exception {
        if (map == null) {
            return null;
        }
        T obj = beanClass.newInstance();
        BeanUtils.populate(obj, map);
        return obj;
    }

    public static Map<?, ?> objectToMap(Object obj) {
        if(obj == null) {
            return null;
        }

        return new org.apache.commons.beanutils.BeanMap(obj);
    }


}
