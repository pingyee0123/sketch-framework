package cn.com.pingyee.sketch.boot.web.wechat;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author: Yi Ping
 * @create: 2019-01-24 13:32
 * @since: 1.2.0
 */
@Data
public class WechatReponse {
    @ApiModelProperty(value = "错误码")
    private long errcode;
    @ApiModelProperty(value = "错误信息")
    private String errmsg;

    public boolean isSuccess(){
        return  errcode == 0;
    }
}
