package cn.com.pingyee.sketch.boot.web.wechat;


import cn.com.pingyee.sketch.boot.web.wechat.miniprogram.MiniProgramProperty;
import cn.com.pingyee.sketch.boot.web.wechat.officialaccount.OfficeAccountProperty;
import cn.com.pingyee.sketch.boot.web.wechat.pay.PayProperty;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;

/**
 * @author: YiPing
 * @create: 2018-08-23 09:29
 * @since: 1.0
 */
@Data
@ConfigurationProperties(prefix = "sketch.wechat" )
public class WechatProperty {

    @NestedConfigurationProperty
    private OfficeAccountProperty officeAccount = new OfficeAccountProperty();
    private PayProperty pay;
    @NestedConfigurationProperty
    private MiniProgramProperty miniProgram;

}
