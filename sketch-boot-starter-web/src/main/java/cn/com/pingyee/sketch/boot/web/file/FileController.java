package cn.com.pingyee.sketch.boot.web.file;

import cn.com.pingyee.sketch.core.service.FileService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotNull;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Base64;

/**
 * @author: Yi Ping
 * @date: 2019/11/12 0012 14:20
 * @since: 1.2.0
 */

@RequestMapping
@Api(tags = "0.文件上传")
@Validated
public class FileController {

    private FileService fileService;

    public FileController(FileService fileService) {
        this.fileService = fileService;
    }

    @PostMapping("/files")
    @ResponseBody
    public FileInfoApiResponse upload(@NotNull MultipartFile file, HttpServletRequest request) throws IOException {
        FileService.FileInfo info = fileService.upload(file.getInputStream(), file.getOriginalFilename());
        return new FileInfoApiResponse(info.getName(),getFullContextPath(request) +  info.getUrl());
    }

    @PostMapping("/base64files")
    @ApiOperation("base64文件上传")
    @ResponseBody
    public FileInfoApiResponse uploadByBase64(@RequestParam("file") @ApiParam("base64字符") String base64Str,
                                              HttpServletRequest request) throws IOException {
        // base64内容
        byte[] bytes = Base64.getDecoder().decode(base64Str.split(",")[1].getBytes("UTF-8"));
        // 文件后缀
        String suffix = "." +  base64Str.split(";")[0].split("/")[1];

        FileService.FileInfo info = fileService.upload(new ByteArrayInputStream(bytes), fileService.generateKey() +  suffix);
        return new FileInfoApiResponse(info.getName(), getFullContextPath(request)  + info.getUrl());

    }

    private static String getFullContextPath(HttpServletRequest request){
        return new StringBuilder(request.getScheme())
                .append("://")
                .append(request.getServerName())
                .append(80== request.getServerPort()?"":":" + request.getServerPort())
                .append("/")
                .append(request.getContextPath())
                .toString();
    }
}
