package cn.com.pingyee.sketch.boot.web;

import cn.com.pingyee.sketch.core.exception.*;
import cn.com.pingyee.sketch.core.response.ApiJson;
import cn.com.pingyee.sketch.core.util.Assert;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.web.ServerProperties;
import org.springframework.boot.autoconfigure.web.servlet.error.BasicErrorController;
import org.springframework.boot.autoconfigure.web.servlet.error.ErrorViewResolver;
import org.springframework.boot.web.servlet.error.ErrorAttributes;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.multipart.MaxUploadSizeExceededException;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ValidationException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author: Yi Ping
 * @create: 2018-11-22 16:40
 * @since: 1.0.0
 */

@Slf4j
public class SketchErrorController extends BasicErrorController {

    private ErrorAttributes errorAttributes;
    private boolean businessErrorReturns200 = false;
    private boolean alwaysReturns200=true;
    private Map<Class,ExceptionHandler> exceptionHandlers = new HashMap<>();

    public static Map<Class, ExceptionHandler>  initExceptionHandlers(){
        Map<Class, ExceptionHandler> exceptionHandlers = new HashMap<>(16);
        exceptionHandlers.put(Exception.class, new DefaultExceptionHandler());
        exceptionHandlers.put(ApiException.class, e-> ApiJson.exception((ApiException)e));
        exceptionHandlers.put(BindException.class, e->ApiJson.error("400007", getBindingResultErrorMessage(((BindException) e).getBindingResult())));
        exceptionHandlers.put(ValidationException.class, e->ApiJson.error("400008",e.getMessage()));
        exceptionHandlers.put(MaxUploadSizeExceededException.class, e->ApiJson.error("400009", e.getMessage()));
        exceptionHandlers.put(MissingServletRequestParameterException.class, e->ApiJson.error("400010", e.getMessage()));

        return  exceptionHandlers;
    }

    public <T extends  Exception> void  addExceptionHandler(Class<T> clz , ExceptionHandler<T> handler){
        exceptionHandlers.put(clz, handler);
    }

    public SketchErrorController(ErrorAttributes errorAttributes, ServerProperties serverProperties, List<ErrorViewResolver> errorViewResolvers){
        this(errorAttributes, serverProperties, errorViewResolvers, true);
    }



    public SketchErrorController(ErrorAttributes errorAttributes, ServerProperties serverProperties, List<ErrorViewResolver> errorViewResolvers, boolean businessErrorReturns200) {
        super(errorAttributes, serverProperties.getError(), errorViewResolvers);
        this.errorAttributes = errorAttributes;
        this.businessErrorReturns200 = businessErrorReturns200;
        if(!businessErrorReturns200){
            this.alwaysReturns200 =false;
        }
        exceptionHandlers.putAll(initExceptionHandlers());
    }

    @Override
    public ResponseEntity<Map<String, Object>> error(HttpServletRequest request) {
        if(log.isErrorEnabled()){
            log.error("access url \"{}\" encount error." ,request.getAttribute("javax.servlet.error.request_uri"));
        }
        return  ResponseEntity.status(getStatusValue(request)).body(getErrorAttributes(request));
    }



    @Override
    protected Map<String, Object> getErrorAttributes(HttpServletRequest request, boolean includeStackTrace) {
        Map<String,Object> map = new HashMap<>(16);
        ApiJson json;
        Throwable error = errorAttributes.getError(new ServletWebRequest(request));
        if(error != null) {
            if(! (error instanceof BusinessException)) {
                log.error(error.getMessage(), error);
            }
            json = handleException(error);
        }else {
            json= ApiJson.error("100001",getStatus(request).toString() + "; uri: " + request.getAttribute("javax.servlet.error.request_uri") );
        }
        map.putAll((Map<? extends String, ?>) MapUtils.objectToMap(json));
        map.remove("class");
//        map.put("path",request.getAttribute("javax.servlet.error.request_uri") )
        return  map;
    }

    private ApiJson handleException(Throwable error){
        Assert.isNotNull(error , " error can not be null");

        if(error instanceof Exception){
            Exception e = (Exception) error;
           return  findHandler(e.getClass()).handle(e);
        }else {
            return ApiJson.error("100001", error.getMessage());
        }
    }

    private ExceptionHandler findHandler(final  Class<? extends Exception> clazz ){
        return findHandler(clazz, exceptionHandlers);
    }

    public static ExceptionHandler findHandler(final  Class<? extends Exception> clazz, Map<Class, ExceptionHandler> exceptionHandlers){
        Assert.isTrue(exceptionHandlers != null && exceptionHandlers.containsKey(Exception.class), "exceptionHandlers should contains ke Exception.class");
        Class clz = clazz;
        ExceptionHandler h;
        while ((h=exceptionHandlers.get(clz)) == null){
            clz = clz.getSuperclass();
        }

        return h;
    }


    private Map<String, Object> getErrorAttributes(HttpServletRequest request){
        return  getErrorAttributes( request, false);
    }

    private int getStatusValue(HttpServletRequest request){
        if(alwaysReturns200){
            return  200;
        }

        Throwable e = errorAttributes.getError(new ServletWebRequest(request));
        if(e instanceof  ApiException) {
            if (e instanceof HttpClientException) {
                return  e instanceof ResourceNotFoundException ? 404: 400;
            } else if (e instanceof HttpServerException) {
                return 500;
            } else {
                return this.businessErrorReturns200?200:500;
            }
        }else if(e instanceof ValidationException || e instanceof BindException) {
            return this.businessErrorReturns200?200:400;
        }else {
            return  super.getStatus(request).value();
        }
    }


    public static String getBindingResultErrorMessage(BindingResult result){
        StringBuilder m = new StringBuilder();
        if(result.getFieldErrorCount()>0){
            result.getFieldErrors().stream().forEach(
                    e-> m/*.append(e.getObjectName()).append(".")*/.append(e.getField()).append(" ").append(e.getDefaultMessage()).append(";"));
        }else {
            result.getAllErrors().stream().forEach(e->
                m./*append(e.getObjectName()).*/append(e.getArguments()).append(" ").append(e.getDefaultMessage()).append(";"));
        }

        return m.toString();
    }

    @FunctionalInterface
    public interface ExceptionHandler<T extends Throwable>{

        ApiJson handle(T exception);
    }

    public static class DefaultExceptionHandler implements ExceptionHandler<Exception>{

        @Override
        public ApiJson handle(Exception exception) {
            log.error(exception.getMessage(),exception);
            return ApiJson.exception(exception);
        }
    };
}
