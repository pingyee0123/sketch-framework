package cn.com.pingyee.sketch.boot.web.wechat.officialaccount;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * <p> 模板消息请求 </p>
 * @author: Yi Ping
 * @create: 2019-01-24 15:36
 * @since: 1.2.0
 */

@Data
public class TemplateMessageRequest {
    @ApiModelProperty(value = "接收者openid", required = true)
    private String touser;
    @ApiModelProperty(value = "公众号模板id", required = true)
    private String template_id;
    @ApiModelProperty(value = "公众号模板消息所要跳转的url", required = false)
    private String url;
    @ApiModelProperty(value = "跳小程序所需数据，不需跳小程序可不用传该数据", required = false)
    private MiniProgram miniprogram;
    @ApiModelProperty(value = "公众号模板消息的数据", required = true)
    private Map data = new HashMap<>();

    public TemplateMessageRequest(@NotBlank String touser, @NotBlank  String template_id) {
        this.touser = touser;
        this.template_id = template_id;
    }

    public TemplateMessageRequest  addTemplateValue(String key, String value){
        this.data.put(key, new TemplateValue(value));
        return this;
    }

    public TemplateMessageRequest addTemplateValue(String key, String value, String color){
        this.data.put(key, new TemplateValue(value, color));
        return this;
    }

    @Data
    public static class MiniProgram{
        @ApiModelProperty(value = "所需跳转到的小程序appid（该小程序appid必须与发模板消息的公众号是绑定关联关系，暂不支持小游戏）", required = true)
        private String appid;
        @ApiModelProperty(value = "所需跳转到小程序的具体页面路径，支持带参数,（示例index?foo=bar），暂不支持小游戏")
        private String pagepath;

        public MiniProgram(String appid) {
            this.appid = appid;
        }

        public MiniProgram(String appid, String pagepath) {
            this.appid = appid;
            this.pagepath = pagepath;
        }
    }

    /**
     * @author: Yi Ping
     * @create: 2019-01-24 15:33
     * @since: 1.2.0
     */
    @Data
    public static class TemplateValue {

        private String value;
        private String color;

        public TemplateValue(String value, String color) {
            this.value = value;
            this.color = color;
        }

        public TemplateValue(String value) {
            this.value = value;
        }
    }
}
