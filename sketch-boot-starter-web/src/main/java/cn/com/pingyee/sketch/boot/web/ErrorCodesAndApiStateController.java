package cn.com.pingyee.sketch.boot.web;

import cn.com.pingyee.sketch.core.response.ErrorCodes;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

/**
 * @author: Yi Ping
 * @date: 2019/9/27 0027 19:53
 * @since: 1.0.0
 */

@Api(tags = "0.错误码,api说明")
@RequestMapping("/state")
public class ErrorCodesAndApiStateController {

    @ApiOperation("错误码")
    @GetMapping("/error-codes")
    @ResponseBody
    public Map<String, String> errorCodes(String code){
            return StringUtils.isBlank(code)? ErrorCodes.errorCodes():ErrorCodes.errorCode(code);
    }

    @GetMapping("/api-state")
    @ResponseBody
    @ApiOperation("api说明")
    public String state(){
        return  "1. 分页查询公共参数 \n" +
                "\t page: 页码, 0表示第一页， 默认为0, \n" +
                "\t size: 每页查询条数： 默认为10, \n" +
                "\t sort: 排序, 格式: propertyName[,asc|,desc] , eg: 按照订单时间(假设其属性名为orderTime)升序排列：sort=orderTime 或 sort=orderTime,asc," +
                "\t \t 按照订单时间降序排列： sort=orderTime,desc \n"
                +"2 查询参数一般会封装在一个Query里, 且为包装类型， 如果其值为null 表示不过滤该条件（即查询所有), \n" +
                "  \t 需要注意的是数组或集合参数，数组(集合)值为null,查询所有, 数组(集合)为空(.length=0 或.size=0), 查询结果为0条数据\n" +
                "   \t 查询条件之间为and关系  \n\n"
                ;
    }



}
