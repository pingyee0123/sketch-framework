package cn.com.pingyee.sketch.boot.web.log.trace;

import org.apache.commons.lang3.StringUtils;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static cn.com.pingyee.sketch.boot.web.log.AccessLog.TRACE_ID_ATTRIBUTE_NAME;
import static cn.com.pingyee.sketch.boot.web.log.AccessLog.TRACE_ID_HEADER;

/**
 * @author: Yi Ping
 * @date: 2019/6/12 0012 14:52
 * @since: 1.4.36
 */
public class TraceIdInteceptor implements HandlerInterceptor {

    private boolean autoGenerateTraceId;
    private TraceIdGenerator traceIdGenerator;

    public TraceIdInteceptor(boolean autoGenerateTraceId, TraceIdGenerator traceIdGenerator) {
        this.autoGenerateTraceId = autoGenerateTraceId;
        this.traceIdGenerator = traceIdGenerator;
    }

    public TraceIdInteceptor() {
        this(true, new TraceIdGenerator.DefaultTraceIdGenerator());
    }


    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String traceId = getTraceId(request);
        if(StringUtils.isBlank(traceId) && this.autoGenerateTraceId){
            traceId = traceIdGenerator.generate(request);
        }
        request.setAttribute(TRACE_ID_ATTRIBUTE_NAME, traceId);

        return true;
    }

    protected String getTraceId(HttpServletRequest request){
        return request.getHeader(TRACE_ID_HEADER);
    }


}
