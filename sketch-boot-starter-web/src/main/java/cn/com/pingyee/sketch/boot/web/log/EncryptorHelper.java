package cn.com.pingyee.sketch.boot.web.log;

import cn.com.pingyee.sketch.core.encrypt.AESEncryptor;
import cn.com.pingyee.sketch.core.util.Assert;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.UUID;

/**
 * @author: Yi Ping
 * @date: 2019/7/12 0012 14:28
 * @since: 1.4.44
 */
public final class EncryptorHelper {
    private static final String KEY = UUID.randomUUID().toString().substring(0,16);
    private static final AESEncryptor ENCRYPTOR;

    static {
        try {
            ENCRYPTOR = new AESEncryptor(KEY);
        } catch (Exception e) {
          throw new RuntimeException(e);
        }
    }



    public static String encrypt(String s) throws BadPaddingException, IllegalBlockSizeException {
        byte[] content = ENCRYPTOR.encrypt(s.getBytes());
        String encode = Base64.getEncoder().encodeToString(content);
//        String encode = new BASE64Encoder().encode(content);
        return new StringBuilder(encode).append("$").append(KEY).toString();
    }

    public static String decrypt(String encryptStr,String encryptKey) throws UnsupportedEncodingException, InvalidAlgorithmParameterException, NoSuchAlgorithmException, InvalidKeyException, NoSuchPaddingException, BadPaddingException, IllegalBlockSizeException {
        Assert.isNotBlank(encryptStr, "encryptStr  can not be blank");
        Assert.isNotBlank(encryptKey, "encryptKey can not be blank");

        final String key = encryptKey;
        final String encodeStr = encryptStr;
        AESEncryptor aesEncryptor = new AESEncryptor(key);
        byte[] decode = Base64.getDecoder().decode(encodeStr.getBytes());
        byte[] decrypt = aesEncryptor.decrypt(decode);
        return new String(decrypt);
    }

}
