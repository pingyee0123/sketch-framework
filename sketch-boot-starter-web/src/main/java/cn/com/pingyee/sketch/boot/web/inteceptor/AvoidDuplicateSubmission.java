package cn.com.pingyee.sketch.boot.web.inteceptor;

import java.lang.annotation.*;

/**
 * <p> 防重复提交
 * @see AvoidDuplicateSubmissionInterceptor
 * @author YiPing
 * @date 2019.11.11
 * </p>
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
public @interface AvoidDuplicateSubmission {
    /**
     * <p> 防重复提交时间间隔, 单位ms
     *  如果不指定 则由 {@link AvoidDuplicateSubmissionInterceptor 指定}
     * </p>
     */
    int value() default 0;

}
