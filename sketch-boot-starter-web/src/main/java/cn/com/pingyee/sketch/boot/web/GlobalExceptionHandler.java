package cn.com.pingyee.sketch.boot.web;

import cn.com.pingyee.sketch.core.exception.BusinessException;
import cn.com.pingyee.sketch.core.response.ApiResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.multipart.MaxUploadSizeExceededException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ValidationException;

/**
 * @author Yi Ping
 * @version 2.1
 * @date 2020/8/1 13:58
 */

@Slf4j
@RestControllerAdvice
public class GlobalExceptionHandler {


    @ExceptionHandler(value = Throwable.class)
    public ResponseEntity<ApiResponse> handleThrowable(Throwable e, HttpServletRequest r, HttpServletResponse response){
        return ResponseEntity.badRequest().body(ApiResponse.exception(e));
    }

    private ResponseEntity<ApiResponse> handleException(Throwable e){
        return handleThrowable(e,null, null);
    }

    @ExceptionHandler(value = BindException.class)
    public ResponseEntity<ApiResponse> handleBindException(BindException e){
        return handleException(new BusinessException("400007", ExceptionHandlerHelper.beautifyBindingResultErrorMessage(e)));
    }

    @ExceptionHandler(value = ValidationException.class)
    public ResponseEntity<ApiResponse> handleValidationException(ValidationException e){
        return handleException(new BusinessException("400008", e.getMessage()));
    }

    @ExceptionHandler(value = MaxUploadSizeExceededException.class)
    public ResponseEntity<ApiResponse> handleMaxUploadSizeExceededException(MaxUploadSizeExceededException e){
        return  handleException(new BusinessException("400009", e.getMessage()));
    }

    @ExceptionHandler(value = MissingServletRequestParameterException.class)
    public ResponseEntity<ApiResponse> handleMissingServletRequestParameterException(MissingServletRequestParameterException e){
        return  handleException(new BusinessException("400010", e.getMessage()));
    }
}
