package cn.com.pingyee.sketch.boot.web.inteceptor;

import cn.com.pingyee.sketch.core.model.User;
import cn.com.pingyee.sketch.core.request.UserContextHolder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author Yi Ping
 * @version 1.0
 * @date 2020/4/4 15:58
 */
@Slf4j
public  abstract class UserInterceptor implements HandlerInterceptor {

    private  boolean mustHavingOperator = false;

    public void setMustHavingOperator(boolean mustHavingOperator) {
        this.mustHavingOperator = mustHavingOperator;
    }


    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        User<?> user = getCurrentUser(request);
        if(user == null && mustHavingOperator){
            log.error(" 请求中缺少必要的用户信息 ");
            return false;
        }
        if(user != null) {
            UserContextHolder.setCurrentUser(user);
        }
        return true;
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler,  Exception ex) throws Exception {
        UserContextHolder.clear();
    }



    /**
     * <p> 从请求中获取用户信息 </p>
     * @param request
     * @return
     */
    protected  abstract User<?> getCurrentUser(HttpServletRequest request);



}
