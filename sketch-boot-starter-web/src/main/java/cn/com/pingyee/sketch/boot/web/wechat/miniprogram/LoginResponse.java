package cn.com.pingyee.sketch.boot.web.wechat.miniprogram;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * <p> 小程序登录的返回信息 </p>
 * @author: Yi Ping
 * @create: 2018-12-17 18:33
 * @since: 1.0.0
 */

@Data
public class LoginResponse implements Serializable {

    @ApiModelProperty(value = "用户唯一标识")
    private String openid;
    @ApiModelProperty(value = "会话密钥")
    private String session_key;
    @ApiModelProperty(value = "用户在开放平台的唯一标识符，在满足 UnionID 下发条件的情况下会返回，详见 UnionID 机制说明。")
    private String unionid;
    @ApiModelProperty(value = "错误码")
    private int errcode;
    @ApiModelProperty(value = "错误信息")
    private String errmsg;


    public boolean isSuccess(){
        return  errcode ==0;
    }
}
