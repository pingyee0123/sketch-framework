package cn.com.pingyee.sketch.boot.web.wechat.pay.sdk;

import lombok.Data;

/**
 * @author: Yi Ping
 * @date: 2019/7/8 0008 18:43
 * @since: 1.4.36
 */
@Data
public class UnifiedOrderResponse {


    private String return_code;
    private String return_msg;

    private String appid;
    private String mch_id;
    private String device_info;
    private String nonce_str;
    private String sign;
    private String result_code;
    private String err_code;
    private String err_code_des;

    private String trade_type;
    private String prepay_id;


    /**
     * <p> 是否交易成功 </p>
     * @return
     */
    public boolean isResultSuccess(){
        return isReturnSuccess() &&   "SUCCESS".equals(result_code);
    }

    /**
     * <p> 是否通讯成功 </p>
     * @return
     */
    public boolean isReturnSuccess(){
        return  "SUCCESS".equals(return_code);
    }
}
