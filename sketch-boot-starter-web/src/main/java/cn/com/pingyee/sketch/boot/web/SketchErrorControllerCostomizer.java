package cn.com.pingyee.sketch.boot.web;

/**
 * @author: Yi Ping
 * @date: 2019/11/5 0005 15:11
 * @since: 1.0.0
 */

@FunctionalInterface
public interface SketchErrorControllerCostomizer {

    void customize(SketchErrorController sketchErrorController);
}
