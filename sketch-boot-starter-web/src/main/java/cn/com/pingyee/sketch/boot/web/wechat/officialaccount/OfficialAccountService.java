package cn.com.pingyee.sketch.boot.web.wechat.officialaccount;

import cn.com.pingyee.sketch.boot.web.wechat.Utils;
import cn.com.pingyee.sketch.boot.web.wechat.WechatException;
import cn.com.pingyee.sketch.boot.web.wechat.WechatProperty;
import cn.com.pingyee.sketch.core.exception.ApiException;
import cn.com.pingyee.sketch.core.exception.BusinessException;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.text.MessageFormat;
import java.util.Date;


/**
 *
 * <p> 微信公众号服务, 管理接口调用</p>
 * @author: YiPing
 * @create: 2018-08-28 18:23
 * @since: 1.0
 */
//@Service
public   class OfficialAccountService {
    private static  final Logger logger = LoggerFactory.getLogger(OfficialAccountService.class);

    /** 授权access_token url **/
    private final  static  String SNS_ACCESS_TOKEN_URL = "https://api.weixin.qq.com/sns/oauth2/access_token?appid={APPID}&secret={SECRET}&code={CODE}&grant_type=authorization_code";
    private final static  String SNS_USER_INFO_URL = "https://api.weixin.qq.com/sns/userinfo?access_token={access_token}&openid={openid}&lang=zh_CN";

    private final  static String ACCESS_TOKEN_URL = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid={APPID}&secret={APPSECRET}";
    private final static String USER_INFO_URL = "https://api.weixin.qq.com/cgi-bin/user/info?access_token={ACCESS_TOKEN}&openid={OPENID}&lang=zh_CN";
    /** QR CODE 票据 **/
    private final static  String QRCODE_CREATE_TICKET = "https://api.weixin.qq.com/cgi-bin/qrcode/create?access_token={TOKEN}";
    /** 二维码 Url **/
    private final static String QRCODE = "https://mp.weixin.qq.com/cgi-bin/showqrcode?ticket={0}";
    /** 发送公众号模板消息 **/
    private final static String URL_TEMPLATE_MESSAGE = "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token={ACCESS_TOKEN}";

    private static final String ACCESS_TOKEN_KEY_PREFIX = "WECHAT_ACCESS_TOKEN_";
    private static  BaseAccessToken accessToken;


    RestTemplate restTemplate;
    WechatProperty wechat;
//    @Autowired
//    RedisTemplate redisTemplate;



    public OfficialAccountService(WechatProperty wechatProperty) {
        this.wechat = wechatProperty;
        this.restTemplate = new RestTemplate();
    }


    /**
     * <p> 获取 access_token</p>
     * @return
     */
    public String getAccessToken(){
        return  getBaseAccessToken();
    }

    /**
     * <p> 获取access_token， 获取顺序， 静态变量 accessToken, 缓存， 微信服务器</p>
     * <p> 须先设置微信公众平台ip白名单， 设置方法：
     *  微信公众平台--开发--基本设置</p>
     * @return
     */
    public  String getBaseAccessToken(){
        if(accessToken == null || accessToken.isExpired()){
            /** 缓存 accessToken 防止测试时系统频繁启动， 超出获取access_token的每日调用上限 **/
            accessToken = getBaseAccessTokenFromCache();
            if(accessToken == null || accessToken.isExpired()) {
                getAndCacheBaseAccessTokenFromWechatService();
            }
        }
        return  accessToken.getAccess_token();
    }

    /**
     * <p> 从微信服务器上获取 access_token 并缓存 </p>
     */
    private void getAndCacheBaseAccessTokenFromWechatService() {
        accessToken = getBaseAccessTokenFromWechatService();
        accessToken.setExpiredDate(DateUtils.addSeconds(new Date(), accessToken.getExpires_in()));
        cacheBaseAccessToken(accessToken);
    }

//    protected void cacheBaseAccessToken(BaseAccessToken accessToken) {
//        String key = generateAccessTokenCacheKey();
//        redisTemplate.opsForValue().set(key,accessToken);
//    }


    protected  void cacheBaseAccessToken(BaseAccessToken accessToken){};

    private String generateAccessTokenCacheKey() {
        return ACCESS_TOKEN_KEY_PREFIX + wechat.getOfficeAccount().getAppid();
    }

    private BaseAccessToken getBaseAccessTokenFromCache() {
        String key = generateAccessTokenCacheKey();
        return  getBaseAccessTokenFromCache(key);
//        if(redisTemplate.hasKey(key)){
//            return (BaseAccessToken) redisTemplate.opsForValue().get(key);
//        }
//        return null;
    }

    protected  BaseAccessToken getBaseAccessTokenFromCache(String key){
        return  null;
    };

    @Cacheable(value = "WECHAT_USER_INFO", key = "#openId")
    public UserInfo getUserInfo(String openId){
       return getUserInfoIntenal(getBaseAccessToken(), openId);
    }

    /**
     * <p> 从微信服务器上获取 access_token </p>
     * @return
     */
    private BaseAccessToken getBaseAccessTokenFromWechatService() {
        ResponseEntity<String> forEntity = restTemplate.getForEntity(ACCESS_TOKEN_URL, String.class, wechat.getOfficeAccount().getAppid(), wechat.getOfficeAccount().getSecret());
        String response = forEntity.getBody();
        JSONObject jsonObject = JSON.parseObject(response);
        if(jsonObject.get("errcode") != null){
            throw new BusinessException("100034",jsonObject.getString("errmsg"));
        }
        return  JSON.parseObject(response, BaseAccessToken.class);
    }

    /**
     * <p> 微信公众号， 用户点击授权的的回调处理接口 </p>
     * @param code 用户同意授权之后， 微信生成一个code用于获取访问用户信息的access_token
     * @param state
     * @return
     */
    public UserInfo authorize(String code, String state){

        ResponseEntity<String> response = restTemplate.getForEntity(SNS_ACCESS_TOKEN_URL, String.class,
                wechat.getOfficeAccount().getAppid(),
                wechat.getOfficeAccount().getSecret(),
                code);
        if(logger.isDebugEnabled()){
            logger.debug("response status was " + response.getStatusCode().value());
        }

        String responseBody =  response.getBody();
        JSONObject jsonObject = JSON.parseObject(responseBody, JSONObject.class);
        if( StringUtils.isNotBlank(jsonObject.getString("errcode"))){
            throw new ApiException(responseBody);
        }else {
            AccessToken accessToken = JSON.parseObject(responseBody, AccessToken.class);
            return  authorize(accessToken);
        }

    }

    private UserInfo authorize(AccessToken accessToken){
        return  getUserInfoIntenalWhenAuthorize(accessToken.getAccess_token(), accessToken.getOpenid());
    }

    /**
     * <p> 网页授权时， 获取用户信息</p>
     * @param access_token
     * @param openid
     * @return
     */
    private UserInfo getUserInfoIntenalWhenAuthorize(String access_token, String openid){

        String responseJson  = restTemplate.getForObject(SNS_USER_INFO_URL, String.class,access_token, openid);
        JSONObject jsonObject = JSON.parseObject(responseJson);
        if(jsonObject.get("errcode") ==null){
            try {
                String newJson = new String(responseJson.getBytes("ISO-8859-1"),"UTF-8");
                return  JSON.parseObject(newJson, UserInfo.class);
            } catch (UnsupportedEncodingException e) {
                return  JSON.parseObject(responseJson, UserInfo.class);
            }
        }else {
            throw new WechatException(responseJson);
        }
    }

    /**
     * <p> 正常情况下获取用户信息</p>
     * @param access_token
     * @param openid
     * @return
     */
    private UserInfo getUserInfoIntenal(String access_token, String openid){

        String responseJson  = restTemplate.getForObject(USER_INFO_URL, String.class,access_token, openid);
        JSONObject jsonObject = JSON.parseObject(responseJson);
        String errCode = jsonObject.getString("errcode");
        if(errCode !=null){
            if(errCode.equals("40001")){
                deleteAccessToken();
            }
            throw new BusinessException(responseJson);
        }else {
            try {
                String newJson = new String(responseJson.getBytes("ISO-8859-1"),"UTF-8");
                return  JSON.parseObject(newJson, UserInfo.class);
            } catch (UnsupportedEncodingException e) {
                return  JSON.parseObject(responseJson, UserInfo.class);
            }
        }
    }

    private void deleteAccessToken() {
        accessToken = null;
        deleteCachedAccessToken();
    }

    private void deleteCachedAccessToken(){
        String key = generateAccessTokenCacheKey();
        deleteCachedAccessToken(key);
    }

    protected  void deleteCachedAccessToken(String key){} ;


    private QrcodeTicketResponse getQrcodeTicket(QrcodeTicketRequest request){
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
        HttpEntity<String> entity = new HttpEntity<>(JSON.toJSONString(request), headers);
        String response = restTemplate.postForObject(QRCODE_CREATE_TICKET, entity, String.class, getBaseAccessToken());
        if(response.contains("errcode")){
            throw new BusinessException("100043", response);
        }else {
            return JSON.parseObject(response, QrcodeTicketResponse.class);
        }
    }

    /**
     * <p> 获取微信渠道二维码 </p>
     * @param request
     * @return
     */
    public QrcodeImage getQrcodeImage(QrcodeTicketRequest request){
        QrcodeTicketResponse ticket = getQrcodeTicket(request);
        return new QrcodeImage(getQrcodeTicket(request));
    }

    /**
     * <p> 发送微信模板消息 </p>
     * @param request
     */
    public void sendTemplateMessage(TemplateMessageRequest request){
//        HttpHeaders headers = new HttpHeaders();
//        headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
//        HttpEntity<String> entity = new HttpEntity<>(JSON.toJSONString(request), headers);
        TemplateMessageResponse response =  restTemplate.postForObject(URL_TEMPLATE_MESSAGE, Utils.wrapToApplicationJsonRequest(request), TemplateMessageResponse.class,getAccessToken());
        if(!response.isSuccess()){
            throw  new BusinessException("100047");
        }
    }

    @Data
    public static class BaseAccessToken implements Serializable {
        /** 获取到的凭证 **/
        String access_token;
        /** 	凭证有效时间，单位：秒  **/
        int expires_in;
        /** 结束日期 **/
        private Date expiredDate;

        public BaseAccessToken() {
            expiredDate = new Date();
        }


        public boolean isExpired(){
            return  new Date().after(expiredDate);
        }
    }


    /**
     * <p> 渠道二维码响应 </p>
     */
    @Data
    public static class QrcodeTicketResponse{
        @ApiModelProperty(value = "获取的二维码ticket，凭借此ticket可以在有效时间内换取二维码")
        private String ticket;
        @ApiModelProperty(value = "该二维码有效时间，以秒为单位。 最大不超过2592000（即30天）")
        private long expire_seconds;
        @ApiModelProperty(value = "二维码图片解析后的地址，开发者可根据该地址自行生成需要的二维码图片")
        private String url;

        public QrcodeTicketResponse() {
        }

        public QrcodeTicketResponse(String ticket, long expire_seconds, String url) {
            this.ticket = ticket;
            this.expire_seconds = expire_seconds;
            this.url = url;
        }
    }

    @Data
    public static class QrcodeImage extends QrcodeTicketResponse{
        @ApiModelProperty(value = "二维码url")
        private final String qrcodeImageUrl;

        public QrcodeImage(QrcodeTicketResponse ticketResponse) {
            super(ticketResponse.getTicket(), ticketResponse.getExpire_seconds(), ticketResponse.getUrl());
            this.qrcodeImageUrl = MessageFormat.format(QRCODE, getTicket());
        }
    }

    @ApiModel(description = "二维码请求说明")
    @Data
    public static class QrcodeTicketRequest{
        @ApiModelProperty(value = "该二维码有效时间，以秒为单位。 最大不超过2592000（即30天），此字段如果不填，则默认有效期为30秒。")
        private long expire_seconds;
        @ApiModelProperty(required = true,value = "二维码类型，QR_SCENE为临时的整型参数值，QR_STR_SCENE为临时的字符串参数值，QR_LIMIT_SCENE为永久的整型参数值，QR_LIMIT_STR_SCENE为永久的字符串参数值")
        private QrcodeType action_name;
        @ApiModelProperty(value = "二维码详细信息",required = true)
        private QrcodeActionInfo action_info;
    }

    @ApiModel(description = "二维码详细信息")
    @Data
    public static class QrcodeActionInfo{
        private QrcodeScene scene;
    }

    @ApiModel(description = "二维码场景")
    @Data
    public static class QrcodeScene{
        @ApiModelProperty(value = "场景值ID，临时二维码时为32位非0整型，永久二维码时最大值为100000（目前参数只支持1--100000）")
        private int scene_id;
        @ApiModelProperty(value = "场景值ID（字符串形式的ID），字符串类型，长度限制为1到64")
        private String scene_str;
    }

    public static enum  QrcodeType{
        QR_SCENE,
        QR_STR_SCENE,
        QR_LIMIT_SCENE,
        QR_LIMIT_STR_SCENE;
    }
}
