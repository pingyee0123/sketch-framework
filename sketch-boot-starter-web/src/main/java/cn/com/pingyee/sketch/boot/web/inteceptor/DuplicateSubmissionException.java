package cn.com.pingyee.sketch.boot.web.inteceptor;

import cn.com.pingyee.sketch.core.exception.BusinessException;
import cn.com.pingyee.sketch.core.response.ErrorCodes;

public class DuplicateSubmissionException extends BusinessException {

    public DuplicateSubmissionException() {
        super(ErrorCodes.DUPLICATE_SUBMISSION);
    }
}
