package cn.com.pingyee.sketch.boot.web.file;

import cn.com.pingyee.sketch.core.util.Assert;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author: Yi Ping
 * @date: 2019/11/12 0012 14:14
 * @since: 1.2.0
 */

@Data
@ConfigurationProperties(prefix = "sketch.file.local")
public class LocalFileProperty {

    private String savePath;
    private String urlPrefix = "files/";

    public void setSavePath(String savePath) {
        Assert.isTrue(StringUtils.isNotBlank(savePath), "save path was required");
        assertPathEndWithSlash(savePath);
        this.savePath = savePath;
    }

    public void setUrlPrefix(String urlPrefix) {
        Assert.isTrue(StringUtils.isNotBlank(urlPrefix), "url prefix was required");
        assertPathEndWithSlash(urlPrefix);
        this.urlPrefix = urlPrefix;
    }


    private void assertPathEndWithSlash(String savePath) {
        String suffix = "/";
        if(!savePath.endsWith(suffix)){
            throw new IllegalArgumentException("savePath should end with \"/\"");
        }
    }

}
