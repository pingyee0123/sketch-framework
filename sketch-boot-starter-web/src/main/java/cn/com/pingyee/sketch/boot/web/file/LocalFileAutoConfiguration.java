package cn.com.pingyee.sketch.boot.web.file;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * @author: Yi Ping
 * @date: 2019/11/12 0012 14:30
 * @since: 1.2.0
 */
@Configuration
@ConditionalOnProperty(prefix = "sketch.file.local", value = "save-path")
@Import(LocalFileConfigurerSupport.class)
public class LocalFileAutoConfiguration {
}
