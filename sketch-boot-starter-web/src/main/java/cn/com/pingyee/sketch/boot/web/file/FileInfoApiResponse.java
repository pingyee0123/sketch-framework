package cn.com.pingyee.sketch.boot.web.file;

import cn.com.pingyee.sketch.core.response.ApiResponse;
import cn.com.pingyee.sketch.core.service.FileService;

/**
 * @author: Yi Ping
 * @date: 2019/11/12 0012 14:21
 * @since: 1.2.0
 */
public class FileInfoApiResponse extends FileService.FileInfo implements ApiResponse {
    public FileInfoApiResponse(String name, String url) {
        super(name, url);
    }
}
