package cn.com.pingyee.sketch.boot.web;


import cn.com.pingyee.sketch.core.domain.CrudService;
import cn.com.pingyee.sketch.core.domain.PageQuery;
import cn.com.pingyee.sketch.core.domain.PageResult;
import cn.com.pingyee.sketch.core.exception.spec.EntityNotExistException;
import cn.com.pingyee.sketch.core.response.ApiResponse;
import cn.com.pingyee.sketch.core.validation.group.Create;
import cn.com.pingyee.sketch.core.validation.group.Update;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;


/**
 * @author: YiPing
 * @create: 2018-08-20 20:19
 * @since: 1.0
 */
@RequestMapping("/crud")
@Validated
public abstract class AbstractCrudController<T,ID extends Serializable, Q extends PageQuery, C, U> {

    @Autowired
    CrudService<T,ID, Q,C,U> curdService;

    @ResponseBody
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ApiOperation("查询")
    public T findById(@PathVariable("id") ID id) {
        T t =  curdService.findById(id);
        if(t==null){
            throw new EntityNotExistException("", id);
        }
        return t;
    }


    @ResponseBody
    @RequestMapping(method = RequestMethod.GET)
    @ApiOperation("分页查询")
    public PageResult<? extends  T> findAll(Q query) {
        return  curdService.findAll(query);
    }

    @ResponseBody
    @RequestMapping(method = RequestMethod.POST)
    @ApiOperation("新增")
    public T save(@Validated(value = Create.class)  C  c) {
        return curdService.save(c);
    }


    @ResponseBody
    @RequestMapping(method = RequestMethod.POST, consumes = "application/json")
    @ApiOperation("新增")
    public T  saveJson(@Validated(value = Create.class)  @RequestBody C  c) {
        return curdService.save(c);
    }

      @ResponseBody
      @PutMapping("/{id}")
      @ApiOperation("修改")
      public T update(@PathVariable("id") ID id, @Validated(value = Update.class) U u) {
        return curdService.update(id, u);
        }


    @ResponseBody
    @PutMapping(value = "/{id}", consumes = "application/json")
    @ApiOperation("修改")
    public T updateJson(
            @PathVariable("id") ID id,
            @Validated(value = Update.class) @RequestBody U u){
        return curdService.update(id, u);
    }


    @ResponseBody
    @DeleteMapping(value = "/{id}")
    @ApiOperation("删除")
    public ApiResponse delete(@PathVariable("id") ID id) {
        curdService.delete(id);
        return  ApiResponse.success();
    }


}
