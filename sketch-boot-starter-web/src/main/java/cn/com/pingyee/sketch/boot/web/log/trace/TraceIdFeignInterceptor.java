//package cn.com.pingyee.sketch.boot.web.log.trace;
//
//import com.zyun.common.log.AccessLog;
//import feign.RequestInterceptor;
//import feign.RequestTemplate;
//import org.apache.commons.lang3.StringUtils;
//import org.springframework.web.context.request.RequestContextHolder;
//import org.springframework.web.context.request.ServletRequestAttributes;
//
///**
// * @author: Yi Ping
// * @date: 2019/6/12 0012 14:47
// * @since: 1.4.36
// */
//public class TraceIdFeignInterceptor implements RequestInterceptor {
//
//    @Override
//    public void apply(RequestTemplate template) {
//        try {
//            ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
//            String trace_id = (String) requestAttributes.getRequest().getAttribute(AccessLog.TRACE_ID_ATTRIBUTE_NAME);
//            if (StringUtils.isNotBlank(trace_id)) {
//                template.header(AccessLog.TRACE_ID_HEADER, trace_id);
//            }
//        }catch (Exception e){
//            ;
//        }
//
//
//    }
//
//
//}
