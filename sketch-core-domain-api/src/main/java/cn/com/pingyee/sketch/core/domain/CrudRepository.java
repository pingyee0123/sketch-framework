package cn.com.pingyee.sketch.core.domain;

import java.io.Serializable;
import java.util.Objects;
import java.util.Optional;

/**
 * @author Yi Ping
 * @version 1.0
 * @date 2020/4/23 16:24
 */
public interface CrudRepository<T extends  Entity<ID>, ID extends Serializable, Q extends Query> {

    /**
     * 保存
     * @param
     */
    T save(T t);

    /**
     * 修改
     * @param
     * @return
     */
    T update(T t);

    /**
     * 物理删除
     * @param id
     * @return
     */
    boolean delete(ID id);

    /**
     * <p> 逻辑删除 </p>
     * @param id
     * @return
     */
    default boolean logicDelete(ID id){
        Objects.requireNonNull(id, "to call logic delete , it's id can not be null");
        return findById(id).map(t->t instanceof  LogicDeletable?t: null).map(t-> logicDelete(t)).orElse(false);
    };

    /**
     * 逻辑删除
     * @param t
     * @return
     */
    boolean logicDelete(T t);

    /**
     * 查询
     * @param id
     * @return
     */
    Optional<? extends  T> findById(ID id);

    /**
     * 分页查询
     * @param pageQuery
     * @return
     */
    PageResult<? extends  T> findAll(Q pageQuery);
}
