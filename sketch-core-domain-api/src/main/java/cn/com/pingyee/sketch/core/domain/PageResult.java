package cn.com.pingyee.sketch.core.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * <p>  分页返回结果 </p>
 *
 * @author Yi Ping
 * @version 1.0
 * @date 2020/4/23 16:13
 */
public interface PageResult<T> {

    /**
     * <p> 分页数据 </p>
     * {@link #getRecords()}
     * @return
     */


     List<T> getRecords();

    /**
     * <p> 总页数 </p>
     *
     * @return
     */
    long getTotalPages();

    /**
     * <p> 总记录数</p>
     *
     * @return
     */
    long getTotalElements();

    /**
     * <p> 当前页码 </p>
     *
     * @return
     */
    long getNumber();

    /**
     * <p> 每页返回记录数 </p>
     *
     * @return
     */
    long getSize();

    /**
     * <p> 是否为首页</p>
     */
    boolean isFirst();

    /**
     * <p> 是否为最后一页</p>
     */
    boolean isLast();

    /**
     * <p> 当前页返回的记录数</p>
     */
    long getNumberOfElements();


    static <R> PageResult<R> empty(){

        return  new PageResult<R>() {
            @Override
            public List<R> getRecords() {
                return  new ArrayList<>();
            }

            @Override
            public long getTotalPages() {
                return 0;
            }

            @Override
            public long getTotalElements() {
                return 0;
            }

            @Override
            public long getNumber() {
                return 0;
            }

            @Override
            public long getSize() {
                return 0;
            }

            @Override
            public boolean isFirst() {
                return true;
            }

            @Override
            public boolean isLast() {
                return true;
            }

            @Override
            public long getNumberOfElements() {
                return 0;
            }
        };
    }


    default  <R> PageResult<R> convert(Function<T, R> func){
        final PageResult<T> _this = this;
        DefaultPageResult<R>  result = new DefaultPageResult<>();
        result.setRecords(_this.getRecords()==null?new ArrayList(): _this.getRecords().stream().map(t->func.apply(t)).collect(Collectors.toList()));
        result.setTotalPages(_this.getTotalPages());
        result.setTotalElements(_this.getTotalElements());
        result.setNumber(_this.getNumber());
        result.setSize(_this.getSize());
        result.setFirst(_this.isFirst());
        result.setLast(_this.isLast());
        result.setNumberOfElements(_this.getNumberOfElements());

        return  result;

//        return  new PageResult<R>() {
////            @Override
//            public List<R> getRecords() {
//                return null== _this.getRecords()?new ArrayList<>():
//                        _this.getRecords().stream().map(t->func.apply(t)).collect(Collectors.toList());
//            }
//
//            public long getTotalPages() {
//                return _this.getTotalPages();
//            }
//
//            public long getTotalElements() {
//                return _this.getTotalElements();
//            }
//
//            public long getNumber() {
//                return _this.getNumber();
//            }
//
//            public long getSize() {
//                return _this.getSize();
//            }
//
//            public boolean isFirst() {
//                return _this.isFirst();
//            }
//
//            public boolean isLast() {
//                return _this.isLast();
//            }
//
//            public long getNumberOfElements() {
//                return _this.getNumberOfElements();
//            }
//        };
    }
}
