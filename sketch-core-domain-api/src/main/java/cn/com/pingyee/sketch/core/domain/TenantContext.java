package cn.com.pingyee.sketch.core.domain;

import java.io.Serializable;

/**
 * @author Yi Ping
 * @version 2.1
 * @date 2020/8/7 2:32
 */
public interface
TenantContext<ID extends Serializable> {


    ID getCurrentTenantId();


    ID getPlatformTenantId();

    /**
     * <p> 是否为平台租户， 即最高权限租户， 可以看到其他租户的数据</p>
     * @return
     */
    default boolean isPlatformTenant(){
        return  getCurrentTenantId().equals(getPlatformTenantId());
    }
}
