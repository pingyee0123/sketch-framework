package cn.com.pingyee.sketch.core.domain;

import java.io.Serializable;

/**
 * <p>  定义一个实体类， 与数据库表关联， 有Id</p>
 * @author Yi Ping
 * @version 1.0
 * @date 2020/4/23 16:06
 */
public interface Entity<ID extends Serializable> {

    /**
     * <p> 获取Id</p>
     * @return
     */
    ID getId();


    /**
     * <p>  设置Id </p>
     * @param id
     */
    void setId(ID id);

}
