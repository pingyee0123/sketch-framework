package cn.com.pingyee.sketch.core.domain;

import java.io.Serializable;

/**
 * @author Yi Ping
 * @version 2.1
 * @date 2020/8/7 0:42
 */
public interface Tenantable<ID extends Serializable> {

    ID getTenantId();

    void setTenantId(ID id);
}
