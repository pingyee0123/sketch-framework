package cn.com.pingyee.sketch.core.domain.model;


/**
 * @Date
 */
public interface Checkable {

    /**
     * <p> 审核操作 </p>
     * @param passed
     * @param remark
     */
    default void check(boolean passed, String remark){
        if(getStatus()== Status.CHECKING){
            setRemark(remark);
            setStatus(passed? Status.CHECKED: Status.FAILED);
        }else {
            throw new CheckException("this state is not supported of check operation");
        }
    }

    /**
     * <p> 获取当前状态 </p>
     * @return
     */
    Status getStatus();

    /**
     * <p> 设置状态 </p>
     * @param status
     */
     void setStatus(Status status);

    /**
     * <p> 填写审核内容 </p>
     * @param remark
     */
    void setRemark(String remark);

    enum Status{
        /** 审核中 **/
        CHECKING,
        /** 审核通过 **/
        CHECKED,
        /** 审核失败 **/
        FAILED;
    }


    class CheckException extends UnsupportedOperationException{

        public CheckException(String message) {
            super(message);
        }

    }
}