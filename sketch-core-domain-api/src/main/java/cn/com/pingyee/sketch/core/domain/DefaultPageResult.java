package cn.com.pingyee.sketch.core.domain;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * @author Yi Ping
 * @version 1.0
 * @date 2020/4/23 16:20
 */
@Data
public class DefaultPageResult<T>  implements PageResult<T>, Serializable {

    @ApiModelProperty("返回数据")
    private List<T> records = new ArrayList();
    @ApiModelProperty("总页数")
    private long totalPages;
    @ApiModelProperty("总记录数")
    private long totalElements;
    @ApiModelProperty("当前页码")
    private long number;
    @ApiModelProperty("每页返回记录数")
    private long size;
    @ApiModelProperty("当前页是否为首页")
    private boolean first;
    @ApiModelProperty("当前页是否为最后一页")
    private boolean last;
    @ApiModelProperty("当前页返回的记录数")
    private long numberOfElements;

    public DefaultPageResult() {
    }

    public DefaultPageResult(List<T> content, int page, int size) {
        Objects.requireNonNull(content, "content can not be null");
        int p = page < 0 ? 0 : page;
        int s = size < 1 ? 1 : size;
        this.totalPages = (int) Math.ceil((double)content.size() / (double)s);
        this.totalElements = content.size();
        this.number = p >= this.totalPages ? this.totalPages - 1 : p;
        this.size = s;
        this.first = this.number == 0;
        this.last = this.number == this.totalPages - 1;
        this.records = content.subList((int) (this.number * this.size), (int) Math.min((this.number + 1) * this.size, content.size()));
        this.numberOfElements = this.records.size();
    }



}
