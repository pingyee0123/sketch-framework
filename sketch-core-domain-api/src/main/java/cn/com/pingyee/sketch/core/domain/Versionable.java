package cn.com.pingyee.sketch.core.domain;

/**
 * @author Yi Ping
 * @version 2.1
 * @date 2020/8/1 14:45
 */
public interface Versionable {

    Long getVersion();

    void setVersion(Long v);
}
