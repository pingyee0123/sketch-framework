package cn.com.pingyee.sketch.core.domain;

/**
 * @author Yi Ping
 * @version 1.0
 * @date 2020/4/20 16:54
 */
public interface LogicDeletable {

    boolean isDeleted();

    void markDeleted();
}
