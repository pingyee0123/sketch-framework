package cn.com.pingyee.sketch.core.domain;


/**
 * @author Yi Ping
 * @version 1.0
 * @date 2020/4/22 9:55
 */
public interface HasCode extends PrePersist {

    /**
     * <p> 设置编码 </p>
     * @param code
     */
    void setCode(String code);

    /**
     * <p> 获取编码 </p>
     * @return
     */
    String getCode();

    /**
     * <p> 设置默认编码 </p>
     */
     void setCode();

    /**
     * <p>持久化前出事化操作  </p>
     */
    @Override
    default  void prePersist(){
        this.setCode();
    }
}
