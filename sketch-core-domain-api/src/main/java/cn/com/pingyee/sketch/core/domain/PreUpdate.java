package cn.com.pingyee.sketch.core.domain;

/**
 * @author Yi Ping
 * @version 1.0
 * @date 2020/4/24 8:57
 */

public interface PreUpdate {

    /**
     * <p> 更新操作前 执行，如 初始化 updateDate, updateUserId</p>
     */
    void preUpdate();

}
