package cn.com.pingyee.sketch.core.domain;

/**
 * @author Yi Ping
 * @version 2.2
 * @date 2020/12/2 18:38
 */
public interface LongIdTenantContext extends TenantContext<Long>{

    Long DEFAULT_PLATFORM_TENANT_ID = 1L;

    @Override
    Long getCurrentTenantId();

    @Override
    default Long getPlatformTenantId(){
        return  DEFAULT_PLATFORM_TENANT_ID;
    }

    @Override
    default boolean isPlatformTenant() {
        return getCurrentTenantId().equals(getPlatformTenantId());
    }
}
