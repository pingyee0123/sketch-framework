package cn.com.pingyee.sketch.core.domain;

import java.io.Serializable;

/**
 * @author Yi Ping
 * @version 1.0
 * @date 2020/5/4 13:19
 */
public interface CommandService<T, ID extends Serializable, C,  U > {

    T  save(C c);

    T update(ID id, U u);

    void delete(ID id);
}
