package cn.com.pingyee.sketch.core.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.Optional;

/**
 * @author Yi Ping
 * @version 2.1
 * @date 2020/8/1 14:37
 */

@javax.persistence.MappedSuperclass
@javax.persistence.EntityListeners(org.springframework.data.jpa.domain.support.AuditingEntityListener.class)
public abstract class AbstractEntity implements Entity<Long>, Auditable<Long>, Serializable {

    private Long id;
    private Long createUser;
    private Date createDate;
    private Long lastModifiedUser;
    private Date lastModifiedDate;

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(generator = "snowflakeIdGenerator")
    @org.hibernate.annotations.GenericGenerator(name = "snowflakeIdGenerator" , strategy = "cn.com.pingyee.sketch.core.domain.jpa.SnowflakeIdentityGenerator")
    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @org.springframework.data.annotation.CreatedBy
    @Override
    public Long getCreateUser() {
        return createUser;
    }

    public void setCreateUser(Long createUser) {
        this.createUser = createUser;
    }

    @org.springframework.data.annotation.CreatedDate
    @Override
    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    @org.springframework.data.annotation.LastModifiedBy
    @Override
    public Long getLastModifiedUser() {
        return lastModifiedUser;
    }

    public void setLastModifiedUser(Long lastModifiedUser) {
        this.lastModifiedUser = lastModifiedUser;
    }

    @org.springframework.data.annotation.LastModifiedDate
    @Override
    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public boolean equals(AbstractEntity other) {
        return Optional.ofNullable(other).map(s->s.getId()).map(s->s.equals(getId())).orElse(false);
    }
}
