package cn.com.pingyee.sketch.core.domain;

import java.io.Serializable;

/**
 * <p> 基础的增删改查 </p>
 * @author Yi Ping
 * @version 1.0
 * @date 2020/5/4 13:13
 *
 */
public interface CrudService<T,ID extends Serializable, Q extends PageQuery, C, U>
    extends QueryService<T,ID, Q>,
    CommandService<T, ID, C,U>{


}
