package cn.com.pingyee.sketch.core.domain.enums;

/**
 * @author: Yi Ping
 * @date: 2019/9/6 0006 14:19
 * @since: 1.4.44
 */
public enum Gender {
    FEMALE,
    MALE
}
