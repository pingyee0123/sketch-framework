package cn.com.pingyee.sketch.core.domain;

import java.io.Serializable;

/**
 * @author Yi Ping
 * @version 1.0
 * @date 2020/5/4 13:18
 */
public interface QueryService<T, ID extends Serializable, Q extends PageQuery> {

    /**
     * <p> 分页查询 </p>
     * @param query
     * @return
     */
    PageResult<? extends T> findAll(Q query);

    /**
     * <p> 根据Id 查询</p>
     * @param id
     * @return  如果 id为null或查询不到结果则返回null,否则返回查询到的结果 T
     */
    T findById(ID id);
}
