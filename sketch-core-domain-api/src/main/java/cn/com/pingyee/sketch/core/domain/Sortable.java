package cn.com.pingyee.sketch.core.domain;

import java.math.BigDecimal;

/**
 * @author Yi Ping
 * @version 1.0
 * @date 2020/4/22 10:10
 */
public interface Sortable extends PrePersist {
    BigDecimal DEFAULT_SORT = BigDecimal.ZERO;

    /**
     * <p> 获取记录的顺序号 </p>
     * @return
     */
    BigDecimal getSort();


    /**
     * <p> 设置记录的顺序号 </p>
     * @param sort
     */
    void setSort(BigDecimal sort);


    /**
     * <p> 设置默认的顺序号 </p>
     */
    default  void setSort(){
        if(null == getSort()) {
            setSort(DEFAULT_SORT);
        }
    }

    /**
     * <p> 新增记录， 持久化前调用 </p>
     */
    @Override
    default  void prePersist(){
        setSort();
    }
}
