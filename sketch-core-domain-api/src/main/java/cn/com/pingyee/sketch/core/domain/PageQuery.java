package cn.com.pingyee.sketch.core.domain;

/**
 * @author Yi Ping
 * @version 1.0
 * @date 2020/4/21 16:30
 */
public interface PageQuery extends Query  {

    /**
     * <p> 获取页码 </p>
     * @return
     */
    long getPage();

    /**
     * <p> 每页记录数 </p>
     * @return
     */
    long getSize();

    /**
     * <p> 排序 </p>
     * @return
     */
    String getSort();

    /**
     * <p> 分页查询是否 执行count </p>
     * @return
     */
    boolean isSearchCount();


}
