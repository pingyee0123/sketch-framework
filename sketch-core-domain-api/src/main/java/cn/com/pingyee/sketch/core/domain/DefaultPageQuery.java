package cn.com.pingyee.sketch.core.domain;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author Yi Ping
 * @version 1.0
 * @date 2020/4/23 16:22
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DefaultPageQuery implements PageQuery, Serializable {

    @ApiModelProperty(value = "页码, 0为第一页, 默认0", example = "0")
    private long page =0;
     @ApiModelProperty(value = "每页显示多少条, 默认10条", example = "10")
    private long size =10;
     @ApiModelProperty("排序, eg: 按id降序排列: id,desc; 按id升序排列:id,asc 或 id； 按id, addTime降序排列：id, addTime,desc ")
    private String sort;

    private boolean searchCount;

}
