package cn.com.pingyee.sketch.core.domain;

/**
 * @author Yi Ping
 * @version 1.0
 * @date 2020/4/22 9:54
 */
public interface PrePersist {

    /**
     * <p> 新增记录， 持久化前调用 </p>
     */
    void prePersist();
}
