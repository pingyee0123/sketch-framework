package cn.com.pingyee.sketch.core.domain.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author: Yi Ping
 * @create: 2019-03-30 20:10
 * @since: 2.1
 */

public interface TreeNode<ID extends Serializable> {

    /**
     * <p> 获取其父节点标识, 如pid</p>
     * <p> 返回标识的好处是, 避免树太深， 导致json串很长</p>
     * @return
     */
    ID getPid();

    /**
     * <p> 修改其父节点 </p>
     * @param pid
     * @return
     */
    void setPid(ID pid);

    /**
     *
     * @return 返回树的标识， 返回值不能为null
     */
    ID getId();

    /**
     * <p> 是否为根节点 </p>
     * @return
     */
    default boolean isRoot(){
       return null == getPid();
    }

    /**
     * <P> 获取其孩子元素， 不能返回null， 如果为空返回一个空集合 </P>
     * @return
     */
  <S extends TreeNode<ID>>  Set<S> getChildren();

    /**
     * <p> 添加子树</p>
     * @param  tree 字数
     */
    default <S extends TreeNode<ID>> void  addChild(S tree){

        tree.setPid(this.getId());
        getChildren().add(tree);
    }

    /**
     * <p> 获取家族成员 </p>
     * @return
     */
    default  Set<TreeNode> family(){
        Set<TreeNode> set = new HashSet<>();
        set.add(this);
        set.addAll(getChildren());
        getChildren().forEach(t->set.addAll(t.family()));
        return  set;
    }


    /**
     * <p> 将trees 组织成树并返回根节点 </p>
     * @param trees
     * @return
     */
    static <ID extends Serializable, S extends TreeNode<ID>>  Set<S> organizeAndGetRoots(Set<S> trees){
        Map<ID, S> map = trees.stream().collect(Collectors.toMap(TreeNode::getId, t -> t));
        map.forEach((k,v)->{
            if(!v.isRoot() && null != map.get(v.getPid())){
                map.get(v.getPid()).addChild(v);
            }
        });
        return map.values().stream().filter(TreeNode::isRoot).collect(Collectors.toSet());
    }
}

