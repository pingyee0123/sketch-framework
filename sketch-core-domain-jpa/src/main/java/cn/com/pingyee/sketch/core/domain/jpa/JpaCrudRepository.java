package cn.com.pingyee.sketch.core.domain.jpa;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.io.Serializable;

/**
 * @author Yi Ping
 * @version 1.0
 * @date 2020/7/31 16:39
 */
public interface JpaCrudRepository<T,ID extends Serializable> extends JpaRepository<T,ID>, JpaSpecificationExecutor<T> {

}
