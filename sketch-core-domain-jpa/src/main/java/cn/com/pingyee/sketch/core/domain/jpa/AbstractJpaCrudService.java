package cn.com.pingyee.sketch.core.domain.jpa;

import cn.com.pingyee.sketch.core.domain.CrudService;
import cn.com.pingyee.sketch.core.domain.LogicDeletable;
import cn.com.pingyee.sketch.core.domain.PageQuery;
import cn.com.pingyee.sketch.core.domain.PageResult;
import cn.com.pingyee.sketch.core.exception.spec.EntityNotExistException;
import cn.com.pingyee.sketch.core.util.Assert;
import cn.com.pingyee.sketch.core.util.ReflectUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.Objects;

/**
 * @author Yi Ping
 * @version 2.1
 * @date 2020/7/31 16:16
 * @param  <T> 数据库对象
 */
public abstract class AbstractJpaCrudService<T,R,ID extends Serializable,Q extends PageQuery,C,U>
        implements CrudService<R,ID,Q,C,U>
     {
          private final static Logger log = LoggerFactory.getLogger(AbstractJpaCrudService.class);
          final Class<T> domainClass;
          final JpaCrudRepository<T,ID> jpaCrudRepository;

          public AbstractJpaCrudService(JpaCrudRepository<T, ID> jpaCrudRepository/*, IdGenerator<ID> idGenerator*/) {
               Assert.isNotNull(jpaCrudRepository, "jpaCrudRepository can not be null");
               this.jpaCrudRepository = jpaCrudRepository;

               ParameterizedType type = (ParameterizedType) this.getClass().getGenericSuperclass();
               this.domainClass = (Class<T>) type.getActualTypeArguments()[0];
          }


          protected abstract T convertC2T(C c);
          protected abstract  R convertT2R(T t);
          protected abstract T convertU2T(U u);
          protected abstract Specification<T> toSpecification(Q q);

          @Override
          @Transactional(rollbackFor = Exception.class)
          public R save(C c) {
               Assert.isNotNull(c, " saved object can not be null");
               return convertT2R(jpaCrudRepository.save(convertC2T(c)));
          }

          @Transactional(rollbackFor = Exception.class)
          @Override
          public R update(ID id, U u) {
               if(null == u){
                    return null;
               }
               Assert.isNotNull(id, "id can not be null");

               T t = jpaCrudRepository.findById(id).orElseThrow(() -> new EntityNotExistException(domainClass, id));
               ReflectUtils.copyProperties(t, Objects.requireNonNull(convertU2T(u)));
               return convertT2R(jpaCrudRepository.save(t));
          }

          @Transactional(rollbackFor = Exception.class)
          @Override
          public void delete(ID id) {
               if(id != null) {
                    jpaCrudRepository.deleteById(id);
               }
          }

          protected  void logicDelete(ID id){
               if(id!= null){
                    T t = jpaCrudRepository.findById(id).orElse(null);
                    if(t instanceof LogicDeletable){
                         ((LogicDeletable) t).markDeleted();
                         jpaCrudRepository.save(t);
                    }else {
                         // todo 是否抛出异常
                    }
               }
          }

          @Override
          public PageResult<? extends R> findAll(Q q) {
               Assert.isNotNull(q, "query can not be null");
               Page<T> jpaPage = jpaCrudRepository.findAll(toSpecification(q), JpaUtils.toJpaPageable(q));
               return new JpaPageResult<>(jpaPage).convert(this::convertT2R);
          }

          @Override
          public  R findById(ID id) {
               return id== null? null:
                       jpaCrudRepository.findById(id).map(t-> convertT2R(t)).orElse(null);
//               return jpaCrudRepository.findById(id).map(t-> convertT2R(t)).orElseThrow(()-> new EntityNotExistException(domainClass, id));
          }

          protected final JpaCrudRepository<T,ID> getJpaCrudRepository(){
               return jpaCrudRepository;
          }


     }
