package cn.com.pingyee.sketch.core.domain.jpa;

import cn.com.pingyee.sketch.core.domain.PageQuery;

/**
 * @author Yi Ping
 * @version 2.1
 * @date 2020/8/4 1:38
 */
public abstract class Abstract2SimpleJpaCrudService<T, Q extends PageQuery> extends AbstractJpaCrudService<
        T,T, java.lang.Long, Q,T,T> {

    public Abstract2SimpleJpaCrudService(JpaCrudRepository<T, Long> jpaCrudRepository) {
        super(jpaCrudRepository);
    }

    @Override
    protected final T convertC2T(T t) {
        return t;
    }

    @Override
    protected final T convertT2R(T t) {
        return t;
    }

    @Override
    protected final T convertU2T(T t) {
        return t;
    }

}
