package cn.com.pingyee.sketch.core.domain.jpa;

import cn.com.pingyee.sketch.core.domain.PageQuery;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 * @author Yi Ping
 * @version 2.1
 * @date 2020/8/3 21:34
 */
public interface JpaQuery<T> extends PageQuery, Specification<T> {

    default Pageable toJpaPageable(){
        return JpaUtils.toJpaPageable(this);
    }

    @Override
    Predicate toPredicate(Root<T> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder cb);
}
