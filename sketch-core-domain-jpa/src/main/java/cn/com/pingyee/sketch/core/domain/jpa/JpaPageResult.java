package cn.com.pingyee.sketch.core.domain.jpa;

import cn.com.pingyee.sketch.core.domain.PageResult;
import org.springframework.data.domain.Page;

import java.io.Serializable;
import java.util.List;

/**
 * @author Yi Ping
 * @version 1.0
 * @date 2020/7/31 19:15
 */
public class JpaPageResult<T> implements PageResult<T>, Serializable {
    private final Page<T> page;

    public JpaPageResult(Page<T> page) {
        this.page = page;
    }


    @Override
    public List<T> getRecords() {
        return page.getContent();
    }

    @Override
    public long getTotalPages() {
        return page.getTotalPages();
    }

    @Override
    public long getTotalElements() {
        return page.getTotalElements();
    }

    @Override
    public long getNumber() {
        return page.getNumber();
    }

    @Override
    public long getSize() {
        return page.getSize();
    }

    @Override
    public boolean isFirst() {
        return page.isFirst();
    }

    @Override
    public boolean isLast() {
        return page.isLast();
    }

    @Override
    public long getNumberOfElements() {
        return page.getNumberOfElements();
    }
}
