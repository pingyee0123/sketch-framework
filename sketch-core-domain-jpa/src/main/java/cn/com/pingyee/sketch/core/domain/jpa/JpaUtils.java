package cn.com.pingyee.sketch.core.domain.jpa;

import cn.com.pingyee.sketch.core.domain.DefaultPageResult;
import cn.com.pingyee.sketch.core.domain.PageQuery;
import cn.com.pingyee.sketch.core.domain.PageResult;
import cn.com.pingyee.sketch.core.util.Assert;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * @author Yi Ping
 * @version 1.0
 * @date 2020/7/31 17:59
 */
public abstract class JpaUtils {

    public static Pageable toJpaPageable(PageQuery query){
        Assert.isNotNull(query, "query can not be null");

        String sort = query.getSort();
        String[] sorts;
        Sort.Direction direct;
        Sort sortObject = null;
        if(StringUtils.isNotBlank(sort)){
            String[] split = sort.split(",");
            String directStr = split[split.length - 1];
            if(Sort.Direction.DESC.name().equalsIgnoreCase(directStr)){
                if(split.length -1 >0) {
                    direct = Sort.Direction.DESC;
                    sorts = Arrays.copyOfRange(split, 0, split.length - 1);
                    sortObject = Sort.by(direct,sorts);
                }
            }else if(Sort.Direction.ASC.name().equalsIgnoreCase(directStr)) {
                if(split.length -1 >0) {
                    direct = Sort.Direction.ASC;
                    sorts = Arrays.copyOfRange(split, 0, split.length - 1);
                    sortObject = Sort.by(direct,sorts);
                }
            }else {
                direct = Sort.Direction.ASC;
                sorts = Arrays.copyOfRange(split,0,split.length);
                sortObject = Sort.by(direct,sorts);
            }

        }

        return sortObject == null? org.springframework.data.domain.PageRequest.of((int) query.getPage(), (int) query.getSize())
                : org.springframework.data.domain.PageRequest.of((int) query.getPage(), (int) query.getSize(),sortObject);
    }

    public static Predicate andPredicates(List<Predicate> predicates, CriteriaBuilder builder){
        if(predicates == null || predicates.size() ==0){
            return  null;
        }
        Iterator<Predicate> it = predicates.iterator();
        Predicate p = it.next();
        while (it.hasNext()){
            p = builder.and(p, it.next());
        }
        return p;
    }

    public static <T extends Comparable<T>>  Predicate between(Path<T> expression, T start, T end, CriteriaBuilder cb){
            return null == start?
                    (null== end? null: cb.lessThanOrEqualTo(expression, end)):
                    (null == end? cb.greaterThanOrEqualTo(expression, start): cb.between(expression, start,end));

    }

    public static <T> PageResult<T> fromJpaPage(Page<T> page){
        DefaultPageResult  result = new DefaultPageResult();
        result.setRecords(page.getContent());
        result.setTotalPages(page.getTotalPages());
        result.setTotalElements(page.getTotalElements());
        result.setNumber(page.getNumber());
        result.setSize(page.getSize());
        result.setFirst(page.isFirst());
        result.setLast(page.isLast());
        result.setNumberOfElements(page.getNumberOfElements());

        return  result;
//        return new JpaPageResult<>(page);
    }

}
