package cn.com.pingyee.sketch.core.domain.jpa;

import cn.com.pingyee.sketch.core.domain.PageQuery;

/**
 * @author Yi Ping
 * @version 2.1
 * @date 2020/8/4 2:03
 */
public abstract class AbstractInheritApiSimpleJpaCrudService<T extends R, R,Q extends PageQuery >
    extends AbstractJpaCrudService<T,R,Long,Q,R,R>
{
    public AbstractInheritApiSimpleJpaCrudService(JpaCrudRepository<T, Long> jpaCrudRepository) {
        super(jpaCrudRepository);
    }

    protected abstract T convert2T(R r);

    @Override
    protected T convertC2T(R r) {
        return convert2T(r);
    }

    @Override
    protected R convertT2R(T t) {
        return t;
    }

    @Override
    protected T convertU2T(R r) {
        return convert2T(r);
    }

}
