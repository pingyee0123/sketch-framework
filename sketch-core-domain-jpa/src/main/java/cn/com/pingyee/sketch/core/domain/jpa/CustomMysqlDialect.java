package cn.com.pingyee.sketch.core.domain.jpa;

import org.hibernate.dialect.MySQL57InnoDBDialect;

/**
 * @author Yi Ping
 * @version 2.0
 * @date 2020/8/2 10:41
 */
public class CustomMysqlDialect extends MySQL57InnoDBDialect {

    public CustomMysqlDialect() {
        super();
        registerFunction("bitand", new BitAndFunction());
    }

    @Override
    public String getAddForeignKeyConstraintString(String constraintName, String[] foreignKey, String referencedTable, String[] primaryKey, boolean referencesPrimaryKey) {
//        String foreign = super.getAddForeignKeyConstraintString(constraintName, foreignKey, referencedTable, primaryKey, referencesPrimaryKey);
//        return " alter "+ foreignKey[0] +" set default null " ;
        return  "";
    }
}
