package cn.com.pingyee.sketch.core.domain.jpa;

import cn.com.pingyee.sketch.core.domain.PageQuery;
import cn.com.pingyee.sketch.core.domain.TenantContext;
import cn.com.pingyee.sketch.core.domain.Tenantable;

/**
 * @author Yi Ping
 * @version 2.1
 * @date 2020/8/11 22:47
 */
public abstract class AbstractSimpleTenantableJpaCrudService<T extends Tenantable<Long>,Q extends PageQuery,C,U>
    extends AbstractTenantableJpaCrudService<T,T,Long,Q,C,U>
{
    public AbstractSimpleTenantableJpaCrudService(JpaCrudRepository<T, Long> jpaCrudRepository, TenantContext<Long> tenantContext) {
        super(jpaCrudRepository, tenantContext);
    }

    @Override
    protected final T convertT2R(T t) {
        return t;
    }


}
