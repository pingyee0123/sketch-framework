package cn.com.pingyee.sketch.core.domain.jpa;

import cn.com.pingyee.sketch.core.exception.BusinessException;

/**
 * @author Yi Ping
 * @version 2.1
 * @date 2020/8/11 22:38
 */
public class TenantIdIsEmptyException extends BusinessException {
    public TenantIdIsEmptyException() {
        super("100014");
    }
}
