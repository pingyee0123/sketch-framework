package cn.com.pingyee.sketch.core.domain.jpa;

import org.hibernate.dialect.function.SQLFunctionTemplate;
import org.hibernate.type.BooleanType;

/**
 * @author Yi Ping
 * @version 2.0
 * @date 2020/8/2 11:19
 */
public class BitAndFunction extends SQLFunctionTemplate {


    public BitAndFunction() {
        super(BooleanType.INSTANCE, "?1 & ?2", false);
    }
}
