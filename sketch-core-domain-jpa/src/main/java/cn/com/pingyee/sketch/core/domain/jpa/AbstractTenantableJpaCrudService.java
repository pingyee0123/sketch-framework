package cn.com.pingyee.sketch.core.domain.jpa;

import cn.com.pingyee.sketch.core.domain.PageQuery;
import cn.com.pingyee.sketch.core.domain.PageResult;
import cn.com.pingyee.sketch.core.domain.Tenantable;
import cn.com.pingyee.sketch.core.exception.spec.EntityNotExistException;
import cn.com.pingyee.sketch.core.util.Assert;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.io.Serializable;

/**
 * @author Yi Ping
 * @version 1.0
 * @date 2020/8/7 0:43
 */
public abstract class AbstractTenantableJpaCrudService<T extends Tenantable<ID>,
        R,
        ID extends Serializable,
        Q extends PageQuery,C,U>
extends  AbstractJpaCrudService<T,R,ID,Q,C,U>
{
    protected  cn.com.pingyee.sketch.core.domain.TenantContext<ID>  tenantContext;

    public AbstractTenantableJpaCrudService(JpaCrudRepository<T, ID> jpaCrudRepository, cn.com.pingyee.sketch.core.domain.TenantContext<ID> tenantContext) {
        super(jpaCrudRepository);

        this.tenantContext = tenantContext;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public R save(C c) {
        Assert.isNotNull(c, " saved object can not be null");
        T t = convertC2T(c);
        T tn = doSave(t);
        return convertT2R(tn);
    }

    protected T doSave(T t) {
        t.setTenantId(tenantContext.getCurrentTenantId());
        return getJpaCrudRepository().save(t);
    }

    @Override
    public PageResult<? extends R> findAll(Q q) {
        Assert.isNotNull(q, "query can not be null");
        Specification<T> s = tenantContext.isPlatformTenant()? toSpecification(q): new Specification<T>() {
            public Predicate toPredicate(Root<T> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder cb) {
                ID tenantId = tenantContext.getCurrentTenantId();
                if(tenantId== null){
                    throw new TenantIdIsEmptyException();
                }
                return cb.equal(root.get("tenantId"), tenantId);
            }
        }.and(toSpecification(q));
        Page<T> jpaPage = getJpaCrudRepository().findAll(s, JpaUtils.toJpaPageable(q));
        return new JpaPageResult<>(jpaPage).convert(this::convertT2R);
    }


    @Override
    public R findById(ID id) {
        return  id == null? null: jpaCrudRepository.findById(id)
                .filter(s-> tenantContext.isPlatformTenant() || s.getTenantId().equals(tenantContext.getCurrentTenantId()))
                .map(t-> convertT2R(t))
//                .orElseThrow(()-> new EntityNotExistException(domainClass, id));
                .orElse(null);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void delete(ID id) {
        if(id == null){
            return;
        }

        if(tenantContext.isPlatformTenant()){
            jpaCrudRepository.deleteById(id);
        }else {
            T t = jpaCrudRepository.findById(id).filter(s -> s.getTenantId().equals(tenantContext.getCurrentTenantId()))
                    .orElse(null);
            if(t != null){
                jpaCrudRepository.delete(t);
            }
        }
    }
}
