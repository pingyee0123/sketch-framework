package cn.com.pingyee.sketch.core.domain.jpa;

import cn.com.pingyee.sketch.core.support.id.SnowFlakeIdGenerator;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.id.IdentifierGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Yi Ping
 * @version 2.1
 * @date 2020/8/1 0:02
 */

public class SnowflakeIdentityGenerator implements IdentifierGenerator {
    private static final Logger logger = LoggerFactory.getLogger(SnowflakeIdentityGenerator.class);

    public SnowflakeIdentityGenerator(SnowFlakeIdGenerator snowFlakeIdGenerator) {
        this.snowFlakeIdGenerator = snowFlakeIdGenerator;
    }

    public SnowflakeIdentityGenerator(long workerId, long dataCenterId){
        this(new SnowFlakeIdGenerator(workerId, dataCenterId));
    }

    // todo 改成从环境变量中获取
    public SnowflakeIdentityGenerator(){
//        Runtime.getRuntime().
        this(1L,2L);
    }

    private final SnowFlakeIdGenerator snowFlakeIdGenerator;

    @Override
    public Long generate(SharedSessionContractImplementor session, Object obj) {
        if(logger.isInfoEnabled()){
            logger.info(" start generate id");
        }
        return snowFlakeIdGenerator.generateId();
    }
}
