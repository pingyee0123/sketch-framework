package cn.com.pingyee.sketch.core.domain.jpa;

import cn.com.pingyee.sketch.core.domain.PageQuery;

/**
 * @author Yi Ping
 * @version 2.1
 * @date 2020/8/4 1:42
 */
public abstract class AbstractSimpleJpaCrudService<T, Q extends PageQuery,C,U> extends AbstractJpaCrudService<T,T,Long,Q,C,U> {

    public AbstractSimpleJpaCrudService(JpaCrudRepository<T,Long> jpaCrudRepository) {
        super(jpaCrudRepository);
    }


    @Override
    protected final T convertT2R(T t) {
        return  t;
    }
}
