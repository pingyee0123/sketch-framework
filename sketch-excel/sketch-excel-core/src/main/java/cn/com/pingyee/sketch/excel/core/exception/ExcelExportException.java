package cn.com.pingyee.sketch.excel.core.exception;

public class ExcelExportException extends ExcelException {
    public ExcelExportException(String message) {
        super(message);
    }

    public ExcelExportException(String message, Throwable cause) {
        super(message, cause);
    }
}
