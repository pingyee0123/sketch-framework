package cn.com.pingyee.sketch.excel.core.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

/**
 * @author: Yi Ping
 * @date: 2019/10/25 0025 13:28
 * @since: 0.0.1-SNAPSHOT
 */

@Getter
@Setter
@Builder
@AllArgsConstructor
public class ExcelImportResult{
    @ApiModelProperty("总导入数据条数")
    private Integer totalCount;

    @ApiModelProperty("导入成功的条数")
    private Integer successCount;

    @ApiModelProperty("导入失败的条数")
    private Integer failCount;

    @ApiModelProperty("失败的原因")
    private Object failCause;

    public ExcelImportResult() {
    }

}
