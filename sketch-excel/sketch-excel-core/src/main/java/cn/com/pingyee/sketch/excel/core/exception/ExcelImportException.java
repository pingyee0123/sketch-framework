package cn.com.pingyee.sketch.excel.core.exception;

public class ExcelImportException extends ExcelException {

    public ExcelImportException(String message) {
        super(message);
    }

    public ExcelImportException(String message, Throwable cause) {
        super(message, cause);
    }
}
