package cn.com.pingyee.sketch.excel.core.model;

import cn.com.pingyee.sketch.core.response.ApiResponse;
import lombok.Getter;
import lombok.Setter;

/**
 * @author: Yi Ping
 * @date: 2019/11/6 0006 9:38
 * @since: 1.0.0
 */
@Getter
@Setter
public class ExcelImportResultApiResponse extends ExcelImportResult implements ApiResponse {
    private String code;
    private String message;

    public ExcelImportResultApiResponse(ExcelImportResult excelImportResult, String code) {
        super(excelImportResult.getTotalCount(), excelImportResult.getSuccessCount(), excelImportResult.getFailCount(), excelImportResult.getFailCause());
        this.code = code;
        this.message = ApiResponse.generateMessage(code);
    }

    public ExcelImportResultApiResponse() {
    }
}
