package cn.com.pingyee.sketch.excel.core.annotation;

import java.lang.annotation.*;

/**
 * @author: YiPing
 * @create: 2018-10-20 09:23
 * @since: 1.0
 */

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface ExcelColumn{

    /** 列名 **/
    String name() ;

    String value() default "";

    /** 在表格中的列号， 从0开始 , 取注解的Field在Class中的定义顺序**/
    int position() default Integer.MAX_VALUE;

    /** 是否忽略此属性, 如果为true, 将不在excel 中展示
     *  added in v1.4.6
     * **/
    boolean ignored() default false;
}
