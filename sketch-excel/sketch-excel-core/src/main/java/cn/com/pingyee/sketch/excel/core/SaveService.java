package cn.com.pingyee.sketch.excel.core;

import cn.com.pingyee.sketch.excel.core.exception.ExcelException;

/**
 * @author: Yi Ping
 * @date: 2019/11/5 0005 11:54
 * @since: 0.0.1-SNAPSHOT
 */

@FunctionalInterface
public interface SaveService<T> {

    /**
     * <p> 保存</p>
     * @param t
     * @throws Exception
     */
   void save(T t) throws ExcelException;
}
