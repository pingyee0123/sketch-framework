package cn.com.pingyee.sketch.excel.core.model;

public interface ExcelImportModule {

    long getRowIndex();
    void setRowIndex(long rowIndex);

}
