package cn.com.pingyee.sketch.excel.core;

import cn.com.pingyee.sketch.core.response.ApiResponse;
import cn.com.pingyee.sketch.core.response.ErrorCodes;
import cn.com.pingyee.sketch.excel.core.exception.ExcelImportException;
import cn.com.pingyee.sketch.excel.core.model.ExcelImportModule;
import cn.com.pingyee.sketch.excel.core.model.ExcelImportResult;
import cn.com.pingyee.sketch.excel.core.model.ExcelImportResultApiResponse;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author: Yi Ping
 * @date: 2019/11/5 0005 11:53
 * @since: 0.0.1-SNAPSHOT
 */
@Slf4j
public abstract class ExcelImportUtils {

    public static <T extends ExcelImportModule> ExcelImportResultApiResponse imp2ApiResponse(InputStream is, Class<T> tClass, SaveService<T> saveService){
        try {
            ExcelImportResult result = imp(is, tClass, saveService);
            return new ExcelImportResultApiResponse(result, result.getFailCount()>0? ErrorCodes.IMPORT_EXCEL_FILE_DATA_ERROR :"0" );
        }catch (ExcelImportException e){
            ExcelImportResultApiResponse response = new ExcelImportResultApiResponse();
            response.setCode(ErrorCodes.IMPORT_EXCEL_FILE_ERROR);
            response.setMessage(ApiResponse.messageTemplate(response.getCode()));
            response.setFailCause(e);
            return response;
        }

    }

    public   static <T extends ExcelImportModule> ExcelImportResult imp(InputStream inputStream, Class<T> tClass, SaveService<T> saveService) throws ExcelImportException {
        int successCount =0;
        int failCount =0;
        Map<String,String> errors = new HashMap<>(16);
        Workbook workbook =null;
        try {
            workbook = new HSSFWorkbook(inputStream);
        }catch (IOException e){
            throw new ExcelImportException(e.getMessage(),e);
        }
        Sheet sheet = workbook.getSheetAt(0);
        List<T> data = ExcelUtils.imp2Module(sheet, tClass);
        for(T d: data){
            try {
                saveService.save(d);
                ++successCount;
            }catch (Exception e){
                errors.put(d.getRowIndex() + "", e.getMessage());
                ++failCount;
            }
        }

        try {
            workbook.close();
        }catch (IOException e){
            log.error(e.getMessage(),e);
        }
        try {
            inputStream.close();
        } catch (IOException e) {
            log.error(e.getMessage(),e);
        }

        return  ExcelImportResult.builder()
                .failCause(failCount)
                .totalCount(data.size())
                .successCount(successCount)
                .failCause(errors)
                .build();

    }

}
