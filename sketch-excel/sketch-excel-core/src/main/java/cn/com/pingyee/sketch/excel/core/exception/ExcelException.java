package cn.com.pingyee.sketch.excel.core.exception;

/**
 * <p>　</p>
 */
public class ExcelException  extends Exception{

    public ExcelException(String message) {
        super(message);
    }

    public ExcelException(String message, Throwable cause) {
        super(message, cause);
    }
}
