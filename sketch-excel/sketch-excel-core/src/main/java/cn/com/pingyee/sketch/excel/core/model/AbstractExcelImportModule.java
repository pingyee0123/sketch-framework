package cn.com.pingyee.sketch.excel.core.model;


import lombok.Data;

@Data
public abstract class AbstractExcelImportModule implements ExcelImportModule {

    private long rowIndex;
}
