package cn.com.pingyee.sketch.excel.core;


import cn.com.pingyee.sketch.core.util.Assert;
import cn.com.pingyee.sketch.core.util.ReflectUtils;
import cn.com.pingyee.sketch.excel.core.annotation.ExcelColumn;
import cn.com.pingyee.sketch.excel.core.annotation.ExcelTitle;
import cn.com.pingyee.sketch.excel.core.exception.ExcelExportException;
import cn.com.pingyee.sketch.excel.core.exception.ExcelImportException;
import cn.com.pingyee.sketch.excel.core.model.ExcelImportModule;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;

import java.lang.reflect.Field;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author: YiPing
 * @create: 2018-10-20 10:06
 * @since: 1.0
 */
@Slf4j
public class ExcelUtils {

    public static   void export(Workbook toWorkbook, List<?> data, Class<?> clazz) throws ExcelExportException {

        Field[] _fields = ReflectUtils.getAllFieldsIgnoreStatics(clazz);
        if(_fields.length ==0){
            throw new ExcelExportException("导出的数据模型类 不含任何属性: " + clazz.getName());
        }
        List<Field> fieldList = Arrays.asList(_fields).stream().filter(f->!ignored(f)).collect(Collectors.toList());
        if(CollectionUtils.isEmpty(fieldList)){
            throw  new ExcelExportException("导出的数据模型类 没有需要导出属性: " + clazz.getName());
        }
        fieldList.sort(/*(f1,f2)-> getColumnPosition(f1)-getColumnPosition(f2)*/ Comparator.comparingInt(f->getColumnPosition(f)));
        Field[] fields = new Field[fieldList.size()];
        fieldList.toArray(fields);

        String[] columnNames = new String[fields.length];
        for(int i=0; i<fields.length; i++){
            fields[i].setAccessible(true);
            columnNames[i] = getColumnName(fields[i]);
        }
        String filename = getExcelTitle(clazz);
        // add Title
//        addTitle(style, sheet, getExcelTitle(clazz), fields.length-1 );

        Sheet sheet = toWorkbook.createSheet(filename);
        CellStyle style = toWorkbook.createCellStyle();
        style.setAlignment(HorizontalAlignment.CENTER);
        style.setDataFormat(toWorkbook.createDataFormat().getFormat("yyyy-MM-dd HH:mm:ss"));
        // add Column Header 第一行
        addColumnHeader(sheet,columnNames);

        // add Context 从第二行开始
        if(CollectionUtils.isNotEmpty(data)) {
            for (int r = 0; r < data.size(); r++) {
                Row row = sheet.createRow(r + 1);
                Object rowData = data.get(r);
                for (int c = 0; c < fields.length; c++) {
                    Cell cell = row.createCell(c);
                    cell.setCellStyle(style);
                    Object val;
                    try {
                        val = fields[c].get(rowData);
                    } catch (IllegalAccessException e) {
                        throw new ExcelExportException("access getter method fail ",e);
                    }
                    setCellValue(cell, val);
                }
            }

            for(int c=0; c<fields.length; c++){
                sheet.autoSizeColumn(c);
            }
        }

    }

    private static void setCellValue(Cell cell, Object value){
        Assert.isNotNull(cell, "for set cell value , cell can not be null");
        if(null!= value){
            if(value instanceof Date){
                cell.setCellValue((Date) value);
            }else {
                cell.setCellValue(value.toString());
            }
        }
    }


    public  static <T> List<T> imp(Sheet sheet, Class<T> clazz) throws ExcelImportException {
        Objects.requireNonNull(sheet, "sheet can not be null in function ExcelUtils#imp");
        Objects.requireNonNull(clazz, "clazz can not be null in function ExcelUtils#imp");
        List<T> list = new ArrayList<>();

//        int columnNum = sheet.getRow(0).getLastCellNum();
        int rowNum = sheet.getLastRowNum();
        Field[] fields = ReflectUtils.getAllFields(clazz);
        if(null == fields || fields.length ==0){
            return  list;
        }

        Stream.of(fields).forEach(f->f.setAccessible(true));
//        Map<Integer, Field> positionMap = getColumnPositionFieldMap(fields);
        try {
            // 第0行为列名
             for(int i=1;i<=rowNum;i++){
                 T t = clazz.newInstance();
                 if(t instanceof ExcelImportModule){
                     ((ExcelImportModule) t).setRowIndex(i);
                 }
                 Row row = sheet.getRow(i);
                 if(row == null){
                     continue;
                 }
                 for(Field f: fields){
                     Cell cell = row.getCell(getColumnPosition(f));
                     if(null != cell){
                         f.set(t, getCellValue(cell));
                     }
                 }
                 list.add(t);
             }
        } catch (InstantiationException  e) {
            throw new ExcelImportException(clazz.getName() + "不能被实例化， 请检查其是否存在无参public构造函数");
        } catch (IllegalAccessException e) {
            throw new ExcelImportException(e.getMessage(),e);
        }

        return  list;
    }

    public  static <T extends ExcelImportModule> List<T> imp2Module(Sheet sheet, Class<T> clazz) throws ExcelImportException {
        Objects.requireNonNull(sheet, "sheet can not be null in function  ExcelUtils#imp");
        Objects.requireNonNull(clazz, "clazz can not be null in  function ExcelUtils#imp");
        List<T> list = new ArrayList<>();

//        int columnNum = sheet.getRow(0).getLastCellNum();
        int rowNum = sheet.getLastRowNum();
        Field[] fields = ReflectUtils.getAllFields(clazz);
        if(null == fields || fields.length ==0){
            return  list;
        }

        Stream.of(fields).forEach(f->f.setAccessible(true));
//        Map<Integer, Field> positionMap = getColumnPositionFieldMap(fields);
        try {
            // 第0行为列名
            for(int i=1;i<=rowNum;i++){
                T t = clazz.newInstance();
                t.setRowIndex(i);
                Row row = sheet.getRow(i);
                if(row == null){
                    continue;
                }
                for(Field f: fields){
                    Cell cell = row.getCell(getColumnPosition(f));
                    if(null != cell){
                        f.set(t, getCellValue(cell));
                    }
                }
                list.add(t);
            }
        } catch (InstantiationException  e) {
            throw new ExcelImportException(clazz.getName() + "不能被实例化， 请检查其是否存在无参public构造函数");
        } catch (IllegalAccessException e) {
            throw new ExcelImportException(e.getMessage(),e);
        }

        return  list;
    }


    private static Object getCellValue(Cell cell){
        if(cell.getCellTypeEnum() != null) {
            switch (cell.getCellTypeEnum()) {
//                case BLANK:
//                case ERROR:
//                case FORMULA:
//                    return "";
                case BOOLEAN:
                    return String.valueOf(cell.getBooleanCellValue());
                case NUMERIC:
                    if(HSSFDateUtil.isCellDateFormatted(cell)){
                        Date date = cell.getDateCellValue();
                        return DateFormatUtils.format(date,"yy-MM-dd");
                    }else {
                        return String.format("%.0f", cell.getNumericCellValue());
                    }
                case STRING:
                    return cell.getStringCellValue();
                default:
                    return "";

            }
        }
        return  "";
    }

    private static Map<Integer, Field> getColumnPositionFieldMap(Field[] fields){
        Map<Integer,Field> map = new HashMap<>(16);
        if(fields != null && fields.length>0){
            for(int i=0; i<fields.length; i++){
                int tmpPos = getColumnPosition(fields[i]);
                if(tmpPos ==-1){ tmpPos = i;}
                map.put(tmpPos, fields[i]);
            }
        }

        return  map;
    }

    private static void addTitle(CellStyle style,Sheet sheet, String titleName, int columLength){
        //添加标题第一行 合并单元格
        sheet.addMergedRegion(new CellRangeAddress(0, 0, 0, columLength));
        Cell cell = sheet.createRow(0).createCell(0);
        cell.setCellStyle(style);
        cell.setCellValue(titleName);
    }

    private static void addColumnHeader(Sheet sheet, String[] columnNames){
        if(columnNames== null || columnNames.length ==0){
            throw new IllegalArgumentException("columnNames can not be empty");
        }
        Row row = sheet.createRow(0);
        for(int i=0;i< columnNames.length; i++){
            Cell cell = row.createCell(i);
            cell.setCellValue(columnNames[i]);
//            sheet.autoSizeColumn(i);
        }
    }



    public static String getExcelTitle(Class<?> clazz){
        Objects.requireNonNull(clazz, "clazz can not be null");
        return Optional.ofNullable(clazz.getDeclaredAnnotation(ExcelTitle.class))
                .map(a->a.value()).filter(a->StringUtils.isNotBlank(a))
                .orElse(clazz.getName());
//        ExcelTitle annotation = clazz.getDeclaredAnnotation(ExcelTitle.class);
//        return  annotation!=null && StringUtils.isNotBlank(annotation.value())?annotation.value():clazz.getName();
    }

    private static String getColumnName(Field field){
        ExcelColumn annotation = field.getAnnotation(ExcelColumn.class);
        return annotation!=null && StringUtils.isNotBlank(annotation.name()) ? annotation.name():field.getName();
    }

    private static int getColumnPosition(Field field){
        ExcelColumn annotation = field.getAnnotation(ExcelColumn.class);
        return annotation==null?-1:annotation.position();
    }


    private static boolean ignored(Field field){
        ExcelColumn annotation = field.getAnnotation(ExcelColumn.class);
//        return null != annotation && annotation.ignored();
        return null == annotation || annotation.ignored();
    }
}
