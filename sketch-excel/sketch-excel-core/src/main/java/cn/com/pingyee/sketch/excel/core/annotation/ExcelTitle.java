package cn.com.pingyee.sketch.excel.core.annotation;

import java.lang.annotation.*;

/**
 * @author: YiPing
 * @create: 2018-10-20 09:21
 * @since: 1.0
 */

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface ExcelTitle {

    /** excel 标题 **/
    String value();
}
