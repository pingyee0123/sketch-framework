package cn.com.pingyee.sketch.execl.core.test;


import cn.com.pingyee.sketch.core.support.DateTimeIdGenerator;
import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.alibaba.excel.event.SyncReadListener;
import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
public class EasyExcelTest {


    @Test
    public void testSyncRead(){
        List<Object> orders = EasyExcel.read(getClass().getClassLoader().getResourceAsStream("order.xls")).sheet().doReadSync();
        orders.forEach(o->{
            log.info("result class {}", o.getClass());
            log.info(JSON.toJSONString(o));
        });

    }

    @Test
    public void testSyncReadWithClass(){
        SyncReadListener syncReadListener = new SyncReadListener();
        EasyExcel.read(getClass().getClassLoader().getResourceAsStream("order.xls"),Order.class,syncReadListener)
                .sheet().doRead();
        syncReadListener.getList().forEach(o->{
            log.info("result class {}", o.getClass());
            log.info("data was {}", JSON.toJSONString(o));
        });

    }

    @Test
    public void testSyncReadWithClass2(){
        EasyExcel.read(getClass().getClassLoader().getResourceAsStream("order.xls"),Order.class,null)
                .sheet().doReadSync().forEach(o->{
            log.info("result class {}", o.getClass());
            log.info("data was {}", JSON.toJSONString(o));
        });

    }


    @Test
    public void testRead(){
      //  System.out.println(EasyExcelTest.class.getClassLoader().getResource("order.xls").getPath());
        EasyExcel.read(getClass().getClassLoader().getResourceAsStream("order.xls"),
                Order.class,
                new AnalysisEventListener() {
                    private List<Object> result = new ArrayList<>();
                    @Override
                    public void invoke(Object data, AnalysisContext context) {
                        log.info("data was {}", JSON.toJSONString(data));
                        log.info("analysis context was {}", context);
                        result.add(data);
                    }

                    @Override
                    public void doAfterAllAnalysed(AnalysisContext context) {
                        log.info("data size was  {}", result.size());
                        log.info("data class was {} ", result.get(0).getClass());
                    }
                }).sheet().doRead();
    }

    @Test
    public void testWrite(){
        Set<String> names = new HashSet<>();
        names.add("selectedNumber");
        EasyExcel.write(getClass().getClassLoader().getResource("").getPath() + new DateTimeIdGenerator("yyyy-MM-dd_HHmmss").generateId() + ".xlsx"
        , Order.class)/*.excludeColumnFiledNames(names)*/.sheet().doWrite(data());
    }

    @Test
    public void testWriteDynamicHeadWrite(){
        Set<String> names = new HashSet<>();
        names.add("selectedNumber");
        EasyExcel.write(getClass().getClassLoader().getResource("").getPath() + new DateTimeIdGenerator("yyyyMMddHHmmssS").generateId() + ".xls"
        ).head(head()).excludeColumnFiledNames(names).sheet().doWrite(data());
    }


    private List<List<String>> head(){
        return Stream.of(Stream.of("字符串" + System.currentTimeMillis()).collect(Collectors.toList())
                , Stream.of("日期" + System.currentTimeMillis()).collect(Collectors.toList())
        ).collect(Collectors.toList());
    }

    private List<Order> data(){
        List<Order> list = new ArrayList<>();
        for(int i=0; i<20; i++){
            Order o = new Order();
            o.setLinkman("zhangsan" + i);
            o.setLinkTelephone("180880088"+ StringUtils.leftPad(i+"",2,'0'));
            o.setLinkAddress("联系地址" + i);
            o.setSelectedNumber("199009900" + StringUtils.leftPad(i+"",2,'0'));
            list.add(o);
        }
        return list;
    }
}
