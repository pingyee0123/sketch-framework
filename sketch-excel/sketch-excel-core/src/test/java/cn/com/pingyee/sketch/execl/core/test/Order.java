package cn.com.pingyee.sketch.execl.core.test;


import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

@Data
public class Order {

    @ExcelProperty(value = {/*"联系人", */"联系人姓名"})
    private String linkman;
    @ExcelProperty(value = {/*"联系人", */"联系电话"})
    private String linkTelephone;
    @ExcelProperty("联系地址")
    private String linkAddress;
    @ExcelProperty(index = 3, value = "选中号码")
    private String selectedNumber;
}
