package cn.com.pingyee.sketch.excel.springmvc;

import java.util.List;

/**
 * @author: Yi Ping
 * @date: 2019/5/17 0017 13:26
 * @since: 0.0.1-SNAPSHOT
 */
public class ExcelXlsxView extends AbstractExcelXlsxView {

    public ExcelXlsxView(Class<?> clazz, List<?> data) {
        super(clazz, data);
    }



//    private Class<?> clazz;
//    private List<?> data;
//
//    public Class<?> getClazz() {
//        return clazz;
//    }
//
//    public List<?> getData() {
//        return data;
//    }
//
//    public ExcelXlsxView(Class<?> clazz, List<?> data) {
//        this.clazz = clazz;
//        this.data = data;
//    }
//
//    @Override
//    protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request, HttpServletResponse response) throws Exception {
//        response.setHeader(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + URLEncoder.encode(ExcelUtils.getExcelTitle(clazz) +".xlsx", "UTF-8"));
//
//        ExcelUtils.export(workbook, data,clazz);
//
//        OutputStream os = response.getOutputStream();
//
//        workbook.write(os);
//        os.flush();
//        os.close();
//    }
}
