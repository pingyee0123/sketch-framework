package cn.com.pingyee.sketch.excel.springmvc;

import cn.com.pingyee.sketch.excel.core.ExcelUtils;
import com.alibaba.excel.EasyExcel;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.http.HttpHeaders;
import org.springframework.web.servlet.view.document.AbstractXlsxView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.util.List;
import java.util.Map;

/**
 * @author: Yi Ping
 * @date: 2019/5/17 0017 13:26
 * @since: 0.0.1-SNAPSHOT
 * @deprecated  since 1.2  recommend use {@link ExcelXlsxView } or {@link EasyExcelXlsxView}
 */
@Deprecated
public class XlsxExcelView extends AbstractXlsxView {

    private Class<?> clazz;
    private List<?> data;

    public XlsxExcelView(Class<?> clazz, List<?> data) {
        this.clazz = clazz;
        this.data = data;
    }

    @Override
    protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request, HttpServletResponse response) throws Exception {
        response.setHeader(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + URLEncoder.encode(ExcelUtils.getExcelTitle(clazz) +".xlsx", "UTF-8"));
        ExcelUtils.export(workbook, data,clazz);

        OutputStream os = response.getOutputStream();

        workbook.write(os);
        os.flush();
        os.close();
    }
}
