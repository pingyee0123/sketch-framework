package cn.com.pingyee.sketch.excel.springmvc;

import java.util.List;

/**
 * @author: YiPing
 * @create: 2018-10-20 09:01
 * @since: 1.0
 */

public class ExcelView extends AbstractExcelXlsView {
    public ExcelView(Class<?> clazz, List<?> data) {
        super(clazz, data);
    }


//    private Class<?> clazz;
//    private List<?> data;
//
//    public ExcelView(Class<?> clazz, List<?> data) {
//        this.clazz = clazz;
//        this.data = data;
//    }
//
//    protected Class<?> getClazz() {
//        return clazz;
//    }
//
//    protected List<?> getData() {
//        return data;
//    }
//
//    @Override
//    protected void buildExcelDocument(Map<String, Object> model,
//                                      org.apache.poi.ss.usermodel.Workbook workbook,
//                                      HttpServletRequest request, HttpServletResponse response) throws Exception {
//        response.setHeader(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + URLEncoder.encode(ExcelUtils.getExcelTitle(clazz) +".xls", "UTF-8"));
//
//        ExcelUtils.export(workbook, data,clazz);
//
//        OutputStream os = response.getOutputStream();
//
//        // todo 下面几行是否需要，
//        workbook.write(os);
//        os.flush();
//        os.close();
//
//    }
}
