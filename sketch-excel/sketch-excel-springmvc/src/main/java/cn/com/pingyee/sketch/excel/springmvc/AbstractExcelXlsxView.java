package cn.com.pingyee.sketch.excel.springmvc;

import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * @author Yi Ping
 * @version 1.2
 * @date 2020/3/30 13:50
 */
public abstract class AbstractExcelXlsxView extends AbstractExcelXlsView {



    public AbstractExcelXlsxView(Class<?> clazz, List<?> data) {
        super(clazz, data);
        setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
    }

    /**
     * This implementation creates an {@link XSSFWorkbook} for the XLSX format.
     */
    @Override
    protected Workbook createWorkbook(Map<String, Object> model, HttpServletRequest request) {
        return new XSSFWorkbook();
    }

    @Override
    protected String fileSuffix() {
        return ".xlsx";
    }


}
