package cn.com.pingyee.sketch.excel.springmvc;

import cn.com.pingyee.sketch.excel.core.ExcelUtils;
import cn.com.pingyee.sketch.excel.core.exception.ExcelExportException;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.http.HttpHeaders;
import org.springframework.web.servlet.view.document.AbstractXlsView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.List;
import java.util.Map;

/**
 * @author Yi Ping
 * @version 1.2
 * @date 2020/3/30 13:37
 */
public abstract class AbstractExcelXlsView extends AbstractXlsView {

    private Class<?> clazz;
    private List<?> data;

    public AbstractExcelXlsView(Class<?> clazz, List<?> data) {
        super();
        this.clazz = clazz;
        this.data = data;
    }

    protected  String fileSuffix(){
        return ".xls";
    }


    protected Class<?> getClazz() {
        return clazz;
    }

    protected List<?> getData() {
        return data;
    }

    @Override
    protected void buildExcelDocument(Map<String, Object> model,
                                      org.apache.poi.ss.usermodel.Workbook workbook,
                                      HttpServletRequest request, HttpServletResponse response) throws Exception {
        response.setHeader(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + URLEncoder.encode(ExcelUtils.getExcelTitle(clazz) +   fileSuffix(), "UTF-8"));
        rendData(workbook, request, response);
    }


    protected  void rendData(Workbook wb, HttpServletRequest request, HttpServletResponse response) throws IOException, ExcelExportException {
        ExcelUtils.export(wb, getData(),getClazz());
    }

}
