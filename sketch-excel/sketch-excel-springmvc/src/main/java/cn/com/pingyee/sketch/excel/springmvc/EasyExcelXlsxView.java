package cn.com.pingyee.sketch.excel.springmvc;

import com.alibaba.excel.EasyExcel;
import org.apache.poi.ss.usermodel.Workbook;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * @author Yi Ping
 * @version 1.2
 * @date 2020/3/30 11:21
 */
public class EasyExcelXlsxView extends AbstractExcelXlsxView{

    public EasyExcelXlsxView(Class<?> clazz, List<?> data) {
        super(clazz, data);
    }

    @Override
    protected void rendData(Workbook wb, HttpServletRequest request, HttpServletResponse response) throws IOException {
        EasyExcel.write(response.getOutputStream(),getClazz()).sheet().doWrite(getData());
    }


}
