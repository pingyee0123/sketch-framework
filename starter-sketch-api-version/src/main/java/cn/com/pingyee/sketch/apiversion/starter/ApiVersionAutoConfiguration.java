package cn.com.pingyee.sketch.apiversion.starter;

import org.springframework.boot.autoconfigure.web.servlet.WebMvcRegistrations;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

/**
 * @author Yi Ping
 * @version 1.2
 * @date 2020/3/30 16:25
 */
@Configuration
@EnableConfigurationProperties(ApiVersionProperty.class)
public class ApiVersionAutoConfiguration implements WebMvcRegistrations {

    final  ApiVersionProperty config;

    public ApiVersionAutoConfiguration(ApiVersionProperty config) {
        this.config = config;
    }

    @Override
    public RequestMappingHandlerMapping getRequestMappingHandlerMapping() {
        return new ApiVersionRequestMappingHandlerMapping(config);
    }
}
