package cn.com.pingyee.sketch.apiversion.starter.exception;

/**
 * @author Yi Ping
 * @version 1.2
 * @date 2020/3/26 19:21
 */
public class ApiVersionException extends RuntimeException {

    public ApiVersionException() {
    }

    public ApiVersionException(String message) {
        super(message);
    }
}
