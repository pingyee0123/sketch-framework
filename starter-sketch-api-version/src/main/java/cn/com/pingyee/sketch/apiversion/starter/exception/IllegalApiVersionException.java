package cn.com.pingyee.sketch.apiversion.starter.exception;

/**
 * @author Yi Ping
 * @version 1.2
 * @date 2020/3/26 19:22
 */
public class IllegalApiVersionException extends ApiVersionException {

    public IllegalApiVersionException() {
    }

    public IllegalApiVersionException(String message) {
        super(message);
    }
}
