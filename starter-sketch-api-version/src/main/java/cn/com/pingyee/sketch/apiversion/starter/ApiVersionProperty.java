package cn.com.pingyee.sketch.apiversion.starter;


import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.util.Assert;

/**
 * @author Yi Ping
 * @version 1.2
 * @date 2020/3/26 18:23
 */

//@Builder
@Data
@ConfigurationProperties(prefix = "sketch.api.version")
public class ApiVersionProperty {

    private static final  String DEFAULT_HEADER_NAME = "Api-Version";
    private static final  String DEFAULT_QUERY_PARAM_NAME = "v";
    private static final float DEFAULT_VERSION = ApiVersion.DEFAULT_VERSION;

    /**
     * <p> request是否必须带版本号，如果为true， 则 {@link #matchHighestVersionIfMiss}忽略 </p>
     */
    private boolean imperativeRequestVersion = true;

    /**
     * <p> 如果为true, 则当request不带版本号则匹配最高版本, 否则匹配最低版本</p>
     * <p> 用于兼容以前没有使用版本管理的 apis </p>
     */
    private boolean matchHighestVersionIfMiss = true;

    /**
     * <p> 是否从请求头中取版本信息 </p>
     * <p> 请求头中版本信息优先级最高</p>
     */
    private boolean fetchVersionFromHeader = true;
    /** <p> 版本号请求头名</p> **/
    private String headerName = DEFAULT_HEADER_NAME;

    /**
     * <p> 是否从请求参数中取版本信息</p>
     */
    private boolean fetchVersionFromQueryParam = true;
    /** <p> 版本号参数名 </p> **/
    private String queryParamName = DEFAULT_QUERY_PARAM_NAME;

    /**
     * <p> 没有被注解@ApiVersion 或被注解了@ApiVersion但其value为默认值的， 则取默认版本</p>
     */
    private float defaultVersion = DEFAULT_VERSION;

    public void setHeaderName(String headerName) {
        Assert.hasText(headerName, "version header name can not be blank");
        this.headerName = headerName;
    }

    public void setQueryParamName(String queryParamName) {
        Assert.hasText(queryParamName, "version query param name can not be blank");
        this.queryParamName = queryParamName;
    }
}
