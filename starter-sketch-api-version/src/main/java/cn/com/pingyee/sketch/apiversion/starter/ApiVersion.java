package cn.com.pingyee.sketch.apiversion.starter;

import java.lang.annotation.*;

/**
 * @author Yi Ping
 * @date 2020/3/25 16:25
 * @version  1.2
 */
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ApiVersion {

    float value() default DEFAULT_VERSION;

    float DEFAULT_VERSION = 0.1F;
}
