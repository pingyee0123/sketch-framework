package cn.com.pingyee.sketch.apiversion.starter;


import cn.com.pingyee.sketch.apiversion.starter.exception.ApiVersionException;
import cn.com.pingyee.sketch.apiversion.starter.exception.IllegalApiVersionException;
import cn.com.pingyee.sketch.apiversion.starter.exception.RequestMissApiVersionException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.servlet.mvc.condition.AbstractRequestCondition;

import javax.servlet.http.HttpServletRequest;
import java.util.Collection;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author Yi Ping
 * @version 1.2
 * @date 2020/3/25 16:30
 */

public class ApiVersionRequestCondition extends AbstractRequestCondition<ApiVersionRequestCondition> {

    private final float version;
    private final float defaultVersion;
    private final ApiVersionProperty config;

    public ApiVersionRequestCondition(float version, ApiVersionProperty config) {
        this.version = version;
        this.config = config;
        this.defaultVersion = config.getDefaultVersion();
    }

    /**
     * <p> 谁的版本大取谁 </p>
     * @param other
     * @return
     */
    @Override
    public ApiVersionRequestCondition combine(ApiVersionRequestCondition other) {
         return  this.version >=other.version?this:other;
    }

    @Override
    public ApiVersionRequestCondition getMatchingCondition(HttpServletRequest request) {
        return  this;
//       String v = getVersion(request);
//       return StringUtils.isBlank(v)?null: new ApiVersionRequestCondition(v);
    }

    @Override
    public int compareTo(ApiVersionRequestCondition other, HttpServletRequest request) {
        float otherv = other==null?defaultVersion:other.version;
        Float rv = getVersion(request);
        if(null == rv){
            if(config.isMatchHighestVersionIfMiss()) {
                return this.version > otherv ? -1 : 1;
            }else {
//                return this.version == defaultVersion? -1:
//                        (other.version== defaultVersion)? 1: 0;
                return  this.version < otherv? -1:1;
            }
        }else {
            if (rv >= Math.max(this.version, otherv)) {
                return this.version > otherv ? -1 : 1;
            } else {
                return this.version < otherv ? -1 : 1;
            }
        }

    }

    private Float getVersion(HttpServletRequest request) throws ApiVersionException {
        String v = "";
        if(config.isFetchVersionFromHeader()) {
            v = request.getHeader(config.getHeaderName());
        }
        if(StringUtils.isBlank(v) && config.isFetchVersionFromQueryParam()){
            v = request.getParameter(config.getQueryParamName());
        }
        if(StringUtils.isBlank(v)){
            // todo 匹配路径
        }
        if(StringUtils.isBlank(v)){
            if(config.isImperativeRequestVersion()){
                throw new RequestMissApiVersionException();
            }else {



                return null;
            }
        }
        try{
            float fv =  Float.valueOf(v);
            return  fv<defaultVersion?defaultVersion:fv;
        }catch (NumberFormatException e){
            if(config.isImperativeRequestVersion()){
                throw new IllegalApiVersionException(" illegal api version " + v);
            }else {
                return null;
            }
        }
    }

    @Override
    protected Collection<?> getContent() {
        return Stream.of(version).collect(Collectors.toList());
    }

    @Override
    protected String getToStringInfix() {
        return "";
    }
}
