package cn.com.pingyee.sketch.apiversion.starter;

import org.springframework.web.servlet.mvc.condition.RequestCondition;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

import java.lang.reflect.Method;

/**
 * @author Yi Ping
 * @version 1.2
 * @date 2020/3/25 16:56
 */

public class ApiVersionRequestMappingHandlerMapping extends RequestMappingHandlerMapping {
    private final float defaultVersion;
    private final ApiVersionProperty config;

    public ApiVersionRequestMappingHandlerMapping(ApiVersionProperty config) {
        this.defaultVersion = config.getDefaultVersion();
        this.config = config;
    }

    @Override
    protected RequestCondition<?> getCustomTypeCondition(Class<?> handlerType) {
        // todo handlerType 的上一级类
        ApiVersion apiVersion = handlerType.getDeclaredAnnotation(ApiVersion.class);
        float v = null == apiVersion? defaultVersion:apiVersion.value();
        return  new ApiVersionRequestCondition(v, config);
    }

    @Override
    protected RequestCondition<?> getCustomMethodCondition(Method method) {
        // todo method 的上一级类
        ApiVersion apiVersion = method.getDeclaredAnnotation(ApiVersion.class);
        float v = null == apiVersion? defaultVersion:apiVersion.value();
        return  new ApiVersionRequestCondition(v, config);
    }
}
