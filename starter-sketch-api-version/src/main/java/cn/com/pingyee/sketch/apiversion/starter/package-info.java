/**
 * api 版本管理
 * 1. 每个api 都有版本号，默认版本号为 0.1
 * 2. request 通过请求头 Api-Version=$version 或请求参数 v=$version 携带版本号， 请求头的优先级大于请求参数
 * 3.  request如果不带版本请求， 请求的是最新的版本， 如果request带的版本请求高于当前版本， 亦请求最新版本
 * 4. 如果类和方法上都注解@ApiVersion 取最大版本号
 * @author Yi Ping
 * @version 1.2
 * @date 2020/3/26 11:22
 */
package cn.com.pingyee.sketch.apiversion.starter;