package cn.com.pingyee.sketch.apiversion.starter.exception;

/**
 * @author Yi Ping
 * @version 1.2
 * @date 2020/3/26 19:20
 */
public class RequestMissApiVersionException extends ApiVersionException {
    public RequestMissApiVersionException() {
        this("request api version was needed ");
    }

    public RequestMissApiVersionException(String message) {
        super(message);
    }
}
