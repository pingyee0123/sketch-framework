package cn.com.pingyee.sketch.boot.jpa.view;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.Collection;
import java.util.stream.Collectors;

/**
 * @author: YiPing
 * @create: 2018-09-08 09:52
 * @since: 1.0
 */
public interface QueryView<E> extends Specification<E> {

    default Predicate toPredicate(Root<E> root, CriteriaQuery<?> query, CriteriaBuilder cb){
        Collection<Predicate> predicates = andPredicates(root, query, cb).stream().filter(p->p!=null).collect(Collectors.toList());
        if(CollectionUtils.isNotEmpty(predicates)){
            Predicate[] arr = new Predicate[predicates.size()];
            return cb.and(predicates.toArray(arr));
        }

        return  null;
    }

    Collection<Predicate> andPredicates(Root<E> root, CriteriaQuery<?> query, CriteriaBuilder cb);



}
