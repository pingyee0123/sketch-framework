package cn.com.pingyee.sketch.boot.jpa.view;

/**
 * @author: YiPing
 * @create: 2018-08-14 22:02
 * @since: 1.0
 */
public interface ToEntity<E> {

    E toEntity();
}
