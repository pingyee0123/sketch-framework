package cn.com.pingyee.sketch.boot.jpa.base;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.annotations.Where;

import javax.persistence.Column;


@Where(clause = "deleted = false")
public interface LogicDeletable {

    @ApiModelProperty(hidden = true)
    @Column(name = "deleted")
    @JsonIgnore
    boolean isDeleted();

    void setDeleted(boolean deleted);

    default  void markDeleted(){
        setDeleted(true);
    }
}
