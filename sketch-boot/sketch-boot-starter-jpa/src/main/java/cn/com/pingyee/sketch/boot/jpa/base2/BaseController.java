package cn.com.pingyee.sketch.boot.jpa.base2;

import cn.com.pingyee.sketch.boot.jpa.base.BaseEntity;
import cn.com.pingyee.sketch.boot.jpa.view.CreateView;
import cn.com.pingyee.sketch.boot.jpa.view.PageQueryView;
import cn.com.pingyee.sketch.boot.jpa.view.UpdateView;
import cn.com.pingyee.sketch.core.response.ApiResponse;
import cn.com.pingyee.sketch.core.response.Page;
import cn.com.pingyee.sketch.core.validation.group.Create;
import cn.com.pingyee.sketch.core.validation.group.Update;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * @author: Yi Ping
 * @date: 2019/10/21 0021 10:48
 * @since: 1.0.0
 */

@RequestMapping("/base")
@Validated
public interface BaseController<E extends BaseEntity,
        Q extends PageQueryView<E>,
        C extends CreateView<E>,
        U extends UpdateView<E>,
        R extends  ApiResponse> extends ApiResponseConverter<E,R>{



    @Override
    R convert(E e);

    @RequestMapping(value = "/{id:\\d{1,10}}", method = RequestMethod.GET)
    @ApiOperation("查询")
    R get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET)
    @ApiOperation("分页查询")
     Page<E> findAll(Q q);

    @RequestMapping(method = RequestMethod.POST)
    @ApiOperation("新增")
     R save(@Validated(value = Create.class)  C  c);


    @RequestMapping(method = RequestMethod.POST, consumes = "application/json")
    @ApiOperation("新增")
     default R saveJson(@Validated(value = Create.class)  @RequestBody C  c){
        return save(c);
    }



    @PutMapping("/{id:\\d{1,10}}")
    @ApiOperation("修改")
    R update(@PathVariable("id") int id,
            @Validated(value = Update.class)  U u);


    @PutMapping(value = "/{id:\\d{1,10}}", consumes = "application/json")
    @ApiOperation("修改")
    default R  updateJson(
            @PathVariable("id") int id,
            @Validated(value = Update.class) @RequestBody U u){
        return update(id, u);
    }




    @DeleteMapping(value = "/{id:\\d{1,10}}")
    @ApiOperation("删除")
    ApiResponse delete(@PathVariable("id") Integer id);
}
