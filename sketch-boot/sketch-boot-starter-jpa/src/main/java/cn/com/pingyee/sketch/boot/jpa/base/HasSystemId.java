package cn.com.pingyee.sketch.boot.jpa.base;

import cn.com.pingyee.sketch.core.request.UserContextHolder;

import java.util.Optional;

public interface HasSystemId {

    default Integer getSystemId(){
//        return Optional.ofNullable(UserContextHolder.getCurrentUser())
//                .map(t->t.getTenantId())
//                .map(t-> toString())
//                .map(t->{
//                    try {
//                        return Integer.parseInt(t);
//                    } catch (NumberFormatException e){
//                        return  null;
//                    }
//                }).orElse(null);
        return  -1;
    };

     void setSystemId(Integer ownerId);

    default void setSystemId(){
//        setSystemId(Optional.ofNullable((TenantUser<Integer>)UserContextHolder.getCurrentUser())
//                .map(t->t.getTenantId())
//                .map(t-> toString())
//                .map(t->{
//                    try {
//                        return Integer.parseInt(t);
//                    } catch (NumberFormatException e){
//                        return  null;
//                    }
//                }).orElse(null));
    };
}
