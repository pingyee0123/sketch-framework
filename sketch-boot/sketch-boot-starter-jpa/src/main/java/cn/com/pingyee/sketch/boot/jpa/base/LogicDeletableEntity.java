package cn.com.pingyee.sketch.boot.jpa.base;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

/**
 * @author Yi Ping
 * @date 2018/6/27 9:24
 * @since 1.0
 */
@MappedSuperclass
public abstract class LogicDeletableEntity extends BaseEntity {

    @ApiModelProperty(hidden = true)
    private Boolean deleted;

    @ApiModelProperty(hidden = true)
    @Column(name = "deleted")
    @JsonIgnore
    public boolean isDeleted() {
        return deleted == null ? false : deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted == null ? false : deleted;
    }
}
