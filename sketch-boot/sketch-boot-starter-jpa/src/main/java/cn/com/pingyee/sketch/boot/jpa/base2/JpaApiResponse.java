package cn.com.pingyee.sketch.boot.jpa.base2;

import cn.com.pingyee.sketch.core.response.ApiResponse;
import cn.com.pingyee.sketch.core.response.ErrorCodes;

import javax.persistence.Transient;

/**
 * @author: Yi Ping
 * @date: 2019/10/21 0021 11:11
 * @since: 1.0.0
 */
public interface JpaApiResponse extends ApiResponse {

    @Override
    @Transient
    default String getCode(){
        return SUCCESS.getCode();
    }

    /**
     * 消息提示
     * @return
     */
    @Override
    @Transient
    default String getMessage(){
        return SUCCESS.getMessage();
    }

    /**
     * 是否成功
     * @return
     */
    @Override
    @Transient
    default boolean isSuccess(){
        return ErrorCodes.SUCCESS.equals(getCode());
    }
}
