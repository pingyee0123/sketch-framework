package cn.com.pingyee.sketch.boot.jpa.base2;

import cn.com.pingyee.sketch.core.response.Page;

/**
 * @author: Yi Ping
 * @date: 2019/10/21 0021 11:50
 * @since: 1.0.0
 */
public final class PageAdapter<T> extends Page<T> {

    public PageAdapter(org.springframework.data.domain.Page jpaPage) {
        this.setContent(jpaPage.getContent());
        this.setTotalPages(jpaPage.getTotalPages());
        this.setTotalElements((int) jpaPage.getTotalElements());
        this.setNumber(jpaPage.getNumber());
        this.setSize(jpaPage.getSize());
        this.setFirst(jpaPage.isFirst());
        this.setLast(jpaPage.isLast());
        this.setNumberOfElements(jpaPage.getNumberOfElements());
    }
}
