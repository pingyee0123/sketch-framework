package cn.com.pingyee.sketch.boot.jpa.util;


import cn.com.pingyee.sketch.boot.jpa.exception.EntityNotExistException;
import cn.com.pingyee.sketch.boot.jpa.exception.EntityNotFoundException;

/**
 * @author: YiPing
 * @create: 2018-08-07 10:03
 * @since: 1.0
 */
public abstract class EntityAssert {


     public  static <T> void exist(T entity, Class<T> clazz, int id){
          if(entity == null){
               throw new EntityNotExistException(clazz,id);
          }
     }

     public  static <T> void found(T entity, Class<T> clazz){
          if(entity == null){
               throw new EntityNotFoundException(clazz);
          }
     }

     public  static <T> void found(T entity, Class<T> clazz, String message){
          if(entity == null){
               throw new EntityNotFoundException(clazz, message);
          }
     }
}
