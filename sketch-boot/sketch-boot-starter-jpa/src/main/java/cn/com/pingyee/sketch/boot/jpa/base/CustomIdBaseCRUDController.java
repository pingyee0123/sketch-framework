package cn.com.pingyee.sketch.boot.jpa.base;


import cn.com.pingyee.sketch.boot.jpa.view.CreateView;
import cn.com.pingyee.sketch.boot.jpa.view.QueryView;
import cn.com.pingyee.sketch.boot.jpa.view.UpdateView;
import cn.com.pingyee.sketch.core.response.ApiJson;
import cn.com.pingyee.sketch.core.validation.group.Create;
import cn.com.pingyee.sketch.core.validation.group.Update;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.io.Serializable;

/**
 * @author: YiPing
 * @create: 2018-08-20 20:19
 * @since: 1.0
 */
@RequestMapping("/enhanced2-base")
@Validated
public class CustomIdBaseCRUDController<E ,ID extends Serializable, R extends QueryView<E>, C extends CreateView<E>, U extends UpdateView<E>> {

    @Autowired
    CustomIdBaseCRUDService<E, ID,R,C,U> service;

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ApiOperation("查询")
    public E get(@PathVariable("id") ID id) {
        E t =  service.getOne(id);
        return  t;
    }

    @RequestMapping(value = "/page", method = RequestMethod.POST)
    @ApiOperation("分页查询")
    public Page<E> findPage(R r, Pageable pageable) {
        return service.findAll(r, pageable);
    }

    @RequestMapping(method = RequestMethod.POST)
    @ApiOperation("新增")
    public E save(@Validated(value = Create.class)   C  c) {
        return service.save(c);
    }


    @PutMapping("/{id}")
    @ApiOperation("修改")
    public E update(
            @PathVariable("id") ID id,
            @Validated(value = Update.class)  U u){
        return service.update(id, u);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ApiOperation("删除")
    public ApiJson delete(@PathVariable("id") ID id) {
        service.delete(id);
        return  ApiJson.success();
    }
}
