package cn.com.pingyee.sketch.boot.jpa.base;

import javax.persistence.MappedSuperclass;
import javax.persistence.Version;

/**
 * @author: Yi Ping
 * @date: 2019/9/7 0007 0:14
 * @since: 0.0.1-SNAPSHOT
 */


@MappedSuperclass
public abstract class VersionEntity extends BaseEntity {


    private Integer version;

    @Version
    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }
}
