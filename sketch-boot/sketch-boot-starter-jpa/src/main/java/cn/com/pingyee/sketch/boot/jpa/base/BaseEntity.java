package cn.com.pingyee.sketch.boot.jpa.base;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

/**
 * @author Yi Ping
 * @date 2018/6/22 10:09
 * @since 1.0
 */

@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public abstract class BaseEntity  implements  Serializable{


    @ApiModelProperty(hidden = true)
    private Integer id;
    @ApiModelProperty(hidden = true)
    private Integer addUserId;
    @ApiModelProperty(hidden = true)
    private Date addTime;
    @ApiModelProperty(hidden = true)
    private Integer editUserId;
    @ApiModelProperty(hidden = true)
    private Date editTime;

    @PrePersist
    void prePersist() {
        init();
    }

    protected void init() {
        if(this instanceof HasSystemId){
            ((HasSystemId) this).setSystemId();
        }
    }


    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @ApiModelProperty(hidden = true)
    @JsonIgnore
    @Column(name = "AddUserId", updatable = false)
    public Integer getAddUserId() {
        return addUserId;
    }

    @CreatedBy
    public void setAddUserId(Integer addUserId) {
        this.addUserId = addUserId;
    }

    @ApiModelProperty(hidden = true)
    @Column(name = "AddTime", updatable = false)
    @JsonIgnore
    public Date getAddTime() {
        return addTime;
    }

    @CreatedDate
    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }

    @ApiModelProperty(hidden = true)
    @JsonIgnore
    @Column(name = "EditUserId")
    public Integer getEditUserId() {
        return editUserId;
    }

    @LastModifiedBy
    public void setEditUserId(Integer editUserId) {
        this.editUserId = editUserId;
    }

    @ApiModelProperty(hidden = true)
    @JsonIgnore
    @Column(name = "EditTime")
    public Date getEditTime() {
        return editTime;
    }

    @LastModifiedDate
    public void setEditTime(Date editTime) {
        this.editTime = editTime;
    }


    @Override
    public String toString() {
        return getClass().getName() + '#' + getId();
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {return true;}
        if (o == null || getClass() != o.getClass()) {return false;}
        BaseEntity that = (BaseEntity) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        if (id != null) {
            return Objects.hash(getClass(), id);
        }else {
            return Objects.hash(getClass(), RandomStringUtils.random(6));
        }
    }

    public static boolean validId(Integer id){
        return  id != null && id >0;
    }


}
