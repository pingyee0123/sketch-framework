package cn.com.pingyee.sketch.boot.jpa;

import cn.com.pingyee.sketch.core.request.UserContextHolder;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

import java.util.Optional;

/**
 * @author: Yi Ping
 * @date: 2019/9/25 0025 9:58
 * @since: 1.0.0
 */


@Configuration
@EnableJpaAuditing
public class JpaAutoConfiguration {

    @Bean
    @ConditionalOnMissingBean(AuditorAware.class)

    public AuditorAware<?> auditorAware(){
        return ()-> Optional.ofNullable(UserContextHolder.getCurrentUser()).map(u->u.getId());
    }

}
