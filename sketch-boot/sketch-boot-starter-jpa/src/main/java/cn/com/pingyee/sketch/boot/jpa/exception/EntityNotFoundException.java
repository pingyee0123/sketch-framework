package cn.com.pingyee.sketch.boot.jpa.exception;


import cn.com.pingyee.sketch.core.exception.ResourceNotFoundException;

/**
 * @author: YiPing
 * @create: 2018-08-10 16:07
 * @since: 1.0
 */
public class EntityNotFoundException extends ResourceNotFoundException {

    private Class<?> clazz;

    public EntityNotFoundException(Class clazz) {
        this(clazz, "");
    }

    public EntityNotFoundException(Class<?> clazz, String condition) {
        super("100006",clazz.getName(), condition);
        this.clazz = clazz;
    }
}
