package cn.com.pingyee.sketch.boot.jpa.exception;


import cn.com.pingyee.sketch.core.exception.ResourceNotFoundException;

/**
 * @author: YiPing
 * @create: 2018-08-07 10:07
 * @since: 1.0
 */
public class EntityNotExistException extends ResourceNotFoundException {


    private  Class<?> clazz;
    private  int id;


    public EntityNotExistException(Class clazz, int id){
        super("100005", clazz.getName(), String.valueOf(id));
        this.clazz = clazz;
        this.id = id;
    }


    public EntityNotExistException(String entityClassName, int id){
        super("100005", entityClassName, String.valueOf(id));
        this.id = id;
    }

}
