package cn.com.pingyee.sketch.boot.jpa.base2;

import cn.com.pingyee.sketch.boot.jpa.base.BaseEntity;
import cn.com.pingyee.sketch.boot.jpa.view.CreateView;
import cn.com.pingyee.sketch.boot.jpa.view.PageQueryView;
import cn.com.pingyee.sketch.boot.jpa.view.UpdateView;
import org.springframework.data.domain.Page;

import javax.transaction.Transactional;
import java.util.List;
/**
 * @author: Yi Ping
 * @date: 2019/10/21 0021 10:30
 * @since: 1.0.0
 */
public interface BaseService<T extends BaseEntity, R extends PageQueryView<T>, C extends CreateView<T>, U extends UpdateView<T>> {

    /**
     * <p> 保存 </p>
     * @param c
     * @return
     */
    @Transactional(rollbackOn = Exception.class)
    T save(C c);

    @Transactional(rollbackOn = Exception.class)
    List<T> saveAll(Iterable<C> iterable);


    @javax.transaction.Transactional(rollbackOn = Exception.class)
    T update(Integer id, U u);

    @Transactional(rollbackOn = Exception.class)
     void delete(Integer id) ;


    /**
     *
     * @param id
     * @return
     */
    T findById(Integer id);

    /**
     * <p> 查询所有 </p>
     * @param r
     * @return
     */
    Page<T> findAll(R r);


     long count(R r);



}
