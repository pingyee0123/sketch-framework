package cn.com.pingyee.sketch.boot.jpa.util;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;

/**
 * @author: Yi Ping
 * @create: 2019-01-11 09:48
 * @since: 2.1.0
 */
public  abstract  class PredicateUtils {

    public static <T extends Comparable<T>>  Predicate between(Path<T> expression, T start, T end, CriteriaBuilder cb){
        if(start == null){
            if(end != null) {
                return cb.lessThanOrEqualTo(expression, end);
            }
        }else {
            if(end !=null) {
                return  (cb.between(expression, start, end));
            }else {
                return cb.greaterThanOrEqualTo(expression, start);
            }
        }

        return  null;
    }

}
