package cn.com.pingyee.sketch.boot.jpa.view;

import cn.com.pingyee.sketch.core.request.PageRequest;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.Arrays;


/**
 * @author: Yi Ping
 * @date: 2019/10/21 0021 9:47
 * @since: 1.0.0
 */
public interface PageQueryView<E> extends QueryView<E> , PageRequest {

    default Pageable toPageable(){
        String sort = getSort();
        String[] sorts;
        Sort.Direction direct;
        Sort sortObject = null;
        if(StringUtils.isNotBlank(sort)){
            String[] split = sort.split(",");
            String directStr = split[split.length - 1];
            if(Sort.Direction.DESC.name().equalsIgnoreCase(directStr)){
                if(split.length -1 >0) {
                    direct = Sort.Direction.DESC;
                    sorts = Arrays.copyOfRange(split, 0, split.length - 1);
                    sortObject = Sort.by(direct,sorts);
                }
            }else if(Sort.Direction.ASC.name().equalsIgnoreCase(directStr)) {
                if(split.length -1 >0) {
                    direct = Sort.Direction.ASC;
                    sorts = Arrays.copyOfRange(split, 0, split.length - 1);
                    sortObject = Sort.by(direct,sorts);
                }
            }else {
                direct = Sort.Direction.ASC;
                sorts = Arrays.copyOfRange(split,0,split.length);
                sortObject = Sort.by(direct,sorts);
            }

        }

        return sortObject == null? org.springframework.data.domain.PageRequest.of(getPage(),getSize())
                : org.springframework.data.domain.PageRequest.of(getPage(),getSize(),sortObject);
    }

}
