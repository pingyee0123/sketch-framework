package cn.com.pingyee.sketch.boot.jpa.base;


import cn.com.pingyee.sketch.boot.jpa.view.CreateView;
import cn.com.pingyee.sketch.boot.jpa.view.QueryView;
import cn.com.pingyee.sketch.boot.jpa.view.UpdateView;
import cn.com.pingyee.sketch.core.util.ReflectUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.io.Serializable;

/**
 * @author Yi Ping
 * @date 2018/6/22 9:02
 * @since 1.0
 */
public abstract  class CustomIdBaseCRUDService<T ,ID extends Serializable, R extends QueryView<T>, C extends CreateView<T>, U extends UpdateView<T>>{
    protected Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    protected CustomIdBaseRepository<T,ID> repository;



    public T save(C c) {
      return repository.save(c.toEntity());
    }



    public T update(ID id, U u){
        T source = getOne(id);
       return  update(source,u.toEntity());
    }

    /**
     * <p> 子类重写改方法， 增加修改约束条件</p>
     * @param source
     * @param targe
     * @return
     */
    protected  T update(T source, T targe){
        if(source == null){
            return  null;
        }else {
            ReflectUtils.copyProperties(source, targe);
            return  repository.save(source);
        }
    }

    public void delete(ID id) {
        T t = repository.getOne(id);
        if(t != null) {
            repository.delete(t);
        }
    }


    public T getOne(ID var1) {
        return repository.getOne(var1);
    }


    public Page<T> findAll(R r, Pageable pageable){
        return  repository.findAll(r, pageable);
    }

}
