package cn.com.pingyee.sketch.boot.jpa.view;

/**
 * @author: YiPing
 * @create: 2018-08-14 22:01
 * @since: 1.0
 */
public interface CreateView<E> extends ToEntity<E>  {

    @Override
    E toEntity();

}
