package cn.com.pingyee.sketch.boot.jpa.base2;

import cn.com.pingyee.sketch.core.response.ApiResponse;

/**
 * @author: Yi Ping
 * @date: 2019/10/21 0021 11:18
 * @since: 1.0.0
 */
public interface ApiResponseConverter<S,T extends ApiResponse> {

    T convert(S source);
}
