package cn.com.pingyee.sketch.boot.jpa.base2;

import cn.com.pingyee.sketch.boot.jpa.base.BaseEntity;
import cn.com.pingyee.sketch.boot.jpa.base.BaseRepository;
import cn.com.pingyee.sketch.boot.jpa.base.LogicDeletable;
import cn.com.pingyee.sketch.boot.jpa.base.LogicDeletableEntity;
import cn.com.pingyee.sketch.boot.jpa.exception.EntityNotExistException;
import cn.com.pingyee.sketch.boot.jpa.view.CreateView;
import cn.com.pingyee.sketch.boot.jpa.view.PageQueryView;
import cn.com.pingyee.sketch.boot.jpa.view.UpdateView;
import cn.com.pingyee.sketch.core.exception.BusinessException;
import cn.com.pingyee.sketch.core.exception.HttpClientException;
import cn.com.pingyee.sketch.core.response.ErrorCodes;
import cn.com.pingyee.sketch.core.util.Assert;
import cn.com.pingyee.sketch.core.util.ReflectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.mapping.PropertyReferenceException;

import javax.transaction.Transactional;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.List;


/**
 * @author: Yi Ping
 * @date: 2019/10/25 0025 14:58
 * @since: 1.0.0
 */
public abstract class AbstractBaseService<T extends BaseEntity,
        R extends PageQueryView<T>,
        C extends CreateView<T>,
        U extends UpdateView<T>>

    implements BaseService<T,R,C,U>
{

    private final Class<T> domainClass;

    @Autowired
    BaseRepository<T> repository;

    public AbstractBaseService() {
        ParameterizedType type = (ParameterizedType) this.getClass().getGenericSuperclass();
        domainClass = (Class<T>) type.getActualTypeArguments()[0];
    }



    /**
     * <p> 保存 </p>
     * @param c
     * @return
     */
    @Override
    @Transactional(rollbackOn = Exception.class)
    public T save(C c) {
        return repository.save(c.toEntity());
    }

    @Override
    @Transactional(rollbackOn = Exception.class)
    public List<T> saveAll(Iterable<C> iterable){
        List<T> list = new ArrayList<>();
        iterable.forEach(c ->list.add(save(c)));
        return  list;
    }


    @Override
    @Transactional(rollbackOn = Exception.class)
    public T update(Integer id, U u){
        T source = repository.findById(id).orElseThrow(()-> new EntityNotExistException(domainClass, id));
        return update(source, u.toEntity());
    }

    /**
     * <p> 子类重写改方法， 增加修改约束条件</p>
     * @param source
     * @param targe
     * @return
     */
    protected    T update(T source, T targe){
        if(source == null || targe == null){
            return  null;
        }else {
            ReflectUtils.copyProperties(source, targe);
            return repository.save(source);
        }
    }

    @Override
    @Transactional(rollbackOn = Exception.class)
    public    void delete(Integer id) {
        T t = repository.findById(id).orElseThrow(()->new EntityNotExistException(domainClass, id));
        beforeDelete(t);
        if(t instanceof LogicDeletableEntity){
            ((LogicDeletableEntity) t).setDeleted(true);
            repository.save(t);
        }else if( t instanceof  LogicDeletable) {
            ((LogicDeletable) t).markDeleted();
            repository.save(t);
        }else {
            repository.delete(t);
        }

    }


    /**
     *
     * @param id
     * @return
     */
    @Override
    public    T findById(Integer id) {
        T t = repository.findById(id).orElseThrow(() -> new EntityNotExistException(domainClass, id));
        // 兼容二级缓存， 当使用逻辑删除时， 即update操作, 对象在hibernate二级缓存中并未消失
        if (t instanceof LogicDeletableEntity && ((LogicDeletableEntity) t).isDeleted()) {
                throw new BusinessException(ErrorCodes.RESOURCE_LOGIC_DELETED);
        }
        if(t instanceof LogicDeletable && ((LogicDeletable) t).isDeleted()){
            throw new BusinessException(ErrorCodes.RESOURCE_LOGIC_DELETED);
        }

        setExtendProperties(t);
        return t;
    }


    @Override
    public Page<T> findAll(R r) {
        try {
            Page page =  repository.findAll(r, r.toPageable());
            setExtendProperties(page.getContent());
            return  page;
        }catch (PropertyReferenceException e){
            throw new HttpClientException(ErrorCodes.RESOURCE_PROPERTY_NOT_EXIST, e.getMessage());
        }
    }

    @Override
    public long count(R query){
        Assert.isNotNull(query, String.format("%s.%s query can not be null", getClass().getName(), "count"));
        return repository.count(query);
    }




    protected void setExtendProperties(T t){

    }

    protected    void setExtendProperties(List<T> list){

    }
    protected void beforeDelete(T t) {

    }

}
