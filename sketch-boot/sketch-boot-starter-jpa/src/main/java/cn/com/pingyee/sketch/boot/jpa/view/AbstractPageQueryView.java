package cn.com.pingyee.sketch.boot.jpa.view;

import cn.com.pingyee.sketch.boot.jpa.base.HasSystemId;
import cn.com.pingyee.sketch.core.request.DefaultPageRequest;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author: Yi Ping
 * @date: 2019/10/25 0025 12:14
 * @since: 1.0.0
 */
public abstract class AbstractPageQueryView<E> extends DefaultPageRequest implements PageQueryView<E> {


    @Override
    public Collection<Predicate> andPredicates(Root<E> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
        List<Predicate> predicates = new ArrayList<>();
        Optional.of((HasSystemId.class.isAssignableFrom(root.getJavaType()) && this instanceof HasSystemId))
                .map(t -> t ? cb.equal(root.get("systemId"), ((HasSystemId) this).getSystemId()) : null)
                .ifPresent(predicates::add);
        predicates.addAll(andPredicatesInternal(root,query, cb));
        return  predicates;
//        List<Predicate> predicates = new ArrayList<>();
//        if(HasSystemId.class.isAssignableFrom(root.getJavaType()) && this instanceof HasSystemId){
//            predicates.add(cb.equal(root.get("systemId"), ((HasSystemId)this).getSystemId()));
//        }
//        return  predicates;
    }


    protected abstract Collection<Predicate> andPredicatesInternal(Root<E> root, CriteriaQuery<?> query, CriteriaBuilder cb);
}
