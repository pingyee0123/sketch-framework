package cn.com.pingyee.sketch.boot.jpa.base2;

import cn.com.pingyee.sketch.boot.jpa.base.BaseEntity;
import cn.com.pingyee.sketch.boot.jpa.view.CreateView;
import cn.com.pingyee.sketch.boot.jpa.view.PageQueryView;
import cn.com.pingyee.sketch.boot.jpa.view.UpdateView;
import cn.com.pingyee.sketch.core.response.ApiResponse;
import cn.com.pingyee.sketch.core.response.Page;
import cn.com.pingyee.sketch.core.validation.group.Create;
import cn.com.pingyee.sketch.core.validation.group.Update;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * @author: Yi Ping
 * @date: 2019/10/25 0025 15:06
 * @since: 1.0.0
 */

@RequestMapping("/base")
@Validated
public abstract class AbstractBaseController<E extends BaseEntity,
        Q extends PageQueryView<E>,
        C extends CreateView<E>,
        U extends UpdateView<E>,
        R extends  ApiResponse> implements BaseController<E,Q,C,U,R>  {


    @Autowired
    BaseService<E,Q,C,U> service;

    /**
     * <p> </p>
     * @param e
     * @return
     */
    @Override
    public abstract R convert(E e);

    @ResponseBody
    @RequestMapping(value = "/{id:\\d{1,10}}", method = RequestMethod.GET)
    @ApiOperation("查询")
    @Override
    public   R get(@PathVariable("id") Integer id) {
        E t =  service.findById(id);
        return convert(t);
    }


    @ResponseBody
    @Override
    @RequestMapping(method = RequestMethod.GET)
    @ApiOperation("分页查询")
    public Page<E> findAll(Q q) {
        org.springframework.data.domain.Page<E> page = service.findAll(q);
        return new PageAdapter(page);
    }

    @ResponseBody
    @Override
    @RequestMapping(method = RequestMethod.POST)
    @ApiOperation("新增")
    public R save(@Validated(value = Create.class)  C  c) {
        E e =  service.save(c);
        return convert(e);
    }





    @ResponseBody
    @Override
    @PutMapping("/{id:\\d{1,10}}")
    @ApiOperation("修改")
    public R update(
            @PathVariable("id") int id,
            @Validated(value = Update.class)  U u){
        E e =  service.update(id, u);
        return  convert(e);
    }





    @ResponseBody
    @Override
    @DeleteMapping(value = "/{id:\\d{1,10}}")
    @ApiOperation("删除")
    public ApiResponse delete(@PathVariable("id") Integer id) {
        service.delete(id);
        return ApiResponse.success();
    }
}
