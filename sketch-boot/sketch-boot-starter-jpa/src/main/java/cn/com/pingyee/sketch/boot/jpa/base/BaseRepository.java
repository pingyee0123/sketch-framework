package cn.com.pingyee.sketch.boot.jpa.base;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * @author Yi Ping
 * @date 2018/6/27 16:39
 * @since 1.0
 */
public interface BaseRepository<T extends BaseEntity> extends JpaRepository<T, Integer>, JpaSpecificationExecutor<T> {

}
