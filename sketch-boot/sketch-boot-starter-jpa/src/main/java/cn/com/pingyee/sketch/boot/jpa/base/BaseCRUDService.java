package cn.com.pingyee.sketch.boot.jpa.base;


import cn.com.pingyee.sketch.boot.jpa.exception.EntityNotExistException;
import cn.com.pingyee.sketch.boot.jpa.view.CreateView;
import cn.com.pingyee.sketch.boot.jpa.view.QueryView;
import cn.com.pingyee.sketch.boot.jpa.view.UpdateView;
import cn.com.pingyee.sketch.core.exception.BusinessException;
import cn.com.pingyee.sketch.core.exception.HttpClientException;
import cn.com.pingyee.sketch.core.util.ReflectUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mapping.PropertyReferenceException;

import javax.transaction.Transactional;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Yi Ping
 * @date 2018/6/22 9:02
 * @since 1.0
 */
public abstract  class BaseCRUDService<T extends BaseEntity, R extends QueryView<T>, C extends CreateView<T>, U extends UpdateView<T>>{
    protected Logger logger = LoggerFactory.getLogger(getClass());
    private  final Class<?> entityClass;

    @Autowired
    protected BaseRepository<T> repository;


    public BaseCRUDService() {
        ParameterizedType type = (ParameterizedType) this.getClass().getGenericSuperclass();
        this.entityClass = (Class<T>) type.getActualTypeArguments()[0];
    }

    @Transactional(rollbackOn = Exception.class)
    public T save(C c) {
      return repository.save(c.toEntity());
    }


    @Transactional(rollbackOn = Exception.class)
    public List<T> saveAll(Iterable<C> iterable){
        List<T> list = new ArrayList<>();
        iterable.forEach(c ->list.add(save(c)));
        return  list;
    }


    @Transactional(rollbackOn = Exception.class)
    public T update(Integer id, U u){
        T source = repository.findById(id).orElseThrow(()-> new EntityNotExistException(this.entityClass, id));
        return update(source, u.toEntity());
    }

    /**
     * <p> 子类重写改方法， 增加修改约束条件</p>
     * @param source
     * @param targe
     * @return
     */
    protected  T update(T source, T targe){
        if(source == null || targe == null){
            return  null;
        }else {
            ReflectUtils.copyProperties(source, targe);
            return  repository.save(source);
        }
    }

    @Transactional(rollbackOn = Exception.class)
    public void delete(Integer id) {
        T t = repository.findById(id).orElseThrow(()->new EntityNotExistException(this.entityClass, id));
        beforeDelete(t);
        if(t instanceof LogicDeletableEntity){
            ((LogicDeletableEntity) t).setDeleted(true);
            repository.save(t);
        }else {
            repository.delete(t);
        }

    }


    /**
     *
     * @param id
     * @return
     */
    public T findById(Integer id) {
        T t =repository.findById(id).orElseThrow(()-> new EntityNotExistException(this.entityClass,id));
        // 兼容二级缓存， 当使用逻辑删除时， 即update操作, 对象在hibernate二级缓存中并未消失
        if(t instanceof  LogicDeletableEntity && ((LogicDeletableEntity) t).isDeleted()){
            throw new BusinessException("100003");
        }
        setExtendProperties(t);
        return  t;
    }


    public  Page<T> findAll(R r, Pageable pageable) {
        try {
            Page page =  repository.findAll(r, pageable);
            setExtendProperties(page.getContent());
            return  page;
        }catch (PropertyReferenceException e){
            throw new HttpClientException("100004", e.getMessage());
        }
    }

    public List<T> findAll(R r){
        List<T> l=   repository.findAll(r);
        setExtendProperties(l);
        return l;
    }


    public long count(R r){
        return  repository.count(r);
    }


    protected  void setExtendProperties(T t){

    }

    protected  void setExtendProperties(List<T> list){

    }
    protected void beforeDelete(T t) {
    }

}
