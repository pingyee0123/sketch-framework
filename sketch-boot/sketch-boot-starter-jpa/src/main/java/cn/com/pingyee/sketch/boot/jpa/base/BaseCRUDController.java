package cn.com.pingyee.sketch.boot.jpa.base;

import cn.com.pingyee.sketch.boot.jpa.view.CreateView;
import cn.com.pingyee.sketch.boot.jpa.view.QueryView;
import cn.com.pingyee.sketch.boot.jpa.view.UpdateView;
import cn.com.pingyee.sketch.core.response.ApiJson;
import cn.com.pingyee.sketch.core.validation.group.Create;
import cn.com.pingyee.sketch.core.validation.group.Update;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;


/**
 * @author: YiPing
 * @create: 2018-08-20 20:19
 * @since: 1.0
 */
@RequestMapping("/base")
@Validated
public class BaseCRUDController<E extends BaseEntity, R extends QueryView<E>, C extends CreateView<E>, U extends UpdateView<E>> {

    @Autowired
    BaseCRUDService<E,R,C,U> service;

    @ResponseBody
    @RequestMapping(value = "/{id:\\d{1,10}}", method = RequestMethod.GET)
    @ApiOperation("查询")
    public ApiJson<E> get(@PathVariable("id") Integer id) {
        E t =  service.findById(id);
       return ApiJson.success(t);
    }


    @ResponseBody
    @RequestMapping(method = RequestMethod.GET)
    @ApiOperation("分页查询")
    public ApiJson<Page<E>> findAll( R r, @ApiIgnore Pageable pageable) {
        return ApiJson.success(service.findAll(r, pageable));
    }

    @ResponseBody
    @RequestMapping(method = RequestMethod.POST)
    @ApiOperation("新增")
    public ApiJson<E> save(@Validated(value = Create.class)  C  c) {
        return ApiJson.success(service.save(c));
    }


    @ResponseBody
    @RequestMapping(method = RequestMethod.POST, consumes = "application/json")
    @ApiOperation("新增")
    public ApiJson<E> saveJson(@Validated(value = Create.class)  @RequestBody C  c) {
        return ApiJson.success(service.save(c));
    }



    @ResponseBody
    @PutMapping("/{id:\\d{1,10}}")
    @ApiOperation("修改")
    public ApiJson<E> update(
            @PathVariable("id") int id,
            @Validated(value = Update.class)  U u){
        return ApiJson.success(service.update(id, u));
    }


    @ResponseBody
    @PutMapping(value = "/{id:\\d{1,10}}", consumes = "application/json")
    @ApiOperation("修改")
    public ApiJson<E> updateJson(
            @PathVariable("id") int id,
            @Validated(value = Update.class) @RequestBody U u){
        return ApiJson.success(service.update(id, u));
    }




    @ResponseBody
    @DeleteMapping(value = "/{id:\\d{1,10}}")
    @ApiOperation("删除")
    public ApiJson delete(@PathVariable("id") Integer id) {
        service.delete(id);
        return  ApiJson.success();
    }


}
