package cn.com.pingyee.shetch.core.test.jna;


import com.sun.jna.Library;
import com.sun.jna.Native;
import org.junit.Test;

import java.io.UnsupportedEncodingException;

/**
 * @author Yi Ping
 * @version 2.1
 * @date 2020/9/8 15:56
 */
public class JnaTest {

    public interface TscLibDll extends Library {
    TscLibDll INSTANCE =
        (TscLibDll) Native.loadLibrary("C:\\TSCLIB", TscLibDll.class);

        int about ();

        /**
         * <P> 打开打印机端口 </P>
         * @param pirnterName
         * @return
         */
        int openport (String pirnterName);

        /**
         * <p> 关闭打印机端口 </p>
         * @return
         */
        int closeport ();

        int sendcommand (String printerCommand);

        /**
         * <p> 设定标签的宽度、高度、打印速度、打印浓度、传感器类别、gap/black mark
         * 垂直间距、gap/black mark 偏移距离)</p>
         * @param width 宽度 单位: mm
         * @param height 高度 单位: mm
         * @param speed  打印速度 (打印速度随机型不同而有不同的选项)
         *  <p> 1.0: 每秒1.0 英寸打印速度
         * 1.5: 每秒1.5 英寸打印速度
         * 2.0: 每秒2.0 英寸打印速度
         * 3.0: 每秒3.0 英寸打印速度
         * 4.0: 每秒4.0 英寸打印速度
         * 5.0: 每秒5.0 英寸打印速度
         * 6.0: 每秒6.0 英寸打印速度
         * </p>
         * @param density  设定打印浓度   0~15，数字越大打印结果越黑
         * @param sensor  设定使用传感器类别
         *    0 表示使用垂直间距传感器(gap sensor)
         *   1 表示使用黑标传感器(black mark sensor)
         * @param vertical  设定gap/black mark 垂直间距高度，单位: mm
         * @param offset <p>设定gap/black mark 偏移距离，单位: mm，此参数若使用一般标签时均设为0</p>
         * @return
         */
        int setup (String width,String height,String speed,String density,String sensor,String vertical,String offset);
        int downloadpcx (String filename,String image_name);

        /**
         * 使用条形码机内建条形码打印
         * @param x 条形码X 方向起始点，以点(point)表示。
         * (200 DPI，1 点=1/8 mm, 300 DPI，1 点=1/12 mm)
         * @param y     条形码Y 方向起始点，以点(point)表示。
         * (200 DPI，1 点=1/8 mm, 300 DPI，1 点=1/12 mm)
         * @param type
         * 128 Code 128, switching code subset A, B, C automatically
         * 128M Code 128, switching code subset A, B, C manually.
         * EAN128 Code 128, switching code subset A, B, C automatically
         * 25 Interleaved 2 of 5
         * 25C Interleaved 2 of 5 with check digits
         * 39 Code 39
         * 39C Code 39 with check digits
         * 93 Code 93
         * EAN13 EAN 13
         * EAN13+2 EAN 13 with 2 digits add-on
         * EAN13+5 EAN 13 with 5 digits add-on
         * EAN8 EAN 8
         * EAN8+2 EAN 8 with 2 digits add-on
         * EAN8+5 EAN 8 with 5 digits add-on
         * CODA Codabar
         * POST Postnet
         * UPCA UPC-A
         * UPCA+2 UPC-A with 2 digits add-on
         * UPCA+5 UPC-A with 5 digits add-on
         * UPCE UPC-E
         * UPCE+2 UPC-E with 2 digits add-on
         * UPCE+5 UPC-E with 5 digits add-o
         * @param height  设定条形码高度，高度以点来表示
         * @param readable      设定是否打印条形码码文
         * 0: 不打印码文
         * 1: 打印码文
         * @param rotation
         * @param narrow
         * @param wide
         * @param code
         * @return
         */
        int barcode (String x,String y,String type,String height,String readable,String rotation,String narrow,String wide,String code);
        int printerfont (String x,String y,String fonttype,String rotation,String xmul,String ymul,String text);

        /**
         * <p>清除　</p>
         * @return
         */
        int clearbuffer ();
        int printlabel (String set, String copy);
        int formfeed ();
        int nobackfeed ();
        int windowsfont (int x, int y, int fontheight, int rotation, int fontstyle, int fontunderline, String szFaceName, String content);
    }

//    @Test
    public void testPrint1() throws UnsupportedEncodingException {
        System.setProperty("jna.encoding","GBK");
        TscLibDll.INSTANCE.openport("2120TU");
        TscLibDll.INSTANCE.clearbuffer();
        TscLibDll.INSTANCE.setup("60", "40", "1.5", "8", "1", "40", "0");
//        TscLibDll.INSTANCE.printerfont("30","30","TSS24.BF2","0","1","1",
//                "食用农产品合格证");
        TscLibDll.INSTANCE.windowsfont(70,30,35,0,2,0,
                "黑体","食用农产品合格证");
        TscLibDll.INSTANCE.sendcommand("QRCODE 40,100,H,5,A,0,M1,S7,\"http://dev.trace.pingyee.com.cn/index.html?id=1981644321530843136\"");
        TscLibDll.INSTANCE.windowsfont(40,280,30,0,2,0, "Arial","产品溯源码");
        int WORD_SHIFT = 215;
        TscLibDll.INSTANCE.windowsfont(WORD_SHIFT,100,20,0,2,0, "Arial","产品名称:山林跑鸡");
        TscLibDll.INSTANCE.windowsfont(WORD_SHIFT,130,20,0,2,0, "Arial","品牌名称:凤羽九华");
        TscLibDll.INSTANCE.windowsfont(WORD_SHIFT,160,20,0,2,0, "Arial","宰杀日期:2020-08-27");
        TscLibDll.INSTANCE.windowsfont(WORD_SHIFT,190,20,0,2,0, "Arial","联系方式:18912345678");
        TscLibDll.INSTANCE.windowsfont(WORD_SHIFT,220,20,0,2,0, "Arial","单位名称: 青阳坊烁农");
        TscLibDll.INSTANCE.windowsfont(WORD_SHIFT,250,20,0,2,0, "Arial","产品贸易有限公司");
        TscLibDll.INSTANCE.printlabel("1","1");
        TscLibDll.INSTANCE.closeport();
    }

    public static void main(String[] args) {
//        System.setProperty("javawebstart.version", "C:\\WINDOWS\\System32\\");
////        System.setProperty("javawebstart.version", "C:\\WINDOWS\\System32\\");
//        System.out.println("java lib path " + System.getProperty("java.library.path"));
        //TscLibDll.INSTANCE.about();
        TscLibDll.INSTANCE.openport("2120TU");
        //TscLibDll.INSTANCE.downloadpcx("C:\\UL.PCX", "UL.PCX");
//        TscLibDll.INSTANCE.sendcommand("REM ***** This is a test by JAVA. *****");
//        TscLibDll.INSTANCE.sendcommand("QRCODE 100,100,L,8,A,0,M1,S7,\"http://www.baidu.com\"");
        TscLibDll.INSTANCE.setup("50", "40", "5.0", "8", "0", "0", "0");
        TscLibDll.INSTANCE.clearbuffer();
        TscLibDll.INSTANCE.sendcommand("QRCODE 25,25,H,4,A,0,M2,S7,\"http://dev.trace.pingyee.com.cn/index.html?id=1981644321530843136\"");
        //TscLibDll.INSTANCE.sendcommand("PUTPCX 550,10,\"UL.PCX\"");
        TscLibDll.INSTANCE.printerfont ("100", "10", "3", "0", "1", "1", "中文测试");
//        TscLibDll.INSTANCE.barcode("100", "40", "128", "50", "1", "0", "2", "2", "123456789");
//        TscLibDll.INSTANCE.windowsfont(400, 200, 48, 0, 3, 1, "arial", "DEG 0");
//        TscLibDll.INSTANCE.windowsfont(400, 200, 48, 90, 3, 1, "arial", "DEG 90");
//        TscLibDll.INSTANCE.windowsfont(400, 200, 48, 180, 3, 1, "arial", "DEG 180");
//        TscLibDll.INSTANCE.windowsfont(400, 200, 48, 270, 3, 1, "arial", "DEG 270");
        TscLibDll.INSTANCE.printlabel("1", "1");
        TscLibDll.INSTANCE.closeport();
    }

    @Test
    public void test(){
        System.out.println(System.getProperty("java.library.path"));
    }
}
