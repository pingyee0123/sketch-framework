package cn.com.pingyee.shetch.core.test.testutils;

/**
 * @author Yi Ping
 * @version 0.0.1
 * @description
 * @date 2020/12/22 23:18
 */
public interface ValueGenerator<T> {

    T generateValue();

}
