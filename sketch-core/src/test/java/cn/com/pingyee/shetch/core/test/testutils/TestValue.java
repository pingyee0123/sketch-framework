package cn.com.pingyee.shetch.core.test.testutils;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * @author Yi Ping
 * @version 0.0.1
 * @description
 * @date 2020/12/22 23:19
 */
@Target(ElementType.FIELD)
@Retention(RUNTIME)
public @interface TestValue {

    Class<? extends ValueGenerator> generator() ;

    long arg() default Long.MIN_VALUE;
}
