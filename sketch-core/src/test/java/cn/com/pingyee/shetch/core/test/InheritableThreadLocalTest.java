package cn.com.pingyee.shetch.core.test;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author Yi Ping
 * @version 1.0
 * @date 2020/5/7 9:25
 */


@Slf4j
public class InheritableThreadLocalTest {

    private ExecutorService executor = Executors.newFixedThreadPool(2);

    @Test
    public  void test(){
        InheritableThreadLocal<String> holder = new InheritableThreadLocal<>();
        holder.set("主线程复制");

        new Thread(()-> log.info("子线程取值 " +  holder.get())).start();
    }


    @Test
    public void testThreadPool(){
        InheritableThreadLocal<String> holder = new InheritableThreadLocal<>();
        holder.set("主线程复制");
        String v = holder.get();
        executor.execute(()-> {
            holder.set(v +" 子线程赋值");
            log.info("子线程取值 " +  holder.get());
        });
        executor.execute(()-> {
            holder.set(v +" 子线程赋值");
            log.info("子线程取值 " +  holder.get());
        });
        executor.execute(()-> {
            holder.set(v +" 子线程赋值");
            log.info("子线程取值 " +  holder.get());
        });
        executor.execute(()-> {
            holder.set(v +" 子线程赋值");
            log.info("子线程取值 " +  holder.get());
        });
        executor.execute(()-> {
            holder.set(v +" 子线程赋值");
            log.info("子线程取值 " +  holder.get());
        });

    }


}
