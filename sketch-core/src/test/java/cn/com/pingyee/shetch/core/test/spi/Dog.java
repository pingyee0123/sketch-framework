package cn.com.pingyee.shetch.core.test.spi;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Yi Ping
 * @version 2.0
 * @date 2020/7/21 23:17
 */
public class Dog implements  IShout {

    private  static final Logger log = LoggerFactory.getLogger(Dog.class);

    @Override
    public void shout() {
        log.info("wang wang");
    }

}
