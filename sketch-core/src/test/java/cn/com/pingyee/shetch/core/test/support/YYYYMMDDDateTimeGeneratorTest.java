package cn.com.pingyee.shetch.core.test.support;


import cn.com.pingyee.sketch.core.support.DateTimeIdGenerator;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.junit.Assert;
import org.junit.Test;

import java.util.Date;

@Slf4j
public class YYYYMMDDDateTimeGeneratorTest {

    @Test
    public void test(){
        Date date  = new Date();
        String id = (String)new DateTimeIdGenerator("yyyyMMddHHmmssS", date).generateId();
        Assert.assertTrue("id length was 17",id.length()==17);
        Assert.assertTrue(id.equalsIgnoreCase(DateFormatUtils.format(date,"yyyyMMddHHmmssS")));
        log.info("id was {} ", id);
    }
}
