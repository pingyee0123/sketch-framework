package cn.com.pingyee.shetch.core.test;

import java.util.concurrent.TimeUnit;

/**
 * @author Yi Ping
 * @version 1.0
 * @date 2020/5/27 22:58
 */
public class ThreadTest2 {

    public static void main(String[] args){
        Runtime.getRuntime().addShutdownHook(new Thread(()-> System.out.println("hook")));
        System.out.println("main thread start");
        try {
            TimeUnit.SECONDS.sleep(3);
        } catch (InterruptedException e) {
            System.out.println("thread interrupted");
        }

    }
}
