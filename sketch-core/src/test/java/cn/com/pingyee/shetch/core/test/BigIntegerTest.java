package cn.com.pingyee.shetch.core.test;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

import java.math.BigInteger;

/**
 * @author Yi Ping
 * @version 1.0
 * @date 2020/5/19 0:10
 */

@Slf4j
public class BigIntegerTest {

    @Test
    public void test(){
        BigInteger bigInteger = BigInteger.ONE;
        while ((bigInteger = bigInteger.nextProbablePrime() ).longValue()< 1000) {
            log.info("{}", bigInteger);
        }
    }
}
