package cn.com.pingyee.shetch.core.test;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.TimeUnit;

/**
 * @author Yi Ping
 * @version 1.0
 * @date 2020/5/16 16:03
 */
@Slf4j
public class ThreadTest {

    public static final int COUNT = 999;

    public static void main(String[] args){
        Runtime.getRuntime().addShutdownHook(new Thread(()->log.info("hook")));
        log.info("main thread start");
        try {
            TimeUnit.SECONDS.sleep(2);
        } catch (InterruptedException e) {
            log.info("thread interrupted");
        }

    }

    @Test
    public  void interruptMethodTest() throws InterruptedException {

        Thread t1 = new Thread(() -> {
            log.debug("------------sleeping");
            try {
                TimeUnit.SECONDS.sleep(10);
            } catch (InterruptedException e) {
                log.debug("--------- thread interrupted {}", Thread.currentThread().isInterrupted());
                e.printStackTrace();
            }

            for (int i = 0; i< COUNT; i++){
                ;
            }
            log.info(" thread end");
        });
        t1.start();
        Thread.interrupted();
        log.info("main thread was interrupted {}", Thread.currentThread().isInterrupted());


        TimeUnit.SECONDS.sleep(1);
        log.info("t1 was interrupted {}", t1.isInterrupted());
        t1.interrupt();
        log.info("t1 was interrupted {}", t1.isInterrupted());

    }



    @Test
    public void test2() throws InterruptedException {
        Thread t = new MyThread();
        t.start();
        TimeUnit.MICROSECONDS.sleep(500);
        t.interrupt();
//        t.stop();

        t.join();
        TimeUnit.MICROSECONDS.sleep(100);
    }

    public static class MyThread extends Thread{
        @Override
        public void run() {
            log.info("Thread interrupted {}", this.isInterrupted());
            try {
                for(int i=0; i<10000; i++){
                    log.info("i {}", i);
                }
                log.info("Thread sleep start {}", this.isInterrupted());
                TimeUnit.SECONDS.sleep(2);
                log.info("Thread sleep end");
            } catch (InterruptedException e) {
                log.info("Thread interrupted  after sleep {}", this.isInterrupted());
            }
        }
    }


    @Test
    public void testGetCPU(){
        int i = Runtime.getRuntime().availableProcessors();
        log.info("processor number was {}", i);
    }

    @Test
    public void testGetIpAddr() throws SocketException {
        Enumeration<NetworkInterface> networkInterfaces = NetworkInterface.getNetworkInterfaces();
        while (networkInterfaces.hasMoreElements()){
            NetworkInterface net = networkInterfaces.nextElement();
            Enumeration<InetAddress> inetAddresses = net.getInetAddresses();
            log.info("net was {}\n", net.toString());
            while (inetAddresses.hasMoreElements()){
                InetAddress inetAddress = inetAddresses.nextElement();
                String hostAddress = inetAddress.getHostAddress();
                log.info(hostAddress);
            }
        }
    }


    @Test
    public void barrierTest() throws InterruptedException {
        CyclicBarrier barrier = new CyclicBarrier(3,()->log.info("arrive barrier"));
        log.info("main start");
        for(int i=0;i<3;i++){
            new Thread(()->{
                log.info("start wait");
                try {
                    barrier.await();
                    log.info("start");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (BrokenBarrierException e) {
                    e.printStackTrace();
                }

            }).start();
        }

        TimeUnit.SECONDS.sleep(1);

    }


    @Test
    public void interruptThis(){
        log.info("interrupted {} ", Thread.currentThread().isInterrupted());
        Thread.currentThread().interrupt();
        log.info("interrupted after {} ", Thread.currentThread().isInterrupted());
        /** 清理中断标识 **/
        Thread.interrupted();
        log.info("static call interrupted {}", Thread.currentThread().isInterrupted());
    }

}
