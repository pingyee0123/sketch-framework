package cn.com.pingyee.shetch.core.test.dynamic;

import cn.com.pingyee.sketch.core.response.ApiResponse;
import lombok.extern.slf4j.Slf4j;
import net.sf.cglib.proxy.*;
import org.junit.Assert;
import org.junit.Test;

import java.lang.reflect.Method;
import java.util.Date;

/**
 * @author Yi Ping
 * @version 1.0
 * @date 2020/5/22 18:03
 */
@Slf4j
public class CglibProxyTest {

    @Test
    public void testCglib(){
        Bird b = new Bird("yanzi");
        Enhancer enhancer = new Enhancer();
        enhancer.setInterfaces(new Class[]{ApiResponse.class});
        enhancer.setSuperclass(Bird.class);
        enhancer.setCallbacks(new MethodInterceptor[]{new LogMethodInterceptor(), new ExecutionTimeMethodInterceptor()});
        enhancer.setCallbackTypes(new Class[]{LogMethodInterceptor.class, ExecutionTimeMethodInterceptor.class});
        enhancer.setCallbackFilter(new CallbackFilter() {
            @Override
            public int accept(Method method) {
                return 0;
            }
        });
//        Bird bird = (Bird) enhancer.create(new Class[]{String.class}, new String[]{"yanzi1" });
        Bird bird = (Bird) enhancer.create();
        bird.fly();
        if(bird instanceof ApiResponse){
            log.info("response code was {}", ((ApiResponse) bird).getCode());
        }else {
            log.info("bird was not an instance of ApiResponse");
        }
    }


    public static class  Bird implements JdkDynamicProxyTest.Flyable{

        private String name;

        public Bird() {
        }

        public Bird(String name) {
            this.name = name;
        }

        @Override
        public void fly() {
            log.info("bird {} fly", name );
        }
    }

    @Test
    public void testTarget(){
        Bird bird = new Bird("zhangsan");
        Enhancer enhancer = new Enhancer();
        enhancer.setInterfaces(new Class[]{ApiResponse.class});
        enhancer.setSuperclass(Bird.class);
        enhancer.setCallback(new TargetMethodInterceptor<>(bird));

        Bird proxybird = (Bird) enhancer.create();
        proxybird.fly();
        if(proxybird instanceof ApiResponse){
            log.info("response code was {}", ((ApiResponse) proxybird).getCode());
        }else {
            log.info("bird was not an instance of ApiResponse");
        }
    }

    public  static class TargetMethodInterceptor<T> implements MethodInterceptor{
        T target;

        public TargetMethodInterceptor(T target) {
            Assert.assertNotNull( "target can not be null", target);

            this.target = target;
        }

        @Override
        public Object intercept(Object obj, Method method, Object[] args, MethodProxy proxy) throws Throwable {
            if(!method.getName().equals("getCode")) {
                return  method.invoke(target, args);
            }else {
                return  ApiResponse.success().getCode();
            }
        }
    }

    public static class LogMethodInterceptor implements MethodInterceptor{

        @Override
        public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
            log.info("start log");
            Object result;
            if(!method.getName().equals("getCode")) {
                 result = methodProxy.invokeSuper(o, objects);
            }else {
                result =  ApiResponse.success().getCode();
//               result =  method.invoke(o, objects);
            }
            log.info("end log");
            return  result;
        }
    }


    public static class  ExecutionTimeMethodInterceptor implements MethodInterceptor{

        @Override
        public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
            long start = System.currentTimeMillis();
            log.info("start time ", new Date(start));
            Object result = methodProxy.invokeSuper(o, objects);
            long time = System.currentTimeMillis() - start;
            log.info("costs time was {}", time);
            return  result;
        }
    }


}
