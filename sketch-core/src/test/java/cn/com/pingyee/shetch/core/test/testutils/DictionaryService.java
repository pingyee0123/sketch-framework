package cn.com.pingyee.shetch.core.test.testutils;

import java.util.List;

/**
 * @author Yi Ping
 * @version 2.1
 * @description
 * @date 2020/12/31 17:28
 */
public interface DictionaryService<ID> {

    List<ID> findAllIdsByPid(ID pid);
}
