package cn.com.pingyee.shetch.core.test.spi;

/**
 * @author Yi Ping
 * @version 2.1
 * @date 2020/11/27 15:43
 */
public class Cat implements IShout {
    @Override
    public void shout() {
        System.out.println("miao miao");
    }
}
