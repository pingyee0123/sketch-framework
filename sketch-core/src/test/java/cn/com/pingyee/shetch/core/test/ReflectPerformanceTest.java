package cn.com.pingyee.shetch.core.test;

import lombok.Data;
import org.junit.Test;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * <p> 反射性能测试 </p>
 * @author: Yi Ping
 * @date: 2019/10/23 0023 13:35
 * @since: 1.0.0
 */

public class ReflectPerformanceTest extends ExecuteTimeTest{


    private Person person;
    private Method method;

    @Override
    public void before() throws NoSuchMethodException {
        super.before();
        person = new Person();
        person.setName("zhansan");
        method = Person.class.getMethod("getName");
    }


    @Test
    public  void derectInvoke(){
        person.getName();
    }

    @Test
    public void reflectInvoke() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
            Person.class.getMethod("getName").invoke(person);
    }

    @Test
    public void reflecInvokeWithCache() throws InvocationTargetException, IllegalAccessException {
            method.invoke(person);
    }

    @Test
    public void derectInvokeRepete(){
        repeteTest(this::derectInvoke);
    }
//    @Test
    public void reflectInvokeRepete() {
        repeteTest(()-> {
            try {
                this.reflectInvoke();
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        });
    }

    @Test
    public void reflecInvokeWithCacheRepete()  {
        repeteTest(()-> {
            try {
                reflecInvokeWithCache();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        });
    }





    @Test
    public void test2(){
        System.out.println(Math.pow(10,6));
    }

    @Data
    public static class Person{
        private String name;
    }
}
