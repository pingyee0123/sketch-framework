package cn.com.pingyee.shetch.core.test.testutils;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author Yi Ping
 * @version 2.1
 * @description
 * @date 2020/12/22 22:14
 */

public final class TestUtils {

    private Logger log = LoggerFactory.getLogger(TestUtils.class);

    public static Map<Class, ValueGenerator> GENERATORS = new ConcurrentHashMap<>();

    static {
        register(Boolean.class, ()-> RandomUtils.nextBoolean());
        register(boolean.class, ()-> RandomUtils.nextBoolean());
        register(char.class, ()-> RandomStringUtils.random(1).charAt(0));
        register(Character.class, ()-> RandomStringUtils.random(1).charAt(0));
        register(String.class, ()-> RandomStringUtils.randomAlphanumeric(10, 30));
        register(short.class, ()->(short) RandomUtils.nextInt(1, 1000));
        register(Short.class, ()->(short) RandomUtils.nextInt(1, 1000));
        register(int.class, ()-> RandomUtils.nextInt(1, 1000));
        register(Integer.class, ()-> RandomUtils.nextInt(1, 1000));
        register(long.class, ()-> RandomUtils.nextLong(1, 1000));
        register(Long.class, ()-> RandomUtils.nextLong(1, 1000));
        register(double.class, ()-> RandomUtils.nextDouble(0.1d, 99999d));
        register(Double.class, ()-> RandomUtils.nextDouble(0.2d, 99999d));
        register(float.class, ()-> RandomUtils.nextFloat(0.2f, 99999f));
        register(Float.class, ()-> RandomUtils.nextFloat(0.2f, 99999f));
        register(BigDecimal.class, ()-> BigDecimal.valueOf(RandomUtils.nextDouble(0.2d, 999999d)));
        register(Date.class, ()-> DateUtils.addDays(new Date(), (RandomUtils.nextBoolean()?1:-1) * RandomUtils.nextInt(1,999)));
        register(LocalDateTime.class, ()-> LocalDateTime.now().plusDays(RandomUtils.nextInt(-999,999)));
        register(LocalDate.class, ()-> LocalDate.now().plusDays(RandomUtils.nextLong(-999, 999)));
        register(LocalTime.class, ()-> LocalTime.now().plusSeconds(RandomUtils.nextInt(-999, 999)));
    }

    public static <T> void register(Class<T> clz, ValueGenerator<T> generator){
        GENERATORS.put(clz, generator);
    }

    public static void register2(Class<? extends ValueGenerator > clz, ValueGenerator generator){
        GENERATORS.put(clz, generator);
    }

    public static <T> T createBean(Class<T> clazz) throws TestException{
        Objects.requireNonNull(clazz, "class can not be null");
        if(clazz.isEnum()){
            T[] enumConstants = clazz.getEnumConstants();
            return (enumConstants!=null && enumConstants.length >0)? enumConstants[RandomUtils.nextInt(0, enumConstants.length)]:null;
        }
        if(GENERATORS.containsKey(clazz)){
            return (T) GENERATORS.get(clazz).generateValue();
        }
        // todo
        if(List.class.isAssignableFrom(clazz)){
        }
        try {
            T t = clazz.newInstance(); // 抛出异常

            Field[] fields = getAllFields(clazz);
            if(fields != null && fields.length >0){
                for(Field f: fields){
                    // 静态属性不设值
                    if(Modifier.isStatic(f.getModifiers())){
                        continue;
                    }
                    f.setAccessible(true);
                    Type fieldType = f.getGenericType();
                    Object value =null;
                    if(fieldType instanceof ParameterizedType && List.class.isAssignableFrom((Class<?>) ((ParameterizedType) fieldType).getRawType())){
                        Class<?> clz = (Class<?>) ((ParameterizedType) fieldType).getActualTypeArguments()[0];
                        List list = new ArrayList();
                        for(int i = 0; i< RandomUtils.nextInt(1,10); i++){
                            list.add(createBean(clz));
                        }
                        try {
                            f.set(t,list);
                            continue;
                        } catch (IllegalAccessException | IllegalArgumentException e) {
                            throw new TestException(e);
                        }
                    }

                    if(! (fieldType instanceof Class)){
                        continue;
                    }
                    ValueGenerator declaredValueGenerator = getDeclaredValueGenerator(f);
                    if(declaredValueGenerator != null){
                        value= declaredValueGenerator.generateValue();
                    }else {
                        value = createBean((Class)fieldType);
                    }
                    try {
                        f.set(t,value);
                    } catch (IllegalAccessException | IllegalArgumentException e) {
//                        log.error(e.getMessage(),e);
                        throw new TestException(e);
                    }
                }
            }
            return  t;
        } catch (InstantiationException | IllegalAccessException e) {
//            log.error(e.getMessage(),e);
            throw new TestException(e);
        }

    }


    private  static ValueGenerator getDeclaredValueGenerator(Field field){
        TestValue testValue = field.getDeclaredAnnotation(TestValue.class);
        if(testValue == null){
            return  null;
        }
        try {
            if(DictionaryGenerator.class.isAssignableFrom(testValue.generator())){
                return  testValue.generator().getConstructor(long.class).newInstance(testValue.arg());
            }else {
                return  testValue.generator().newInstance();
            }
        } catch (InstantiationException | IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
           return  null;
        }
    }

    public static Field[] getAllFields(Class<?> clazz) {
        if (clazz == null) {
            throw new IllegalArgumentException("clazz 不能为null");
        }

        if (clazz != Object.class) {
            Field[] fields = clazz.getDeclaredFields();
            Class<?> superClazz = clazz.getSuperclass();
            Field[] superFields = getAllFields(superClazz);
            return concatArray( superFields, fields);
        }
        return new Field[0];
    }


    public static <T extends Enum<?>> T randomEnum(Class<T> clazz){

        T[] enumConstants = clazz.getEnumConstants();
        if(enumConstants.length >0){
            return  enumConstants[RandomUtils.nextInt(0,enumConstants.length)];
        }else {
            return  null;
        }
    }



    private static <T> T[] concatArray(final T[] first,final T[] second) {
        if(null == first || first.length==0) {
            return second;
        }
        if(null==second || second.length==0) {
            return first;
        }

        T[] result = Arrays.copyOf(first, first.length + second.length);
        System.arraycopy(second, 0, result, first.length, second.length);
        return result;
    }

    public static class TestException extends RuntimeException{
        public TestException() {
        }

        public TestException(String message) {
            super(message);
        }

        public TestException(String message, Throwable cause) {
            super(message, cause);
        }

        public TestException(Throwable cause) {
            super(cause);
        }

        public TestException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
            super(message, cause, enableSuppression, writableStackTrace);
        }
    }

}
