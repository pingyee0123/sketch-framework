package cn.com.pingyee.shetch.core.test;

import cn.com.pingyee.sketch.core.support.id.SnowFlakeIdGenerator;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.Collectors;

/**
 * @author Yi Ping
 * @version 2.1
 * @date 2020/7/30 16:54
 */
@Slf4j
public class SnowFlakeTest {

    private SnowFlakeIdGenerator idGenerator = new SnowFlakeIdGenerator(3,4);

    @Test
    public void test() throws InterruptedException {
        List<Long> ids = new CopyOnWriteArrayList<>();
        Thread thread1 = new Thread(() -> {
            for (int i = 0; i < 30; i++) {
                ids.add(idGenerator.generateId());
            }
        });

        Thread thread2= new Thread(() -> {
            for (int i = 0; i < 30; i++) {
                ids.add(idGenerator.generateId());
            }
        });
        Thread thread3 = new Thread(() -> {
            for (int i = 0; i < 40; i++) {
                ids.add(idGenerator.generateId());
            }
        });
        thread1.start();
        thread2.start();
        thread3.start();

        thread1.join();
        thread2.join();
        thread3.join();

        log.info("ids count {}", ids.size());
        int size = ids.stream().collect(Collectors.toSet()).size();
        log.info("un duplicated size was {} ", size);
        log.info("unsorted ");
//        Assert.assertTrue(ids.size() == size);

        ids.forEach(id-> log.info(id + ""));
        log.info("sorted ");
        ids.stream().filter(id->id != null).sorted().forEach(id-> log.info(id + ""));
    }


    @Test
    public void test2(){
        log.info(System.currentTimeMillis() + "") ;
    }
}
