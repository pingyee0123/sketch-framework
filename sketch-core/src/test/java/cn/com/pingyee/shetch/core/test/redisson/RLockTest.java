package cn.com.pingyee.shetch.core.test.redisson;

import lombok.extern.slf4j.Slf4j;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.redisson.Redisson;
import org.redisson.api.RLock;
import org.redisson.config.Config;

@Slf4j
public class RLockTest {

    private  Redisson redisson;

//    @Before
    public void setup(){
        Config config = new Config();
        config.useSingleServer().setAddress("redis://baiduyun.pingyee.com.cn:6379");
        config.useSingleServer().setPassword("Xia510987");
        this.redisson = (Redisson) Redisson.create(config);
    }

//    @Test
    public void reentrantLockTest(){
        RLock lock = redisson.getLock("anyLock3");
//        redisson.set
        System.out.println("start lock");
        lock.lock();
    }

    @Test
    public void test(){

    }

//    @After
    public void close(){
        if(log.isInfoEnabled()){
            log.info("==== shutdown redis connection ====");
        }
        redisson.shutdown();
    }
}
