package cn.com.pingyee.shetch.core.test.testutils;

import com.hb.amp.ppw.common.SpringUtils;
import com.hb.amp.ppw.common.tests.ValueGenerator;
import com.hb.amp.ppw.modules.system.bo.Dictionary;
import com.hb.amp.ppw.modules.system.service.DictionaryService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.RandomUtils;

import java.util.List;

/**
 * @author Yi Ping
 * @version 0.0.1
 * @description
 * @date 2020/12/30 19:07
 */
public class DictionaryGenerator implements ValueGenerator<Long> {

    private long pid;

    public DictionaryGenerator(long pid) {
        this.pid = pid;
    }

//    public DictionaryGenerator() {
//    }

    @Override
    public Long generateValue() {
//        DictionaryService dictionaryService = SpringUtils.bean(DictionaryService.class);
//        if(dictionaryService != null){
//            List<Dictionary> list = dictionaryService.findAllByPid(pid);
//            return CollectionUtils.isEmpty(list)?null: list.get(RandomUtils.nextInt(0, list.size())).getId();
//        }else {
//            return  null;
//        }
        return  null;
    }
}
