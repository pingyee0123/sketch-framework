package cn.com.pingyee.shetch.core.test.netty;

import cn.com.pingyee.sketch.core.util.StreamUtils;

import java.io.IOException;
import java.io.InputStream;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * @author: Yi Ping
 * @date: 2019/10/21 0021 15:11
 * @since: 1.0.0
 */
public class NettyTest {

    public static void main(String[] args) throws IOException {
        ServerSocket serverSocket = new ServerSocket(8080);
        System.out.println("---start");
        while (true){
            Socket s = serverSocket.accept();

            new Thread(()-> {
                try {
                    InputStream resourceAsStream = Thread.currentThread().getContextClassLoader().getResourceAsStream("src/test/resources/static/index.html");
                    StreamUtils.copy(resourceAsStream, s.getOutputStream());
                    s.getOutputStream().flush();
                    s.getOutputStream().close();
                    s.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }).start();
        }


    }
}
