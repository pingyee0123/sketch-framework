package cn.com.pingyee.shetch.core.test;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.junit.Test;

import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;

/**
 * @author Yi Ping
 * @version 1.0
 * @date 2020/5/19 21:33
 */

@Slf4j
public class UncaughtExceptionHandlerTest {

    @Test
    public  void test() throws InterruptedException {
        Runnable r = () -> {
            if (RandomUtils.nextBoolean()) {
                throw new RuntimeException("aa");
            } else {
                log.info("success");
            }
        };
        Thread t = new Thread(r);
        t.setUncaughtExceptionHandler((t1,e)->{
                log.info("exception message:  {} \n {}", e.getMessage(), ExceptionUtils.getStackTrace(e)); }
        );
        t.start();
        t.join();
    }

    @Test
    public void streamTest(){
        Integer reduce = Stream.of(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12)
                .parallel()
                .reduce((i1, i2) -> {
                    log.info("i1, i2, {}, {}", i1, i2);
                    return i1 + i2;
                }).get();
//                .reduce(0, (u, i) -> {
//                    log.info("{}", i);
//                    return u + i;
//                }, (u1, u2) -> u1 + u2);

        log.info("sum {}", reduce);
    }
}
