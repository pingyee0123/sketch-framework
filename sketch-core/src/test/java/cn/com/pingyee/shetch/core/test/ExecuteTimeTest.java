package cn.com.pingyee.shetch.core.test;

import org.junit.After;
import org.junit.Before;

/**
 * @author: Yi Ping
 * @date: 2019/11/18 0018 18:01
 * @since: 1.2.0
 */
public class ExecuteTimeTest {
    private long start;
    private long count = 99999999L;



    @Before
    public void before() throws NoSuchMethodException {
        start = System.currentTimeMillis();
    }

    @After
    public void after(){
        System.out.println("共耗时==== " +  (System.currentTimeMillis()-start) );
    }


    public void repeteTest(long count, Runnable runnable){
        for(int i=0; i<count;i++){
            runnable.run();
        }
    }


    public void repeteTest(Runnable runnable){
        repeteTest( count,runnable);
    }
}
