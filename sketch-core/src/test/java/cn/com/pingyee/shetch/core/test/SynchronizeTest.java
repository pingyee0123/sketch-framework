package cn.com.pingyee.shetch.core.test;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author Yi Ping
 * @version 1.0.0-SNAPSHOT
 * @date 2020/5/12 19:48
 */
public class SynchronizeTest {

    public static void main(String[] args){
        long start = System.currentTimeMillis();

        Person p = new Person();

        Runnable r = () -> {
            for(int i=0;i<10000;i++) {
                p.get();
//                System.out.println(Thread.currentThread().getName() + " " + p.get());
            }
        };
        List<Thread> threads = new ArrayList<>();
        for(int i=0; i<1000;i++){
            threads.add(new Thread(r));
        }
        threads.forEach(s->{s.setDaemon(false); s.start();});


        threads.forEach(s-> {
            try {
                s.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });


        System.out.println("www was " + p.get());
        System.out.println("time was " + ( System.currentTimeMillis()  - start) );

    }


    public static class Person{
//        private AtomicInteger count = new AtomicInteger();
//
//        public  int get(){
//            return count.incrementAndGet();
//        }

        private int count;

        public synchronized int get(){
            return ++count;
        }
    }



}
