package cn.com.pingyee.shetch.core.test;

import com.maximeroussy.invitrode.WordGenerator;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

import java.util.Random;

/**
 * @author Yi Ping
 * @version 1.0
 * @date 2020/5/18 22:59
 */

@Slf4j
public class RandomGenerateEnglishPronounceableWordsTest {

    RandomStrings randomStrings = new RandomStrings();

    @Test
    public void testRandomStrings(){
        log.info("random string {}", randomStrings.randomString(8));
    }


    @Test
    public void testInvitrode(){
        WordGenerator wordGenerator = new WordGenerator();
        String word = wordGenerator.newWord(RandomUtils.nextInt(4,10));
        log.info("word was {},{}", word.toLowerCase(), word.toUpperCase());
    }
    /**
     * Created by Muhammad Rizwan on 11/9/2016.
     */
    private static class RandomStrings {

        char[] alp = new char[26];

        public RandomStrings()
        {
            int j =0;
            for (int i = 97;i <= 122;i++){
                alp[j] = (char)i;
                j++;
            }
        }

        public String randomString(int length)
        {
            String s = "";
            for (int i =0;i <=length;i++)
            {
                Random r = new Random();
                s += alp[r.nextInt(alp.length)];
            }
            return s;
        }
        public String randomString()
        {
            String s = "";
            Random rr = new Random();
            int j = rr.nextInt(10);
            for (int i =0;i <=j;i++)
            {
                Random r = new Random();
                s += alp[r.nextInt(alp.length)];
            }
            return s;
        }
        public String randomParagraph(int words)
        {
            String s = "";
            for(int i = 0 ;i <= words;i++)
            {
                Random r = new Random();
                s += " " + randomString(r.nextInt(9));
                int j = r.nextInt(15);
                if(j >=4 && j <=6)
                    s += ".";
            }
            if(s.substring(s.length()-1,s.length()) != ".")
                s += ".";
            return s;
        }
        public String randomParagraph()
        {
            String s = "";
            Random rr = new Random();
            int jj = rr.nextInt(35);
            for(int i = 0 ;i <= jj;i++)
            {
                Random r = new Random();
                s += " " + randomString(r.nextInt(9));
                int j = r.nextInt(15);
                if(j >=4 && j <=6)
                    s += ".";
            }
            if(s.substring(s.length()-1,s.length()) != ".")
                s += ".";
            return s;
        }

        public String randomPage(int paragraph)
        {
            String s = "";
            for(int i = 0 ;i <= paragraph;i++)
            {
                Random r = new Random();
                int j = r.nextInt(10) + r.nextInt(50);
                s += randomParagraph(j) + "\n" ;
            }
            return s;
        }

        public String randomPage()
        {
            String s = "";
            Random rr = new Random();
            int jj = rr.nextInt(4) + 2;
            for(int i = 0 ;i <= jj;i++)
            {
                Random r = new Random();
                int j = r.nextInt(10) + r.nextInt(50);
                s += randomParagraph(j) + "\n" ;
            }
            return s;
        }
    }
}
