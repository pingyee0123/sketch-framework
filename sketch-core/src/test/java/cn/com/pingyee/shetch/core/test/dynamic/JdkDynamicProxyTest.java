package cn.com.pingyee.shetch.core.test.dynamic;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * @author Yi Ping
 * @version 1.0
 * @date 2020/5/21 14:57
 */

@Slf4j
public class JdkDynamicProxyTest {

    @Test
    public void test(){
        Flyable flyable = (Flyable) Proxy.newProxyInstance(getClass().getClassLoader(), new Class[]{Flyable.class}, new JdkInvocation(new FlyImpl()));
        flyable.fly();

    }


    public interface Flyable{

         void fly();
    }


    public static class FlyImpl implements Flyable{

        @Override
        public void fly() {
            log.info("flying");
        }
    }

    public static class JdkInvocation implements InvocationHandler{

        public JdkInvocation(Object target) {
            this.target = target;
        }

        private Object target;

        @Override
        public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
            log.info("start");
            Object result = method.invoke(target, args);
            log.info("end");

            return  result;
        }
    }
}
