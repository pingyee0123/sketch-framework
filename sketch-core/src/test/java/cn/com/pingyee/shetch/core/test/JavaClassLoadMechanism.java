package cn.com.pingyee.shetch.core.test;

import org.junit.Test;

import java.math.BigDecimal;

/**
 * @author Yi Ping
 * @version 2.1
 * @date 2020/11/27 15:45
 */
public class JavaClassLoadMechanism {

    public static class Supper{
        protected static int value =123;
        static {
            System.out.println("Supper init !");
        }
    }

    public static class SubClass extends Supper{
        static {
            System.out.println("Sub init !");
        }
    }


    @Test
    public  void  testmain(){
        System.out.println(SubClass.value);
    }

    @Test
    public void notInitialization(){
        Supper[] suppers = new Supper[10];
        Supper[] suppers1 = suppers.clone();
    }

    public static class ConstClass{

        static {
            System.out.println("ConstClass init!");
        }

        public static final String HELLOWORLD="hello world";
        public static final int ONE = 1;
        static final BigDecimal TEN = BigDecimal.TEN;
        static final Integer TWO =2;
    }

    @Test
    public void constTest(){
        System.out.println(new ConstClass().HELLOWORLD);
    }

    @Test
    public void test2(){
        System.out.println(ConstClass.ONE);
    }

    @Test
    public void test3(){
        System.out.println(ConstClass.TEN);
    }

    @Test
    public void  test4(){
        System.out.println(ConstClass.TWO);
    }


    public static class DeadLoopClass{
        static {
            if(true){
                System.out.println(Thread.currentThread() + "init DeadLoopClass");
                while (true){

                }
            }
        }
    }


    public static void main(String[] args) {
        Runnable script = new Runnable() {
            @Override
            public void run() {
                System.out.println(Thread.currentThread() + "start");
                DeadLoopClass d = new DeadLoopClass();
                System.out.println(Thread.currentThread() + "end");
            }
        };

        Thread thread1 = new Thread(script, "thread1");
        Thread thread2 = new Thread(script, "thread2");
        thread1.start();
        thread2.start();

        try {
            thread1.join();
            thread2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
