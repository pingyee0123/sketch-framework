package cn.com.pingyee.shetch.core.test.netty;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

/**
 * @author: Yi Ping
 * @date: 2019/10/21 0021 15:32
 * @since: 1.0.0
 */
public class TimeServerHandler implements Runnable {

    private Socket socket;

    public TimeServerHandler(Socket socket) {
        this.socket = socket;
    }

    @Override
    public void run() {
        BufferedReader in = null;
        PrintWriter out = null;

        try{
            in = new BufferedReader(new InputStreamReader(this.socket.getInputStream()));
            out = new PrintWriter(this.socket.getOutputStream(), true);
            while (true){
                out.println("CURRENT TIME WAS " +  System.currentTimeMillis());
            }
        }catch (Exception e){

        }finally {
            if(in != null){
                try {
                    in.close();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }

            if(out != null){
                out.close();
            }

            if(this.socket != null){
                try {
                    this.socket.close();
                    this.socket = null;
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }
}
