package cn.com.pingyee.shetch.core.test.netty.aio;

import java.io.IOException;

/**
 * @author: Yi Ping
 * @date: 2019/10/22 0022 10:30
 * @since: 1.0.0
 */
public class AioTimeServer {

    public static void main(String[] args) throws IOException{
        int port = 8080;
        AsyncTimeServerHandler timeServer = new AsyncTimeServerHandler(port);
        new Thread(timeServer, "AIO-AsyncTimeServerHandler-001").start();

    }

}
