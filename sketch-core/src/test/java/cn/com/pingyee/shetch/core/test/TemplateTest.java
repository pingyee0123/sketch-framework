package cn.com.pingyee.shetch.core.test;

import org.junit.Test;

/**
 * @author Yi Ping
 * @version 1.0
 * @date 2020/5/23 21:50
 */
public class TemplateTest {

    @Test
    public void test(){
        find("ss", new Findable<String>() {
            @Override
            public String find() {
                return "ss";
            }
        });
    }

    public <T, U extends Findable<T>> T find(T t, U u ){
        return u.find();
    }

    interface  Findable<T>{

        T find();
    }



    class  FindClass<T>{

    }


    static class FindableInteger implements Findable<Integer>{

        @Override
        public Integer find() {
            return  1;
        }
    }


    static class FindableString implements Findable<String>{

        @Override
        public String find() {
            return "findableString";
        }
    }
}
