package cn.com.pingyee.shetch.core.test.spi;

/**
 * @author Yi Ping
 * @version 2.0
 * @date 2020/7/21 23:16
 */
public interface IShout {
    public void shout();
}
