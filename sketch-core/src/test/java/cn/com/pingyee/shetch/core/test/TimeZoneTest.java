package cn.com.pingyee.shetch.core.test;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

import java.util.Date;
import java.util.TimeZone;

/**
 * @author Yi Ping
 * @version 1.0
 * @date 2020/5/18 16:56
 */

@Slf4j
public class TimeZoneTest {

    @Test
    public void test(){
        TimeZone zone = TimeZone.getDefault();
        log.info("default zone was {}", zone.getID());
    }

    @Test
    public  void getTimeStamp(){
        log.info("time was {}", new Date().getTime() ); ; // 1589792741050 gmt8
        // 1589792796507
        log.info("dis was {}" , new Date().getTime() - 1589793038949L );

        log.info("time was {}", new Date(1589793038949L));
    }

    @Test
    public void testSystemCurrentTimeInMillis(){
        long millis = 1589794027565L;
        log.info("system current time in millis {}",System.currentTimeMillis());
        log.info("diff was {}", (System.currentTimeMillis()-millis)/1000);
    }
}
