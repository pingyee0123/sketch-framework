package cn.com.pingyee.shetch.core.test.spi;

import java.util.ServiceLoader;

/**
 * @author Yi Ping
 * @version 2.0
 * @date 2020/7/21 23:21
 */
public class SpiMain {

    public  static void  main(String[] args){
        ServiceLoader<IShout> shouts = ServiceLoader.load(IShout.class);
        shouts.forEach(s-> s.shout());
    }
}
