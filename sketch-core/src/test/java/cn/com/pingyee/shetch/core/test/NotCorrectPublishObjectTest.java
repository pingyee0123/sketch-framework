package cn.com.pingyee.shetch.core.test;

import org.junit.Test;

import java.util.concurrent.TimeUnit;

/**
 * @author Yi Ping
 * @version 1.0
 * @date 2020/4/6 23:42
 */
public class NotCorrectPublishObjectTest {

    private Holder holder;

    @Test
    public void test(){
        new Thread(()-> holder = new Holder(10)).start();
        new Thread(()->{
            holder.assertSantiy();
        }).start();
    }


    public static class Holder{
        private int n;
        public Holder(int n){
            this.n = n;
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        public void assertSantiy(){
            if(n!=this.n){
                throw new AssertionError("this statement is false");
            }
        }
    }
}
