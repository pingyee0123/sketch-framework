package cn.com.pingyee.shetch.core.test;

import org.junit.Assert;
import org.junit.Test;

import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author Yi Ping
 * @version 1.0.0-SNAPSHOT
 * @date 2020/5/12 20:41
 */
public class ConcurrentModificationExceptionTest {

    @Test
    public void test(){
        List<Integer> list = Stream.of(1, 2, 3, 4, 5, 6).collect(Collectors.toList());
        Exception exception = null;
        try {
            list.forEach(l -> {
                System.out.println(l);
                if (l == 4) {
                    list.remove(l);
                }
            });
        }catch (ConcurrentModificationException e){
            exception = e;
            e.printStackTrace();
        }

        Assert.assertNotNull(exception);
    }

    @Test
    public void test2(){
        List<Integer> list = Stream.of(1, 2, 3, 4, 5, 6).collect(Collectors.toList());
        Iterator<Integer> iterator = list.iterator();
        while (iterator.hasNext()){
            Integer i = iterator.next();
            System.out.println(i);
            if(i ==4){
                iterator.remove();
            }
        }
    }
}
