package cn.com.pingyee.sketch.core.response;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.SequenceInputStream;
import java.net.URL;
import java.net.URLConnection;
import java.security.AccessController;
import java.security.PrivilegedActionException;
import java.security.PrivilegedExceptionAction;
import java.util.*;

/**
 * <p> 默认常用的系统错误码 </p>
 * @author: Yi Ping
 * @date: 2019/9/24 0024 15:36
 * @since: 1.0.0
 */
public abstract  class ErrorCodes {
    private static final Map<String, String> ERROR_CODES = new TreeMap<>();

    public static final String ERROR_CODE_CONFIG_FILE_NAME = "error-code";

    static {
        /*  系统默认错误提示 **/
        ResourceBundle errorCodes = ResourceBundle.getBundle(ERROR_CODE_CONFIG_FILE_NAME, JarsControl.INSTANCE);
        for(String code: errorCodes.keySet()){
            ERROR_CODES.put(code, errorCodes.getString(code));
        }
    }

    public static final  String SUCCESS                 = "0";
    public static final String DEFAULT_ERROR           = "100001";
    public static final String DEFAULT_SERVER_ERROR    = "500002";
    public static final String RESOURCE_LOGIC_DELETED = "100003";
    public static final String RESOURCE_PROPERTY_NOT_EXIST = "100004";
    public static final String RESOURCE_NOT_EXIST      = "100005";
    public static final String RESOURCE_NOT_FOUND      = "100006";
    public static final String PARAMETER_ERROR         = "400007";
    public static final String PARAMETER_INVALIDATED   = "400008";
    public static final String FILE_SIZE_EXCEED        = "400009";
    public static final String PARAMETER_MISSING        ="400010";
    public static final String DUPLICATE_SUBMISSION     = "400011";
    public static final String IMPORT_EXCEL_FILE_DATA_ERROR = "100012";
    public static final String IMPORT_EXCEL_FILE_ERROR  ="100013";

    public static Map<String, String> errorCode(String code){
        Map<String,String> map = new HashMap<>(16);
        map.put(code,ERROR_CODES.get(code));
        return map;
    }

    /**
     * <p> 如果code 为空， 则返回所有的错误码</p>
     * @return
     */
    public static Map<String, String> errorCodes(){
        return new HashMap<>(ERROR_CODES);
    }

    /**
     * <p> 返回错误消息模板</p>
     * @param code
     * @return
     */
    public static String messageTemplate(String code) {
       return ERROR_CODES.get(code);
    }



    /**
     * <p>  加载所有jar 包下的ResourceBundle </p>
     * <p> 如 error-code.properties 在 1.jar 和 2.jar 中都有， 则都会加载</p>
     * @author Yi Ping
     * @version 1.0
     * @date 2020/5/7 14:47
     */
    private static class JarsControl extends ResourceBundle.Control {

        public static JarsControl INSTANCE = new JarsControl();

        private JarsControl(){
        }

        @Override
        public ResourceBundle newBundle(String baseName, Locale locale, String format, ClassLoader loader, boolean reload) throws IllegalAccessException, InstantiationException, IOException {

            String bundleName = toBundleName(baseName, locale);
            ResourceBundle bundle = null;
            if (format.equals("java.class")) {
                try {
                    @SuppressWarnings("unchecked")
                    Class<? extends ResourceBundle> bundleClass
                            = (Class<? extends ResourceBundle>)loader.loadClass(bundleName);

                    // If the class isn't a ResourceBundle subclass, throw a
                    // ClassCastException.
                    if (ResourceBundle.class.isAssignableFrom(bundleClass)) {
                        bundle = bundleClass.newInstance();
                    } else {
                        throw new ClassCastException(bundleClass.getName()
                                + " cannot be cast to ResourceBundle");
                    }
                } catch (ClassNotFoundException e) {
                }
            } else if (format.equals("java.properties")) {
                final String resourceName = toResourceName(bundleName, "properties");
                if (resourceName == null) {
                    return bundle;
                }
                final ClassLoader classLoader = loader;
                final boolean reloadFlag = reload;
                InputStream stream = null;
                try {
                    stream = AccessController.doPrivileged(
                            new PrivilegedExceptionAction<InputStream>() {
                                public InputStream run() throws IOException {
                                    List<InputStream> iss = new ArrayList<>();
                                    Enumeration<URL> urls = classLoader.getResources(resourceName);
                                    while (urls.hasMoreElements()){
                                        URL url = urls.nextElement();
                                        URLConnection connection = url.openConnection();
                                        if(connection != null){
                                            connection.setUseCaches(false);
                                            iss.add(connection.getInputStream());
                                            /**  流与流之间加入换行  **/
                                            iss.add(new ByteArrayInputStream(new byte[]{'\n'}));
                                        }
                                    }
                                    return   new SequenceInputStream(Collections.enumeration(iss));
                                }
                            });
                } catch (PrivilegedActionException e) {
                    throw (IOException) e.getException();
                }
                if (stream != null) {
                    try {
                        bundle = new PropertyResourceBundle(stream);
                    } finally {
                        stream.close();
                    }
                }
            } else {
                throw new IllegalArgumentException("unknown format: " + format);
            }
            return bundle;
        }


    }



}
