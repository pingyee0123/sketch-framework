package cn.com.pingyee.sketch.core.batchoperation;

import cn.com.pingyee.sketch.core.exception.ApiException;
import cn.com.pingyee.sketch.core.exception.BusinessException;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Yi Ping
 * @version 2.1
 * @date 2020/8/15 22:00
 */
public interface BatchOperationService<M extends SingleOperatee<K>, K extends Serializable> {

    void doSingle(M m) throws BusinessException;

   default  BatchOperationApiResponse  doBatch(List<M> models) throws BusinessException{
       check(models);

       List<FailCause<K>> failCauses = new ArrayList<>();
       for(M m: models){
           try{
               doSingle(m);
           }catch (Exception e){
               failCauses.add(new FailCause<>(m.getKey(), e.getMessage()));
           }
       }
       return BatchOperationApiResponse.newBatchOperationApiResponse(models, failCauses);
   }


   default  void check(List<M> models) throws  BusinessException{

   }
}
