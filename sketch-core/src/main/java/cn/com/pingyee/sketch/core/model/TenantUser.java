package cn.com.pingyee.sketch.core.model;

import java.io.Serializable;

/**
 * <p> 多租户系统中 的用户</p>
 * @author Yi Ping
 * @version 1.0
 * @date 2020/5/9 10:19
 *  ID  用户Id
 *  TID 租户Id
 */
public interface TenantUser<ID extends Serializable, TID extends  Serializable>  extends User<ID>{

    /**
     * <p> 返回租户Id  </p>
     * @return
     */
    TID getTenantId();

}
