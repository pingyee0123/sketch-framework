package cn.com.pingyee.sketch.core.util;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * @author: Yi Ping
 * @date: 2019/10/28 0028 16:32
 * @since: 1.0.0
 */
public class MD5Utils {

    public static String md5ToHex(String str)
    {
        Assert.isNotNull(str, "str can not be null");
        return md5ToHex(str,0);
    }


    /**
     * <p> md5签名 </p>
     * @param str  需要md5签名的字符
     * @param offset 签名字符的起始位置
     * @return
     */
    private static String md5ToHex(String str, int offset)
    {
        try
        {
            MessageDigest md5 = MessageDigest.getInstance("MD5");
            byte[] b = str.getBytes("UTF8");
            md5.update(b, offset, b.length);
            return byteArrayToHexString(md5.digest());
        }
        catch (NoSuchAlgorithmException ex)
        {
            ex.printStackTrace();
            return null;
        }
        catch (UnsupportedEncodingException ex)
        {
            ex.printStackTrace();
            return null;
        }
    }

    /**
     *
     * @param b byte[]
     * @return String
     */
    private static String byteArrayToHexString(byte[] b)
    {
        String result = "";
        for (int i = 0; i < b.length; i++)
        {
            result += byteToHexString(b[i]);
        }
        return result;
    }

    private static String[] hexDigits =
            {
                    "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b",
                    "c", "d", "e", "f"};

    /**
     *
     * @param b
     * @return
     */
    private static String byteToHexString(byte b)
    {
        int n = b;
        if (n < 0)
        {
            n = 256 + n;
        }
        int d1 = n / 16;
        int d2 = n % 16;
        return hexDigits[d1] + hexDigits[d2];
    }


}
