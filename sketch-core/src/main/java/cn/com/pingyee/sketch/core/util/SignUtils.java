package cn.com.pingyee.sketch.core.util;

import org.apache.commons.lang3.StringUtils;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Map;
import java.util.TreeMap;

/**
 * <p> 签名工具类 </p>
 * @author Yi Ping
 * @version 2.1
 * @date 2020/8/12 17:03
 */
public abstract class SignUtils {

    public static String sign(Map<String, String> param, String key, String charset, boolean ignoreBlankValue) throws UnsupportedEncodingException {
        Assert.isNotNull(param, "param can not be null");
//        Assert.isNotBlank(key, "key can not be blank");

//        List<String> keys = param.keySet().stream().collect(Collectors.toList());
//        keys.sort(String::compareTo);
        Map<String,String> sortedMap = new TreeMap<>(String::compareTo);
        sortedMap.putAll(param);
        StringBuilder builder = new StringBuilder();
        for (Map.Entry<String,String> entry : sortedMap.entrySet()){
            String k = entry.getKey();
            String v = entry.getValue();
            if(StringUtils.isBlank(v) && ignoreBlankValue){
                ;
            }else {
               builder.append(k).append('=').append(URLEncoder.encode(v,charset)).append('&');
            }
        }
        if(builder.length() >0) {
            builder.deleteCharAt(builder.length() - 1);
        }
        if(StringUtils.isNotBlank(key)) {
            builder.append(key);
        }

        return MD5Utils.md5ToHex(builder.toString()).toUpperCase();
    }

    public static String sign(Map<String, String> param, String key, boolean ignoreBlankValue){
        try {
            return sign(param, key, "utf-8", ignoreBlankValue);
        } catch (UnsupportedEncodingException e) {
            return  null;
        }
    }

    public static String sign(Map<String,String> param, String key){
        return sign(param, key, false);
    }

    public static String signIgnoreBlankValue(Map<String,String> param){
        return sign(param, null, true);
    }

}
