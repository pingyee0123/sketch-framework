package cn.com.pingyee.sketch.core.support;

import cn.com.pingyee.sketch.core.support.id.IdGenerator;

/**
 */

public class PrefixIdGenerator implements IdGenerator<String> {

    private String prefix;
    private IdGenerator idGenerator;

    public PrefixIdGenerator(String prefix, IdGenerator idGenerator) {
        this.prefix = prefix;
        this.idGenerator = idGenerator;
    }

    @Override
    public String generateId() {
        return  prefix + idGenerator.generateId();
    }

}
