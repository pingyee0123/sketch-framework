package cn.com.pingyee.sketch.core.model;

import java.io.Serializable;

/**
 * @author Yi Ping
 * @version 1.0
 * @date 2020/5/9 10:17
 */
public interface User<ID extends Serializable> {

    /**
     * <p> 返回用户Id</p>
     * @return
     */
    ID getId();
}
