package cn.com.pingyee.sketch.core.response;


import cn.com.pingyee.sketch.core.exception.ApiException;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;
import java.text.MessageFormat;

//@JsonInclude(value= Include.NON_NULL)
//保证序列化json的时候,如果是null的对象,key也会消失

/**
 * @author  Yi Ping
 * @param <T>
 */
@Data
@ApiModel(description = "api返回的标准格式json")
@Slf4j
public class ApiJson<T> implements ApiResponse, Serializable {

    private static final long serialVersionUID = 7000760666908532411L;

    private static ApiJson<?> SUCCESS =  new ApiJson<>(ErrorCodes.SUCCESS);


    @ApiModelProperty("返回码, 0表示成功, 其他表示失败")
    private String code;

    @ApiModelProperty("api 返回数据data是否加密")
    private boolean secure = false;

    @ApiModelProperty("消息提示")
    private String message;


    @ApiModelProperty("返回数据")
    private T data;


//    static {
//        /*  系统默认错误提示 **/
//        ResourceBundle defaultErrorCodes = ResourceBundle.getBundle("error-code");
//        for(String code: defaultErrorCodes.keySet()){
//            ERROR_CODES.put(code, defaultErrorCodes.getString(code));
//        }
////        /* 用户自定义错误提示 **/
//////        ResourceBundle errorCodes = ResourceBundle.getBundle("errorcode");
//////        for(String code: errorCodes.keySet()){
//////            ERROR_CODES.put(code, errorCodes.getString(code));
//////        }
//
//        ERROR_CODES.forEach((k,v)->{
//            try {
//                ERROR_CODES_INTEGER.put(Integer.parseInt(k), v);
//            }catch (Exception e){
//                if(log.isWarnEnabled()) {
//                    log.warn(e.getMessage(), e);
//                }
//            }
//        });
//    }

    /**
     * json 序列化需要, 请勿在实际代码中使用
     */
    public ApiJson() {
    }


//    private ApiJson(int code){
//        if(code == SUCCESS_CODE || ERROR_CODES_INTEGER.containsKey(code)) {
//            this.code = code;
//            this.message = ERROR_CODES_INTEGER.get(code);
//            setSuccess();
//        }else {
//            throw new IllegalArgumentException("error code：" + code + " undefined!");
//        }
//    }


//    private ApiJson(int code, String... wildcards){
//        if(code == SUCCESS_CODE || ERROR_CODES_INTEGER.containsKey(code)) {
//            this.code = code;
//            this.message = MessageFormat.format(ERROR_CODES_INTEGER.get(code), (Object[]) wildcards);
//            setSuccess();
//        }else {
//            throw new IllegalArgumentException("error code：" + code + " undefined!");
//        }
//    }


    private ApiJson(String code, String message, T data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public ApiJson(String code, String message) {
        this(code, message, null);
    }

    private ApiJson(T data){
        this(ErrorCodes.SUCCESS, MessageFormat.format(ErrorCodes.messageTemplate(ErrorCodes.SUCCESS),new Object[]{}), data);
    }

    /**
     * <p> 成功返回不带数据的 ApiJson  </p>
     * @return
     */
    public static ApiJson success(){
        return  SUCCESS;
    }

    /**
     * <p> 成功返回带数据 <code> data </code> ApiJson </p>
     * @param data
     * @param <E>
     * @return
     */
    public static  <E>  ApiJson<E> success(E data){
        return new ApiJson<>(data);
    }


    /**
     * <p> 请求失败返回错误码code  </p>
     * @param code 须为数字
     * @return
     */

    public static ApiJson<?> error(String code,String... message){
        return new ApiJson<>(code, MessageFormat.format(ErrorCodes.messageTemplate(code), message));
    }

    public static ApiJson<?> error(String code){
        return error(code, new String[]{});
    }

    /**
     * <p> 请求失败返回</p>
     * @param e
     * @return
     */
    public static ApiJson<?> exception(ApiException e){
        return  new ApiJson<>(e.getErrorCode(), e.getMessage());
    }

    public static ApiJson<?> exception(Throwable e){
        if(e instanceof  ApiException){
            return exception((ApiException) e);
        }else {
            return error(ErrorCodes.DEFAULT_SERVER_ERROR, e.getMessage());
        }
    }



    /**
     * <p>判断请求是否成功</p>
     * @return
     */
    @Override
    @ApiModelProperty(value = "是否成功")
    public boolean isSuccess() {
        return ErrorCodes.SUCCESS.equals(this.code);
    }




}
