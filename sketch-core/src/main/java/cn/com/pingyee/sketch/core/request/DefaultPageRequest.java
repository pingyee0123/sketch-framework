package cn.com.pingyee.sketch.core.request;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p> 默认的分页请求</p>
 * @author: Yi Ping
 * @create: 2019-02-21 11:04
 * @since: 1.3.0
 */

@Getter
@Setter
public class DefaultPageRequest  implements PageRequest  {

    public static final DefaultPageRequest MAX = new DefaultPageRequest(0, Integer.MAX_VALUE);


    @ApiModelProperty("页码, 0为第一页, 默认0")
    private int page =0;
    @ApiModelProperty("每页显示多少条, 默认20条")
    private int size =10;
    @ApiModelProperty("排序, eg: 按id降序排列: id,desc; 按id升序排列:id,asc 或 id； 按id, addTime降序排列：id, addTime,desc ")
    private String sort;

    public DefaultPageRequest() {
    }

    public DefaultPageRequest(int page, int size) {
        this.page = page;
        this.size = size;
    }
}
