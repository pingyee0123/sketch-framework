package cn.com.pingyee.sketch.core.util;

/**
 * @deprecated  仅作为调试使用
 * @author: YiPing
 * @create: 2018-11-09 17:52
 * @since: 1.0
 */

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;

import javax.imageio.ImageIO;
import javax.imageio.stream.ImageOutputStream;
import javax.validation.constraints.Min;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.Random;

@Slf4j
public class ImageUtil {
    /**
     * <p>图像</p>
     */
    private ByteArrayInputStream image;
    /** 验证码 **/
    private String code;
    /** 验证码长度 **/
    private int  length = 4 ;
    /** 图片宽度 **/
    private int width = 85;
    /** 图片高度 **/
    private int height = 20;

    public ImageUtil() {
        this(4,85,20);
    }

    public ImageUtil(@Min(1) int length) {
        this(length, 85,20);
    }

    public ImageUtil(int length, int width, int height) {
        Assert.isTrue(length <1, "code string length should be great than 1");
        Assert.isTrue(width <80, "image width should be great than 80");
        Assert.isTrue(height <20, "image height should be great than 20");

        this.length = length;
        this.width = width;
        this.height = height;
        init();
    }

    public ImageUtil(int width, int height) {
        this(4,width,height);
    }

    public static ImageUtil instance(int length){
        return  new ImageUtil(length);
    }

    /**
     * 取得RandomNumUtil实例
     */
    public static ImageUtil instance() {
        return new ImageUtil();
    }

    /**
     * 取得验证码图片
     */
    public ByteArrayInputStream getImage() {
        return this.image;
    }

    /**
     * 取得图片的验证码
     */
    public String getCode() {
        return this.code;
    }

    private void init() {
        // 在内存中创建图象
        int width = this.width, height = this.height;
        BufferedImage image = new BufferedImage(width, height,
                BufferedImage.TYPE_INT_RGB);
        // 获取图形上下文
        Graphics g = image.getGraphics();
        // 生成随机类
        Random random = new Random();
        // 设定背景色
        g.setColor(getRandColor(200, 250));
        g.fillRect(0, 0, width, height);
        // 设定字体
            // 每个字符的平均宽度
        int perlength = (width-6) /length;
        int fontsize = (int) Math.min(perlength,height * 0.8);
        g.setFont(new Font("Times New Roman", Font.PLAIN, fontsize));
        // 随机产生155条干扰线，使图象中的认证码不易被其它程序探测到
        g.setColor(getRandColor(160, 200));
        for (int i = 0; i < 155; i++) {
            int x = random.nextInt(width);
            int y = random.nextInt(height);
            int xl = random.nextInt(12);
            int yl = random.nextInt(12);
            g.drawLine(x, y, x + xl, y+yl);
        }
        // 取随机产生的认证码( 默认4位数字)
        String sRand = RandomStringUtils.randomAlphanumeric(length);

        for (int i = 0; i < length; i++) {
            char rand =sRand.charAt(i);
            // 将认证码显示到图象中
            g.setColor(new Color(20 + random.nextInt(110), 20 + random
                    .nextInt(110), 20 + random.nextInt(110)));
            // 调用函数出来的颜色相同，可能是因为种子太接近，所以只能直接生成
            g.drawString(rand + "", perlength * i + 6, RandomUtils.nextInt(fontsize,height-2));
        }
        // 赋值验证码
        this.code = sRand;

        // 图象生效
        g.dispose();
        ByteArrayInputStream input = null;
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        try {
            ImageOutputStream imageOut = ImageIO
                    .createImageOutputStream(output);
            ImageIO.write(image, "JPEG", imageOut);
            imageOut.close();
            input = new ByteArrayInputStream(output.toByteArray());
        } catch (Exception e) {
            log.error("验证码图片产生出现错误：" + e.toString());
        }
        // 赋值图像
        this.image = input;
    }

    /**
     * 给定范围获得随机颜色
     */
    private Color getRandColor(int fc, int bc) {
        Random random = new Random();
        if (fc > 255) {
            fc = 255;
        }
        if (bc > 255) {
            bc = 255;
        }
        int r = fc + random.nextInt(bc - fc);
        int g = fc + random.nextInt(bc - fc);
        int b = fc + random.nextInt(bc - fc);
        return new Color(r, g, b);
    }
}