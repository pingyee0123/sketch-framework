package cn.com.pingyee.sketch.core.support.id;

import cn.com.pingyee.sketch.core.util.Assert;

/**
 * @author Yi Ping
 * @version 2.1
 * @date 2020/7/30 16:31
 */
public class SnowFlakeIdGenerator implements IdGenerator<Long> {
    /** 工作节点位数 **/
    private final long WORKER_ID_BITS = 5L;
    /** 数据中心占用位数 **/
    private final long DATA_CENTER_ID_BITS = 5L;
    /** 序列号占用位数 **/
    private final long SEQUENCE_BITS = 12L;

    /** 工作节点的最大值 **/
    private final long MAX_WORKER_ID = ~(-1L << WORKER_ID_BITS);
    /** 数据中心节点的最大值 **/
    private final long MAX_DATA_CENTER_ID = ~(-1L << DATA_CENTER_ID_BITS);
    //序列号最大值
    private long SEQUENCE_MASK = -1L ^ (-1L << SEQUENCE_BITS);
    //工作id需要左移的位数，12位

    private long workerIdShift = SEQUENCE_BITS;
    //数据id需要左移位数 12+5=17位
    private long datacenterIdShift = SEQUENCE_BITS + WORKER_ID_BITS;
    //时间戳需要左移位数 12+5+5=22位
    private long timestampLeftShift = SEQUENCE_BITS + WORKER_ID_BITS + SEQUENCE_BITS;
    //初始时间戳 2020-07-30
    private long twepoch = 1596099774393L;

    private long workerId;
    private long datacenterId;
    private volatile long sequence;

    public SnowFlakeIdGenerator(long workerId, long datacenterId) {
        Assert.isTrue(workerId <= MAX_WORKER_ID && workerId >0,
                String.format("worker Id can't be greater than %d or less than 0", MAX_WORKER_ID));
        Assert.isTrue(datacenterId>0 && datacenterId <= MAX_DATA_CENTER_ID,
                String.format("datacenter Id can't be greater than %d or less than 0", MAX_DATA_CENTER_ID));

        this.workerId = workerId;
        this.datacenterId = datacenterId;
    }

    //上次时间戳，初始值为负数
    private volatile long lastTimestamp = -1L;

    public long getWorkerId(){
        return workerId;
    }

    public long getDatacenterId(){
        return datacenterId;
    }

    public long getTimestamp(){
        return System.currentTimeMillis();
    }

    @Override
    public synchronized Long generateId() {
        long timestamp = timeGen();
        //获取当前时间戳如果小于上次时间戳，则表示时间戳获取出现异常
        if (timestamp < lastTimestamp) {
            System.err.printf("clock is moving backwards.  Rejecting requests until %d.", lastTimestamp);
            throw new RuntimeException(String.format("Clock moved backwards.  Refusing to generate id for %d milliseconds",
                    lastTimestamp - timestamp));
        }

        //获取当前时间戳如果等于上次时间戳（同一毫秒内），则在序列号加一；否则序列号赋值为0，从0开始。
        if (lastTimestamp == timestamp) {
            sequence = (sequence + 1) & SEQUENCE_MASK;
            if (sequence == 0) {
                timestamp = tilNextMillis(lastTimestamp);
            }
        } else {
            sequence = 0;
        }

        //将上次时间戳值刷新
        lastTimestamp = timestamp;

        /**
         * 返回结果：
         * (timestamp - twepoch) << timestampLeftShift) 表示将时间戳减去初始时间戳，再左移相应位数
         * (datacenterId << datacenterIdShift) 表示将数据id左移相应位数
         * (workerId << workerIdShift) 表示将工作id左移相应位数
         * | 是按位或运算符，例如：x | y，只有当x，y都为0的时候结果才为0，其它情况结果都为1。
         * 因为个部分只有相应位上的值有意义，其它位上都是0，所以将各部分的值进行 | 运算就能得到最终拼接好的id
         */
        return ((timestamp - twepoch) << timestampLeftShift) |
                (datacenterId << datacenterIdShift) |
                (workerId << workerIdShift) |
                sequence;
    }

    //获取时间戳，并与上次时间戳比较
    private long tilNextMillis(long lastTimestamp) {
        long timestamp = timeGen();
        while (timestamp <= lastTimestamp) {
            timestamp = timeGen();
        }
        return timestamp;
    }

    //获取系统时间戳
    private long timeGen(){
        return System.currentTimeMillis();
    }
}
