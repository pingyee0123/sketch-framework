
package cn.com.pingyee.sketch.core.exception;


import cn.com.pingyee.sketch.core.response.ApiJson;
import cn.com.pingyee.sketch.core.response.ErrorCodes;

import java.text.MessageFormat;


/**
 *
 * <p> 接口抛出异常 </p>
 * @author ping
 * @date 2017年12月29日下午1:41:04
 * @since v1.0
 */

public class ApiException extends RuntimeException {

    private final String errorCode;



    public ApiException(String code){
        this(code, new String[]{});
    }


    public ApiException(String code, String ... wildcards){
        this(code,MessageFormat.format(ErrorCodes.messageTemplate(code),(Object[]) wildcards));
    }



    private ApiException(String code, String message){
        this(code, message, null);
    }



    private ApiException(String errorCode, String message, Throwable cause){
        super(message, cause);
        this.errorCode = errorCode;
    }




    public String getErrorCode() {
        return errorCode;
    }

}


