package cn.com.pingyee.sketch.core.annotation;

import java.lang.annotation.*;

/**
 *
 * <p> 提示函数的调用者， 参数值可以为null</p>
 * @author: Yi Ping
 * @date: 2019/10/16 0016 9:09
 * @since: 1.0.0
 */

@Documented
@Retention(RetentionPolicy.CLASS)
@Target({ElementType.FIELD, ElementType.METHOD, ElementType.PARAMETER, ElementType.LOCAL_VARIABLE})
public @interface Nullable {
}
