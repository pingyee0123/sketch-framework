package cn.com.pingyee.sketch.core.util;

import java.util.Arrays;

/**
 *
 * @author ping
 *
 */
public class ArrayUtils {

    /**
     * 合并两个数组
     *
     * @param first
     * @param second
     * @return 返回合并后的数组
     */
    public static <T> T[] concat(final T[] first,final T[] second) {
        if(null == first || first.length==0) {
            return second;
        }
        if(null==second || second.length==0) {
            return first;
        }

        T[] result = Arrays.copyOf(first, first.length + second.length);
        System.arraycopy(second, 0, result, first.length, second.length);
        return result;
    }

    public static <T> T[] concatAll(T[] first, T[][] rest) {
        int totalLength = first==null?0:first.length;
        for (T[] array : rest) {
            totalLength +=(array==null?0: array.length);
        }
        T[] result = Arrays.copyOf(first, totalLength);
        int offset = first.length;
        for (T[] array : rest) {
            System.arraycopy(array, 0, result, offset, array.length);
            offset += array.length;
        }
        return result;
    }
}
