package cn.com.pingyee.sketch.core.support;

import cn.com.pingyee.sketch.core.support.id.IdGenerator;

/**
 *
 */
public class SuffixIdGenerator implements IdGenerator<String> {
    private String suffix;
    private IdGenerator idGenerator;

    public SuffixIdGenerator(String suffix, IdGenerator idGenerator) {
        this.suffix = suffix;
        this.idGenerator = idGenerator;
    }

    @Override
    public String generateId() {
        return idGenerator.generateId() + suffix;
    }
}
