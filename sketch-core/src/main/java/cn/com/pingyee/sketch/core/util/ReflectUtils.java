package cn.com.pingyee.sketch.core.util;


import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * <p> 反射工具类 </p>
 */

public final class ReflectUtils {

    private  static  final Logger logger = LoggerFactory.getLogger(ReflectUtils.class);
    private static boolean debug = logger.isDebugEnabled();

    private  static  final  String NESTED_FIELD_SEPERATOR = ".";

    /**
     * <p> 简单类型</p>
     */
    private  static  final Set<Class> simpleClasses = new HashSet<>();

    /**
     * <p> 没有"属性"的类型， {@link String} 虽然有属性{@link String#hash}, {@link String#value},
     *  但可以认为他是没有属性的，
     * </p>
     */
    private  static  final Set<Class> noPropertyClasses = new HashSet<>();
//    private static final Set<Class> collectionClasses = new HashSet<>();

    static {
        simpleClasses.add(Enum.class);
        simpleClasses.add(void.class);
        simpleClasses.add(Void.class);
        simpleClasses.add(byte.class);
        simpleClasses.add(Byte.class);
        simpleClasses.add(boolean.class);
        simpleClasses.add(Boolean.class);
        simpleClasses.add(int.class);
        simpleClasses.add(Integer.class);
        simpleClasses.add(long.class);
        simpleClasses.add(Long.class);
        simpleClasses.add(short.class);
        simpleClasses.add(Short.class);
        simpleClasses.add(float.class);
        simpleClasses.add(Float.class);
        simpleClasses.add(double.class);
        simpleClasses.add(Double.class);
        simpleClasses.add(BigDecimal.class);
        simpleClasses.add(AtomicInteger.class);
        simpleClasses.add(AtomicBoolean.class);
        simpleClasses.add(AtomicLong.class);
        simpleClasses.add(String.class);
        simpleClasses.add(Date.class);
        simpleClasses.add(LocalDateTime.class);
        simpleClasses.add(LocalDate.class);
        simpleClasses.add(LocalTime.class);

        noPropertyClasses.addAll(simpleClasses);
        noPropertyClasses.add(Object.class);
//
//        collectionClasses.add(Array.class);
//        collectionClasses.add(Collection.class);
    }

    /**
     * <p> Returns an array contains Field objects reflecting all the fields declared  or inherited by
     * the class or interface represented by this Class object. This includes
     * public, protected, default (package) access, and private fields, also
     * includes inherited fields. </p>
     *
     *<p>
     *     如果 clazz 且其父祖类们都未声明 fields， 返回长度为0的数组
     *</p>
     *
     *
     * @param clazz
     * @return
     *
     */
    public static Field[] getAllFields(Class<?> clazz) {
        if (clazz == null) {
            throw new IllegalArgumentException("clazz 不能为null");
        }

        if (clazz != Object.class) {
            Field[] fields = clazz.getDeclaredFields();
            Class<?> superClazz = clazz.getSuperclass();
            Field[] superFields = ReflectUtils.getAllFields(superClazz);
            return ArrayUtils.concat( superFields, fields);
        }
        return new Field[0];
    }

    public  static Field[] getAllFieldsIgnoreStatics(Class<?> clazz){
        Field[] fields = getAllFields(clazz);
        if(fields.length>0){
            List<Field> list = Stream.of(fields).filter(f -> !Modifier.isStatic(f.getModifiers())).collect(Collectors.toList());
            Field[] newFields = new Field[list.size()];
            list.toArray(newFields);
            return  newFields;
        }else {
            return fields;
        }
    }

    /**
     * <p> 获取Field, 如果 <code>clazz</code> 不存在名为<code>fieldName</code> 的field, 则从 <code>clazz</code>
     *  的父祖类中区查找
     * </p>
     * @param clazz
     * @param fieldName
     * @return
     */
    public static Field getField( Class<?> clazz , String fieldName){
        if(clazz==null || StringUtils.isBlank(fieldName)) {
            throw new IllegalArgumentException("clazz can not be null and fieldName can not be empty");
        }

        try {
            return clazz.getDeclaredField(fieldName);
        } catch (NoSuchFieldException | SecurityException e) {
            Class<?> supClazz = clazz.getSuperclass();
            if(supClazz!=null && supClazz != Object.class){
                return getField(supClazz,fieldName);
            }
            return  null;
        }

    }


    public static Field getNestedField(Class<?> clazz, String fieldName) {
        if(clazz==null  || clazz == Object.class
                || fieldName==null || "".equals(fieldName.trim())) {
            return null;
        }
//            throw new IllegalArgumentException("clazz can not be null and fieldName can not be empty");

        if(fieldName.contains(NESTED_FIELD_SEPERATOR)){
            int position = fieldName.indexOf(".");
            String preFieldName = fieldName.substring(0,position);
            String suffixFieldName= fieldName.substring(position + 1);
            Field preField = getField(clazz, preFieldName);
            if(preField != null ){
                return getNestedField((Class<?>)preField.getGenericType(), suffixFieldName);
            }

        }else {
            return  getField(clazz,fieldName);
        }

        return  null;

    }


    public  static boolean isSimpleClass(Class<?> clz){
        if(clz == null) {
            throw new IllegalArgumentException("clz can not be null");
        }

        return simpleClasses.contains(clz)
                || clz.isEnum()
                 || Number.class.isAssignableFrom(clz);
    }

    public static  boolean isSimpleClass(Type type){
        if(type == null) {
            throw new IllegalArgumentException("type can not be null");
        }
        if(type instanceof  Class){
            return  isSimpleClass((Class) type);
        }else {
            return  false;
        }
    }

    public static  boolean isCollectionClass(Class<?> clz){
        if(clz == null) {
            throw new IllegalArgumentException("clz can not be null");
        }
        return  clz.isArray() || Collection.class.isAssignableFrom(clz);
    }

    public static boolean isCollectionType(Type type){
        if(type == null) {
            throw new IllegalArgumentException("clz can not be null");
        }

        if(type instanceof Class){
            return  isCollectionClass((Class<?>) type);
        }else if (type instanceof  ParameterizedType){
            if(((ParameterizedType) type).getRawType() instanceof Class){
                return  isCollectionClass((Class<?>) ((ParameterizedType) type).getRawType());
            }
        }
        return false;
    }


    public static  boolean isNullOrEmpty(Object value){
        if(value == null) {
            return true;
        }
        else {
            Class valueClass = value.getClass();
            if(isCollectionClass(valueClass)){
                return valueClass.isArray()? Array.getLength(value)==0:((Collection) value).size() ==0;
            }else {
                return false;
            }
        }
    }


    public static boolean isNoPropertyClass(Class<?> clz){
        if(clz== null) {
            throw new IllegalArgumentException("clz can not be null");
        }
        return  noPropertyClasses.contains(clz) || clz.isEnum();
    }

    /**
     *
     * @param object
     * @param fieldName
     * @return 如果fieldName为空，返回null
     * @throws NoSuchFieldException
     * @throws SecurityException
     * @throws IllegalArgumentException
     * @throws IllegalAccessException
     * @throws NoSuchMethodException
     * @throws InvocationTargetException
     */
    public static Object getFieldValue(Object object, String fieldName)
    {
        if(object==null || isNoPropertyClass(object.getClass())) {
            return null;
        }
        if(fieldName==null || "".equals(fieldName.trim())) {
            return null;
        }

        try{
            if(fieldName.contains(NESTED_FIELD_SEPERATOR)){
                int position = fieldName.indexOf(NESTED_FIELD_SEPERATOR);
                String preFieldName = fieldName.substring(0,position);
                String suffixFieldName= fieldName.substring(position + 1);
                Field preField = getField(object.getClass(), preFieldName);
                preField.setAccessible(true);
                /*满足 hibernate 延迟加载*/
                Object preObject = getFieldValue(object,preFieldName);
                return getFieldValue(preObject, suffixFieldName);
            }else {
                /**
                 * 通过Method获取值, 已满足hibernate session延迟加载
                 */
                StringBuilder methodName=new StringBuilder().append("get");
                methodName.append(fieldName.substring(0,1).toUpperCase());
                methodName.append(fieldName.substring(1));
                Method method = object.getClass().getMethod(methodName.toString());
                return method.invoke(object);
                /**
                 * 通过Field获取值
                 *
                 Field field = getField(object.getClass(), fieldName);
                 field.setAccessible(true);
                 return field.get(object);
                 */
            }
        }catch ( IllegalArgumentException | IllegalAccessException | NoSuchMethodException | SecurityException | InvocationTargetException e) {
            return null;
        }

    }



    /**
     * 当 fieldName 包含"."时 使用该方法设置field的值,如果object中包含申明类型为接口的属性， 先实例化该属性， 否则会抛出  InstantiationException
     * @param bean
     * @param fieldName
     * @param value
     * @throws IllegalArgumentException
     * @throws IllegalAccessException
     * @throws NoSuchFieldException
     * @throws SecurityException
     * @throws InstantiationException
     * @throws InvocationTargetException
     * @throws NoSuchMethodException
     */
    public static void setFieldValue(Object bean , String fieldName, Object value)
            throws IllegalArgumentException, IllegalAccessException, SecurityException, InstantiationException{
        if(bean==null) {
            throw new IllegalArgumentException();
        }
        if(value==null || fieldName==null || "".equals(fieldName.trim())) {
            return;
        }
        if(fieldName.contains(".")){
            int position = fieldName.indexOf(".");
            String preFieldName = fieldName.substring(0, position);
            String suffixFieldName = fieldName.substring(position + 1);
            Field preField = getField(bean.getClass(), preFieldName);
            Object preFieldValue = getFieldValue(bean, preFieldName);
            if (preFieldValue == null){
                preFieldValue = ((Class<?>)preField.getGenericType()).newInstance();
                preField.setAccessible(true);
                preField.set(bean, preFieldValue);
            }
            setFieldValue(preFieldValue, suffixFieldName, value);
        }else {
            Field field = getField(bean.getClass(), fieldName);
            field.setAccessible(true);
            field.set(bean, value);
        }
    }

    public static Class<?> getFieldClass(Class<?> clazz, String fieldName) {
        if(clazz==null || StringUtils.isBlank(fieldName)) {
            throw new IllegalArgumentException("clazz and fieldName can not be null");
        }

        if(fieldName.contains(NESTED_FIELD_SEPERATOR)){
            int index = fieldName.indexOf(NESTED_FIELD_SEPERATOR);
            String pre = fieldName.substring(0, index);
            String  suff = fieldName.substring(index+1);
            return getFieldClass(getFieldClass(clazz, pre), suff);
        }else {
            Field field = getField(clazz, fieldName);
            Class<?> cls = getParameterizedType(field);
            if(cls != null) {
                return cls;
            }else {
                return (Class<?>) field.getGenericType();
            }
        }
    }

    /**
     * <p> 测试用 </p>
     * @param object
     */
    private static void showObject(Object object){
        if(object == null ) {
            return;
        }
        Class<?> clazz = object.getClass();
        if(clazz.isPrimitive()
                || clazz.isEnum()
                || clazz.isArray()
                || object instanceof Iterable<?>
                || clazz==Date.class
                || clazz== Boolean.class
                || clazz == Integer.class
                || clazz == Long.class
                || clazz == Short.class
                || clazz == Float.class
                || object instanceof Number
                || clazz==String.class
                || clazz ==Byte.class
                || clazz == Character.class
                ){
            System.out.println(object);
            return;
        }

        Field[] fields = getAllFields(object.getClass());
        for (Field field : fields) {
            field.setAccessible(true);
            System.out.println("value of property " + field.getName() + " is :" );
            try {
                showObject(field.get(object));
            } catch (IllegalArgumentException | IllegalAccessException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }


    public  static  <T> void copyProperties(T dest, T orign){
        copyProperties(dest, orign, true);
    }



    public  static <T> void copyProperties(T dest, T orign, boolean ignoreNullValue){
        if(dest == null){
            throw new IllegalArgumentException("dest can not be null");
        }
        if(orign == null){
            throw  new IllegalArgumentException("orign can not be null");
        }

        if(isSimpleClass(dest.getClass())){
            dest = orign;
        }else {
            Field[] fields = getAllFieldsIgnoreStatics(dest.getClass());
            if(fields != null && fields.length >0){
                Object value;
                for(Field f: fields){
                    f.setAccessible(true);
                    try {
                        value = f.get(orign);
                    } catch (IllegalAccessException e) {
                        value = null;
                    }
                    copyProperty(dest, f, value,ignoreNullValue);
                }
            }else {
                // todo
                dest = orign;
            }
        }

    }


    /**
     * <p> 将值 value 赋值给 dest.field , 如果ignoreNullNorEmptyValue=true， 且 value为null或空集合数据, 则dest.field值不变</p>
     * @param dest
     * @param field
     * @param value
     * @param ignoreNullNorEmptyValue
     * @param <T>
     */
    public  static <T> void copyProperty(T dest, Field field, Object value, boolean ignoreNullNorEmptyValue){
        if(dest == null) {
            throw new IllegalArgumentException("dest can not be null ");
        }
        if(field == null) {
            throw new IllegalArgumentException("field can not be null");
        }

        field.setAccessible(true);
        if(isNullOrEmpty(value)) {
            if (ignoreNullNorEmptyValue) {
                ;
            } else {
                try {
                    field.set(dest, value);
                } catch (IllegalAccessException e) {
                    ;
                }
            }
        }else {
            /** 集合对象直接修改 **/
            // todo
            if(isSimpleClass( field.getGenericType()) || isCollectionType(field.getGenericType())){
                try {
                    field.set(dest, value);
                } catch (IllegalAccessException e) { }
            }else{
                Object destValue  = null;
                try {
                    destValue= field.get(dest);
                } catch (IllegalAccessException e) {
                    if(debug) {logger.debug(e.getMessage(),e);}
                }
                if(isNullOrEmpty(destValue)){
                    try {
                        field.set(dest, value);
                    } catch (IllegalAccessException e) {
                        if(debug) {
                            logger.debug(e.getMessage(), e);
                        }
                    }
                }else {
                    //todo 添加对集合对象的处理
                    Object newDescValue= null;
                    try {
                        Type type = field.getGenericType();
                        if(type instanceof  Class) {
                            newDescValue = ((Class<?>) field.getGenericType()).newInstance();
                        } else {
                            logger.error(type + "can not be cast be Class");
                        }
                    } catch (InstantiationException e) {
                        if(debug) {
                            logger.debug(e.getMessage(),e);
                        }
                    } catch (IllegalAccessException e) {
                        if(debug) {
                            logger.debug(e.getMessage(),e);
                        }
                    }
                    if(newDescValue == null) {
                        copyProperties(destValue, value, ignoreNullNorEmptyValue);
                    }else {
                        copyProperties(newDescValue, destValue, ignoreNullNorEmptyValue);
                        copyProperties(newDescValue, value, ignoreNullNorEmptyValue);
                        try {
                            field.set(dest,newDescValue);
                        } catch (IllegalAccessException e) {
                            if(debug) {
                                logger.debug(e.getMessage(), e);
                            }
                        }

                    }
                }
            }
        }
    }


    public static Class<?> getParameterizedType(Field field){
        Type fieldClass = field.getGenericType();
        if(fieldClass != null && fieldClass  instanceof ParameterizedType){
            ParameterizedType pt = (ParameterizedType) fieldClass;
            Type[] types = pt.getActualTypeArguments();
            if(types != null && types.length > 0){
                return (Class<?>) types[0];
            }
        }

        return null;
    }


    public static class ReflectException extends Exception{
        public ReflectException(String message, Throwable cause) {
            super(message, cause);
        }
    }
}
