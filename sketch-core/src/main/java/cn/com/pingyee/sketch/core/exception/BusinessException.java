package cn.com.pingyee.sketch.core.exception;

/**
 * @author: Yi Ping
 * @date: 2019/11/12 0012 17:17
 * @since: 1.2.0
 */
public class BusinessException extends ApiException {

    public BusinessException(String code) {
        super(code);
    }

    public BusinessException(String code, String... wildcards) {
        super(code, wildcards);
    }
}
