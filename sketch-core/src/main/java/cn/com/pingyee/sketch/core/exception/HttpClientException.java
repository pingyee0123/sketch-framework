package cn.com.pingyee.sketch.core.exception;

/**
 * *
 * <p> 客户端错误异常</p>
 * @author: Yi Ping
 * @create: 2018-11-22 16:16
 * @since: 1.0
 */
public class HttpClientException extends ApiException {


    public HttpClientException(String code) {
        super(code);
    }



    public HttpClientException(String code, String... wildcards) {
        super(code, wildcards);
    }



}
