package cn.com.pingyee.sketch.core.encrypt;

import org.apache.commons.lang3.StringUtils;

import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

/**
 * @author: Yi Ping
 * @date: 2019/7/12 0012 12:57
 * @since: 1.4.36
 */
public class AESEncryptor {

//    private String algorithm;E
//    private String key;
//    private String ivParameter = "0392039203920300";


    /**
     * <p> 负责完成加密， 解密工作</p>
     */
    private Cipher enCipher;

    private Cipher deCipher;

    /** 默认 AES 加密 **/
    public AESEncryptor(String key) throws InvalidKeyException, NoSuchAlgorithmException, InvalidAlgorithmParameterException, NoSuchPaddingException, UnsupportedEncodingException {
        this("AES", key, null);
    }


    private AESEncryptor(String algorithm, String key) throws InvalidKeyException, NoSuchAlgorithmException, InvalidAlgorithmParameterException, NoSuchPaddingException, UnsupportedEncodingException {
        this(algorithm, key, null);
    }


    private AESEncryptor(String algorithm, String key, String ivParameter) throws UnsupportedEncodingException, NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, InvalidAlgorithmParameterException {

        if(StringUtils.isBlank(algorithm)){
            throw new IllegalArgumentException("algorithm can not be null");
        }
        if(StringUtils.isBlank(key) || key.length() != 16){
            throw new IllegalArgumentException("invalid parameter key , key can not be null and  key length must be 16");
        }
        if(StringUtils.isNotBlank(ivParameter) && ivParameter.length() != 16){
            throw new IllegalArgumentException("ivalid parameter ivParameter, ivParameter length must be 16");
        }
        final String MODE = algorithm+ (StringUtils.isBlank(ivParameter)? "/ECB/PKCS5Padding": "/CBC/PKCS5Padding");

//        this.algorithm = algorithm;
//        this.key = key;
//        this.ivParameter = ivParameter;
        final SecretKey secretKey = new SecretKeySpec(key.getBytes(),algorithm);
        IvParameterSpec iv = StringUtils.isBlank(ivParameter)?null:new IvParameterSpec(ivParameter.getBytes("UTF-8"));
        enCipher = Cipher.getInstance(MODE);
        enCipher.init(Cipher.ENCRYPT_MODE, secretKey,iv);

        deCipher = Cipher.getInstance(MODE);
        deCipher.init(Cipher.DECRYPT_MODE, secretKey,iv);

    }



    public byte[] encrypt(byte[] bytes) throws BadPaddingException, IllegalBlockSizeException {
        return  enCipher.doFinal(bytes);
    }


    public byte[] decrypt(byte[] buff) throws  BadPaddingException, IllegalBlockSizeException {
        return  deCipher.doFinal(buff);
    }




}
