package cn.com.pingyee.sketch.core.service;

import cn.com.pingyee.sketch.core.util.Assert;
import lombok.Getter;
import org.apache.commons.lang3.time.DateUtils;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author: YiPing
 * @create: 2018-08-02 20:35
 * @since: 1.0
 */
public interface SmsService {

    /**
     * <p>发送短信验证码， 发送成功返回true, 失败返回false</p>
     * @param mobile 手机号码
     * @param content 发送的内容， 如果内容为空， 则发送默认的模板信息 "您的验证码是：[code]。请不要把验证码泄露给其他人。"
     * @return
     * @deprecated  since 1.2
     */
    @Deprecated
    boolean send(String mobile, String content) throws  Exception;

    /**
     * <p> 发送短信消息</p>
     * @param mobile 接收的手机号码
     * @param params 发送的内容参数， 用于替换模板消息中的参数
     * @throws Exception
     */
    void sendMessage(String mobile, Map params) throws  Exception;

    /**
     * <p> 验证短信码 </p>
     * @param mobile
     * @param code
     * @throws Exception
     */
    void validate(String mobile, String code) throws  Exception;


    /**
     * @author: Yi Ping
     * @date: 2019/9/26 0026 10:29
     * @since: 1.0.0
     */

    @Getter
    class ValidationCode {

        private String code;
        private Date expiredIn;

        public ValidationCode(String code, Date expiredIn) {
            Assert.isNotBlank(code, "code can not be blank");
            Assert.isNotNull(expiredIn, "expiredIn can not be null");

            this.code = code;
            this.expiredIn = expiredIn;
        }

        public ValidationCode(String code, int expiredInSeconds ) {
            this(code, DateUtils.addSeconds(new Date(), expiredInSeconds));
        }

        public  boolean expired(){
            return  expiredIn.before(new Date());
        }
    }
}
