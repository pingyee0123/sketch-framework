package cn.com.pingyee.sketch.core.request;

/**
 * @author: Yi Ping
 * @date: 2019/10/10 0010 22:29
 * @since: 1.0.0
 */
public interface PageRequest extends ApiRequest {

    /**
     * <p> 页码 </p>
     * @return
     */
    default int getPage(){
        return 0;
    }

    /**
     * <p> 每页显示多杀条 </p>
     * @return
     */
    default int getSize(){
        return 10;
    }

    /**
     * <p> 排序 </p>
     * @return
     */
    default String getSort(){
        return null;
    }
}
