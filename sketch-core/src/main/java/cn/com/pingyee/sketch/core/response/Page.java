package cn.com.pingyee.sketch.core.response;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * @author Yi Ping
 * @date 2018/6/25 10:55
 * @since 1.0
 */
@ApiModel("分页")
@Data
public class Page<T>  implements ApiResponse {

    @ApiModelProperty("返回数据")
    private List<T> content = new ArrayList<>();

    @ApiModelProperty("总页数")
    private int totalPages;

    @ApiModelProperty("总记录数")
    private  int totalElements;

    @ApiModelProperty("当前页码")
    private  int number;

    @ApiModelProperty("每页返回记录数")
    private  int size;

    @ApiModelProperty("当前页是否为首页")
    private  boolean first;

    @ApiModelProperty("当前页是否为最后一页")
    private  boolean last;

    @ApiModelProperty("当前页返回的记录数")
    private int numberOfElements;

    public Page() {
        super();
    }

    public Page(@NotNull List<T> content, int page, int size){
        super();
        Objects.requireNonNull(content, "content can not be null");
        int p = page<0?0:page;
        int s = size<1?1:size;

        this.totalPages = (int) Math.ceil(content.size()/(double) s);
        this.totalElements = content.size();
        this.number = p >=totalPages?(totalPages-1):p;
        this.size = s;
        this.first = this.number ==0;
        this.last = this.number == (this.totalPages -1);
        this.content = content.subList(this.number * this.size,Math.min((this.number + 1) *this.size, content.size()));
        this.numberOfElements= this.content.size();
    }
}
