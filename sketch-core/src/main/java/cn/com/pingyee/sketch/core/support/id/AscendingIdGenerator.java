package cn.com.pingyee.sketch.core.support.id;

/**
 * 递增Id 生成器
 * @author Yi Ping
 * @version 0.0.1
 * @description
 * @date 2020/12/21 21:06
 */
public interface AscendingIdGenerator extends IdGenerator<Long> {

}
