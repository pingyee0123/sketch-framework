package cn.com.pingyee.sketch.core.util;

import cn.com.pingyee.sketch.core.exception.BusinessException;
import org.apache.commons.lang3.StringUtils;

import java.util.Objects;

/**
 * @author: Yi Ping
 * @date: 2019/7/18 0018 17:22
 * @since: 1.4.44
 */
public abstract class Assert {

    public static void isNotBlank(String s, String message){
        if(StringUtils.isBlank(s)){
            throw new IllegalArgumentException(message);
        }
    }

    public static void isNotNull(Object o, String message){
        Objects.requireNonNull(o, message);
    }

    public static void isTrue(boolean express, String message){
        if(!express){
            throw new IllegalArgumentException(message);
        }
    }

    public static void isTrue(boolean expression, String code, String... args){
        if(!expression){
            throw new BusinessException(code, args);
        }
    }

    /**
     * added since 2.1
     * @param expression
     * @param message
     */
    public static void isFalse(boolean expression, String message){
        Assert.isTrue(!expression, message);
    }

    public static void isFalse(boolean expression, String code, String... args){
        Assert.isTrue(!expression, code, args);
    }


}
