package cn.com.pingyee.sketch.core.request;


import java.io.Serializable;

/**
 * <p> 申明一个 ApiRequest</p>
 * @author  Yi Ping
 * @date  2020-05-09
 */
public interface ApiRequest extends Serializable {

}