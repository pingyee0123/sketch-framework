package cn.com.pingyee.sketch.core.batchoperation;

import lombok.Data;

import java.io.Serializable;

/**
 * @author Yi Ping
 * @version 2.1
 * @date 2020/8/15 22:00
 */
@Data
public
class FailCause<K extends Serializable> implements SingleOperatee<K> {
    K key;
    String message;

    /**
     * for 序列化
     **/
    public FailCause() {
    }

    public FailCause(K key, String message) {
        this.key = key;
        this.message = message;
    }

}
