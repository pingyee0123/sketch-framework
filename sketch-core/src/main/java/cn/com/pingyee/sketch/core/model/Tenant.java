package cn.com.pingyee.sketch.core.model;

import java.io.Serializable;

/**
 * <p> 租户</p>
 * <p> 多租户系统中的概念 </p>
 * @author Yi Ping
 * @version 1.0.0-SNAPSHOT
 * @date 2020/5/13 10:40
 */
public interface Tenant<ID extends Serializable> {

    ID getId();
}
