package cn.com.pingyee.sketch.core.exception.spec;


import cn.com.pingyee.sketch.core.exception.ResourceNotFoundException;

import java.io.Serializable;

/**
 * @author: YiPing
 * @create: 2018-08-07 10:07
 * @since: 1.0
 */
public class EntityNotExistException extends ResourceNotFoundException {


    private  Class<?> clazz;
    private Serializable id;


    public EntityNotExistException(Class clazz, Serializable  id){
        super("100005", clazz.getName(), String.valueOf(id));
        this.clazz = clazz;
        this.id = id;
    }


    public EntityNotExistException(String entityClassName, Serializable id){
        super("100005", entityClassName, String.valueOf(id));
        this.id = id;
    }

}
