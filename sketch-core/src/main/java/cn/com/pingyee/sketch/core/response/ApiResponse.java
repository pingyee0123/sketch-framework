package cn.com.pingyee.sketch.core.response;


import cn.com.pingyee.sketch.core.exception.ApiException;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.text.MessageFormat;

//@JsonInclude(value= Include.NON_NULL)
//保证序列化json的时候,如果是null的对象,key也会消失

/**
 * @author  YiPing
 * @date 2019-10-20
 *
 */
@ApiModel(description = "api返回的标准格式json")
public interface ApiResponse extends Serializable {

    ApiResponse SUCCESS = new DefaultApiResponse();


    /**
     * 返回码, 0表示成功, 其他表示失败
     * @return
     */
    @ApiModelProperty("返回码, 0表示成功, 其他表示失败")
    default String getCode(){
        return SUCCESS.getCode();
    }

    /**
     * 消息提示
     * @return
     */
    @ApiModelProperty("消息提示")
    default String getMessage(){
        return SUCCESS.getMessage();
    };

    /**
     * 是否成功
     * @return
     */
    @ApiModelProperty(value = "是否成功")
    default boolean isSuccess(){
        return ErrorCodes.SUCCESS.equals(getCode());
    }

    /**
     * <p> 接口成功返回 </p>
     * @return
     */
    static ApiResponse success(){
        return SUCCESS;
    }

    /**
     * <p> 返回错误信息 </p>
     * @param code
     * @return
     * @throws ErrorCodeNotDefinedException
     */
     static ApiResponse error(String code) throws ErrorCodeNotDefinedException {
        return ApiResponse.error(code, null);
    }

    /**
     * <p> 返回错误信息 </p>
     * @param code
     * @param wildcards
     * @return
     * @throws ErrorCodeNotDefinedException
     */
    static ApiResponse error(String code, String... wildcards) throws ErrorCodeNotDefinedException {
        String messageTemplate = messageTemplate(code);
        if(null == messageTemplate){
            throw new ErrorCodeNotDefinedException(code);
        }
        return  new DefaultApiResponse(code, MessageFormat.format(messageTemplate, wildcards));
    }

    static String generateMessage(String code, String... wildcards){
        String messageTemplate = messageTemplate(code);
        if(null == messageTemplate){
            throw new ErrorCodeNotDefinedException(code);
        }
        return MessageFormat.format(messageTemplate, wildcards);
    }

    /**
     * <p> 错误码的模板消息 </p>
     * @param code
     * @return
     */
     static String messageTemplate(String code) {
        return ErrorCodes.messageTemplate(code);
    }


    /**
     * <p> 根据异常信息返回错误给前端 </p>
     * @param e
     * @return
     * @throws ErrorCodeNotDefinedException
     */
    static ApiResponse exception(ApiException e) throws ErrorCodeNotDefinedException {
       return new DefaultApiResponse(e.getErrorCode(),e.getMessage());
    }

    /**
     * <p> 根据异常信息返回错误给前端 </p>
     * @param e
     * @return
     * @throws ErrorCodeNotDefinedException
     */
    static ApiResponse exception(Throwable e) throws ErrorCodeNotDefinedException {
        if(e instanceof  ApiException){
            return exception((ApiException) e);
        }
        return  error(ErrorCodes.DEFAULT_SERVER_ERROR, e.getMessage());
    }


    @Data
    class DefaultApiResponse implements ApiResponse , Serializable{

        private String code;
        private String message;

        public DefaultApiResponse() {
            this(ErrorCodes.SUCCESS);
        }

        public DefaultApiResponse(String code) {
            this(code, (ErrorCodes.errorCode(code) != null)? messageTemplate(code) : null);
        }

        public DefaultApiResponse(String code, String message) {
            this.code = code;
            this.message = message;
        }
    }


    /**
     * @author: Yi Ping
     * @date: 2019/10/21 0021 9:24
     * @since: 1.0.0
     */
     class ErrorCodeNotDefinedException extends RuntimeException{

        public ErrorCodeNotDefinedException(String code) {
            super("error code " + code + " not defined ");
        }
    }
}
