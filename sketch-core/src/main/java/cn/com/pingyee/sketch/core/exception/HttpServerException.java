package cn.com.pingyee.sketch.core.exception;

/**
 * *
 * <p> 服务端错误异常 </p>
 * @author: Yi Ping
 * @create: 2018-11-22 16:16
 * @since: 1.0
 */
public class HttpServerException extends ApiException {





    public HttpServerException(String code) {
        super(code);
    }



    public HttpServerException(String code, String... wildcards) {
        super(code, wildcards);
    }


}
