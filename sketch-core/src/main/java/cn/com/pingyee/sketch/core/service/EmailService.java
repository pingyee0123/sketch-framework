package cn.com.pingyee.sketch.core.service;

/**
 * @author: Yi Ping
 * @date: 2019/9/16 0016 12:08
 * @since: 1.0.0
 */
public interface EmailService {

    void send(String toEmailAccount, String subject, String content) throws Exception;
}
