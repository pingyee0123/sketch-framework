package cn.com.pingyee.sketch.core.support;

import cn.com.pingyee.sketch.core.support.id.IdGenerator;
import org.apache.commons.lang3.time.DateFormatUtils;

import java.util.Date;

/**
 *
 */
public class DateTimeIdGenerator implements IdGenerator<String> {

    private String format;
    private Date date;

    public DateTimeIdGenerator(String format, Date date) {
        this.format = format;
        this.date = date;
    }

    public DateTimeIdGenerator(String format) {
        this(format, new Date());
    }

    @Override
    public String generateId() {
         return DateFormatUtils.format(date, format);
    }
}
