package cn.com.pingyee.sketch.core.service;

import lombok.Data;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * @author: YiPing
 * @create: 2018-08-02 19:39
 * @since: 1.0
 */
public interface FileService {


    /**
     * <p> 上传文件 </p>
     * @param file
     * @return
     */
    default FileInfo upload(File file) throws  IOException{
        return upload(file, generateKey(file.getName()));
    }

    /**
     * <p> 上传文件， 指定文件名</p>
     */
    default FileInfo upload(File file, String fileName) throws  IOException{
        return  this.upload(new FileInputStream(file), fileName);
    }

    /**
     * <p> 上传输入流 </p>
     * @param inputStream
     * @return
     */
    default FileInfo upload(InputStream inputStream) throws  IOException{
        return upload(inputStream, generateKey());
    }

    /**
     * <p> 指定文件名上传输入流 </p>
     * @param inputStream
     * @param filename
     * @return
     * @throws IOException
     */
    FileInfo upload(InputStream inputStream, String filename) throws  IOException;


    /**
     * <p>获取文件的访问路径 </p>
     * @param key
     * @return
     */
    String url(String key);


    /**
     * <p> 生成保存文件的key </p>
     * @return
     */
    default String generateKey(){
        return System.currentTimeMillis() + RandomStringUtils.randomNumeric(6);
    }

    default String generateKey(String filename){
        return generateKey() + getSuffix(filename);
    }


    default String getSuffix(String filename){
        if(StringUtils.isNotBlank(filename)){
            int i = filename.lastIndexOf('.');
            if(i != -1){
                return filename.substring(i);
            }
        }

        return "";
    }



    /**
     * @author: YiPing
     * @create: 2018-08-03 10:06
     * @since: 1.0
     */
    @Data
    class FileInfo {

        private  String name;
        private  String url;

        public FileInfo(String name, String url) {
            this.name = name;
            this.url = url;
        }

    }
}
