package cn.com.pingyee.sketch.core.model;

import cn.com.pingyee.sketch.core.util.Assert;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author: Yi Ping
 * @create: 2019-03-30 20:10
 * @since: 1.0.0
 */

public interface Tree {

    /**
     * <p> 获取其父节点标识, 如pid</p>
     * <p> 返回标识的好处是, 避免树太深， 导致json串很长</p>
     * @return
     */
    Object getPid();

    /**
     * <p> 修改其父节点 </p>
     * @param pid
     * @return
     */
    void setPid(Object pid);

    /**
     *
     * @return 返回树的标识， 返回值不能为null
     */
    @NotNull  Object getId();

    /**
     * <p> 是否为根节点 </p>
     * @return
     */
    default boolean isRoot(){
       return null == getPid();
    }

    /**
     * <P> 获取其孩子元素， 不能返回null， 如果为空返回一个空集合 </P>
     * @return
     */
    @NotNull   Set<Tree> getChildren();

    /**
     * <p> 添加子树</p>
     * @param  tree 字数
     */
    default void  addChild(Tree tree){
        Assert.isNotNull(tree, "tree can not be null");

        tree.setPid(this.getId());
        getChildren().add(tree);
    }

    /**
     * <p> 获取家族成员 </p>
     * @return
     */
    default  Set<Tree> fimily(){
        Set<Tree> set = new HashSet<>();
        set.add(this);
        set.addAll(getChildren());
        getChildren().forEach(t->set.addAll(t.fimily()));
        return  set;
    }


    /**
     * <p> 将trees 组织成树并返回根节点 </p>
     * @param trees
     * @return
     */
    static  Set<Tree> organizeAndGetRoots(@NotEmpty Set<Tree> trees){
        Map<Object, Tree> map = trees.stream().collect(Collectors.toMap(Tree::getId, t -> t));
        map.forEach((k,v)->{
            if(!v.isRoot() && null != map.get(v.getPid())){
                map.get(v.getPid()).addChild(v);
            }
        });
        return map.values().stream().filter(Tree::isRoot).collect(Collectors.toSet());
    }
}

