package cn.com.pingyee.sketch.core.request;

import cn.com.pingyee.sketch.core.model.User;

/**
 * 用户上下文
 * @author  Yi Ping
 * @date  2020-05-09
 * @since 2.0
 */
public abstract class UserContextHolder {
	
	private static ThreadLocal<User<?>> context = new ThreadLocal<User<?>>();


	public static User<?> getCurrentUser(){
		return  context.get();
	}

	public static void setCurrentUser(User<?> user){
		context.set(user);
	}

	public static void clear(){
		context.remove();
	}
	
}
