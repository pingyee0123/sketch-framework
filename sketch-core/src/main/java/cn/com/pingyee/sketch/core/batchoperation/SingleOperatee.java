package cn.com.pingyee.sketch.core.batchoperation;

import java.io.Serializable;

/**
 * @author Yi Ping
 * @version 2.1
 * @date 2020/8/15 21:46
 */
@FunctionalInterface
public interface SingleOperatee<K extends Serializable> {

    K getKey();

    static <T extends Serializable> SingleOperatee<T> key(T t){
        return  ()-> t;
    }

    class Default implements SingleOperatee<Serializable>{
        private Serializable key;

        public Default(Serializable key) {
            this.key = key;
        }

        @Override
        public Serializable getKey() {
            return null;
        }
    }
}
