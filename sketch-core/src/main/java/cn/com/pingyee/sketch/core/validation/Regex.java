package cn.com.pingyee.sketch.core.validation;

/**
 * @author: YiPing
 * @create: 2018-08-14 22:25
 * @since: 1.0
 */
public interface Regex {


    /**
     * 正则表达式：验证用户名
     */
    String USERNAME = "^[a-zA-Z]\\w{5,20}$";

    /**
     * 正则表达式：验证密码
     */
    String PASSWORD = "^[a-zA-Z0-9]{6,20}$";
    String PASSWORD_WITHDRAW = "^\\d{6}$";

    /**
     * 正则表达式：验证手机号, 电话号码，服务热线
     */
/**   String REGEX_MOBILE = "^((17[0-9])|(14[0-9])|(13[0-9])|(15[^4,\\D])|(18[0,1,5-9]))\\d{8}$"; **/
    String CELLPHONE = "(\\d{2,3}-)?\\d{11}$";
    String MOBILEPHONE = CELLPHONE;
    String TELEPHONE = "\\d{3,4}-\\d{7,8}";
    String MOBILE_OR_TELEPHONE = "((\\d{2,3}-)?\\d{11}$)||(\\d{3,4}-\\d{7,8})";
    String HOTLINE= "(^400\\d{7}$)||(^9\\d{4}$)||(^1\\d{4}$)";
    String HOTLINE_BANK = "^9\\d{4}$";
    String HOTLINE_TELECOM_OPERATOR = "^1\\d{4}$";
    String MOBILEPHONE_OR_TELEPHONE_HOTLINE = "((\\d{2,3}-)?\\d{11}$)||(\\d{3,4}-\\d{7,8})||(^400\\d{7}$)||(^9\\d{4}$)||(^1\\d{4}$)";
    /**
     * 正则表达式：验证邮箱
     */
    String EMAIL = "^([a-z0-9A-Z]+[-|\\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-zA-Z]{2,}$";
    /**
     * 正则表达式：验证汉字
     */
    String CHINESE="^[\u4e00-\u9fa5],{0,}$";
    /**
     * 正则表达式：验证身份证
     */
    String ID_CARD = "(^\\d{17}([0-9]|[xX])$)|(^\\d{15}$)";

    /**
     * 正则表达式：验证URL
     */

    String URL = "http(s)?://([\\w-]+\\.)+[\\w-]+(/[\\w-\\./?%&=]*)?";
    /**
     * 正则表达式：验证IP地址
     */
    String IP_ADDR = "(25[0-5]|2[0-4]\\d|[0-1]\\d{2}|[1-9]?\\d)";
    /**
     * 验证码
     */
    String VALIDATE_CODE = "\\d{4,6}";
    /**
     * PC_ID
     */

    String PC_ID = "\\w{8,100}";

    /** 姓名 **/

    String NAME = "(\\w{2,20})||(^[\\u4e00-\\u9fa5]{2,10}$)";
    /**
     * 微信OpenId
     */
    String WECHAT_OPEN_ID =  "\\w{28}";


}