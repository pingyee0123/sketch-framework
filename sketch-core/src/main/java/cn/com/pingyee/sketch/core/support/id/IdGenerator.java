package cn.com.pingyee.sketch.core.support.id;

import java.io.Serializable;

/**
 * @author Yi Ping
 * @version 2.1
 * @date 2020/7/30 16:27
 */
public interface IdGenerator<T extends Serializable> {

    T generateId();
}
