package cn.com.pingyee.sketch.core.service;

import cn.com.pingyee.sketch.core.util.FileCopyUtils;

import java.io.*;

/**
 * @author: Yi Ping
 * @date: 2019/9/15 0015 9:39
 * @since: 0.0.1-SNAPSHOT
 */


public class LocalFileService implements FileService {

    private final String savePath;
    private final String urlPrefix;

    public LocalFileService(String savePath, String urlPrefix) {
        this.savePath = savePath;
        this.urlPrefix = urlPrefix;
    }


    @Override
    public FileInfo upload(InputStream inputStream, String filename) throws IOException {
        String fn = generateKey(filename);
        File file = new File(this.savePath);
        if(!file.exists()){
            file.mkdirs();
        }
        FileOutputStream fos = new FileOutputStream(this.savePath + fn);
        FileCopyUtils.copy(inputStream, fos);
        return new FileInfo(fn,url(fn));
    }


    @Override
    public String url(String key) {
        return this.urlPrefix + key;
    }


    public InputStream getFile(String filename) throws FileNotFoundException {
        return  new FileInputStream(this.savePath + filename);
    }

}
