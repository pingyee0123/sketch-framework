package cn.com.pingyee.sketch.core.batchoperation;

import cn.com.pingyee.sketch.core.response.ApiResponse;
import cn.com.pingyee.sketch.core.util.Assert;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

/**
 * @author Yi Ping
 * @version 2.1
 * @date 2020/8/15 21:48
 */
public interface BatchOperationApiResponse extends ApiResponse {

    long getTotalCount();
    long getSuccessCount();
    long getFailCount();
    List<? extends  FailCause<? extends Serializable>> getFailCauses();


    static <K extends Serializable> BatchOperationApiResponse newBatchOperationApiResponse(
            List<? extends SingleOperatee<K>> list,
            List<FailCause<K>> failCauses){
        Assert.isNotNull(list, "list can not be null");
        Assert.isTrue((failCauses==null? 0:failCauses.size() )<= list.size(), "failCause size should be smaller than list");

        return  new BatchOperationApiResponse() {
            @Override
            public long getTotalCount() {
                return list.size();
            }

            @Override
            public long getSuccessCount() {
                return getTotalCount() - getFailCount();
            }

            @Override
            public long getFailCount() {
                return Optional.ofNullable(getFailCauses()).map(s-> s.size()).orElse(0);
            }

            @Override
            public List<? extends FailCause<? extends Serializable>> getFailCauses() {
                return failCauses;
            }
        };
    }

}
