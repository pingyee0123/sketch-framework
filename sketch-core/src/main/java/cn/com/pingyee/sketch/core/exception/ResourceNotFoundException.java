package cn.com.pingyee.sketch.core.exception;

/**
 *
 * <p> 资源为找到异常， 对应http 404 错误</p>
 * @author: Yi Ping
 * @create: 2018-11-28 17:28
 * @since: 1.0.0
 */
public class ResourceNotFoundException extends HttpClientException {


    public ResourceNotFoundException(String code) {
        super(code);
    }


    public ResourceNotFoundException(String code, String... wildcards) {
        super(code, wildcards);
    }


}
