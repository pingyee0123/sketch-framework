package cn.com.pingyee.sketch.core.support.id;

import org.apache.commons.lang3.StringUtils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @author Yi Ping
 * @version 0.0.1
 * @description
 * @date 2020/12/21 21:08
 */
public class CurrentDateTimeIdGenerator implements IdGenerator<String> {

    private final DateTimeFormatter formatter;

    public CurrentDateTimeIdGenerator(String format) {
        if(StringUtils.isBlank(format)){
            throw new IllegalArgumentException("illegal date time format " + format);
        }
        this.formatter = DateTimeFormatter.ofPattern(format);
    }

    @Override
    public String generateId() {
        return formatter.format(LocalDateTime.now());
    }
}
