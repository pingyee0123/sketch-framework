package cn.com.pingyee.sketch.file.aliyunoss.starter;

import cn.com.pingyee.sketch.file.aliyunoss.AliyunOSSProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author: Yi Ping
 * @date: 2019/9/16 0016 12:21
 * @since: 1.0.0
 */

@ConfigurationProperties(prefix = "sketch.file.aliyunoss")
public class AliyunOssProperty extends AliyunOSSProperty {
}
