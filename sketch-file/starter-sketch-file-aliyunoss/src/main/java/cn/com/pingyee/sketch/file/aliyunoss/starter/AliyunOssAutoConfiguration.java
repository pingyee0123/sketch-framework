package cn.com.pingyee.sketch.file.aliyunoss.starter;

import cn.com.pingyee.sketch.file.aliyunoss.AliyunOSSManager;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author: Yi Ping
 * @create: 2019-02-23 18:10
 * @since: 1.6
 */

@Configuration
@EnableConfigurationProperties({AliyunOssProperty.class})
@ConditionalOnClass(AliyunOSSManager.class)
@ConditionalOnProperty(prefix = "sketch.file.aliyunoss", value = "enabled", matchIfMissing = true)
//@Slf4j
public class AliyunOssAutoConfiguration {


    @Bean
    @ConditionalOnMissingBean(AliyunOSSManager.class)
    public AliyunOSSManager aliyunOSSManager(AliyunOssProperty aliyunOSSProperty){
        return  new AliyunOSSManager(aliyunOSSProperty);
    }

}
