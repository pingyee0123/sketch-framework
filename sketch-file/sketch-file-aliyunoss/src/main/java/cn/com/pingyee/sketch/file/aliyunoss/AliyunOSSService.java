package cn.com.pingyee.sketch.file.aliyunoss;


import cn.com.pingyee.sketch.core.service.FileService;
import com.aliyun.oss.OSSClient;
import com.aliyun.oss.model.PutObjectResult;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.io.InputStream;

/**
 * @author: YiPing
 * @create: 2018-11-12 11:40
 * @since: 1.0
 */
public class AliyunOSSService implements FileService {

    private AliyunOSSProperty aliyunOSSProperty;
    private OSSClient ossClient;

    public AliyunOSSService(AliyunOSSProperty property) {
        this.aliyunOSSProperty = property;
        this.ossClient = new OSSClient(property.getEndpoint(), property.getAccessKeyId(), property.getAccessKeySecret());
    }

    @Override
    public FileInfo upload(File file, String filename) {
        String key = getFileKey(filename);
        PutObjectResult putObjectResult = ossClient.putObject(aliyunOSSProperty.getBucketName(), key, file);
        return new FileInfo(key, url(key));
    }


    private String getFileKey(String filename) {
        return aliyunOSSProperty.getFolder()  + filename;
    }

    @Override
    public FileInfo upload(InputStream inputStream, String filename) {
        String key = getFileKey(filename);
        PutObjectResult putObjectResult = ossClient.putObject(aliyunOSSProperty.getBucketName(), key, inputStream);
        return new FileInfo(key, url(key));
    }


    @Override
    public String url(String fileKey) {
        return StringUtils.isNotBlank(aliyunOSSProperty.getCdn())? getCdnUrl(fileKey): getBucketUrl(fileKey);
    }

    private String getCdnUrl(String fileKey){
        return aliyunOSSProperty.getCdn() + "/" + fileKey;
    }

    private String getBucketUrl(String fileKey){
        return "http://" + aliyunOSSProperty.getBucketName() + "." + aliyunOSSProperty.getEndpoint() + "/" +  fileKey;
    }

}

