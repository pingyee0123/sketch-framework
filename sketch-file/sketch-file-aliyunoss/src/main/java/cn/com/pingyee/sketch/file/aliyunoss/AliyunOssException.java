package cn.com.pingyee.sketch.file.aliyunoss;

/**
 * @author: Yi Ping
 * @create: 2019-02-23 18:07
 * @since: 1.6
 */
public class AliyunOssException  extends Exception{

    public AliyunOssException() {
    }

    public AliyunOssException(String message) {
        super(message);
    }

    public AliyunOssException(String message, Throwable cause) {
        super(message, cause);
    }

    public AliyunOssException(Throwable cause) {
        super(cause);
    }

    public AliyunOssException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
