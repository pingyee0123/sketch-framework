package cn.com.pingyee.sketch.file.aliyunoss;

import lombok.Data;

/**
 * @author: Yi Ping
 * @date: 2019/9/16 0016 12:38
 * @since: 1.0.0
 */
@Data
public class UploadPolicy {
    private String accessid;
    private String policy;
    private String signature;
    private String dir;
    private String host;
    private String expire;
    private String callbackUrl;
    private String callbackBody;
    private String callback;
}
