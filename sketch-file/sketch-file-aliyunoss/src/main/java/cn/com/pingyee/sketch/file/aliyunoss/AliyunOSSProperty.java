package cn.com.pingyee.sketch.file.aliyunoss;

import lombok.Data;

/**
 * @author: YiPing
 * @create: 2018-11-12 11:33
 * @since: 1.0
 */
@Data
//@ConfigurationProperties(prefix = "file.aliyun.oss")
public class AliyunOSSProperty {

    private String endpoint;
    private String accessKeyId;
    private String accessKeySecret;
    private String bucketName;
    private String folder;
    private String cdn;

    private String callbackUrl;

}
