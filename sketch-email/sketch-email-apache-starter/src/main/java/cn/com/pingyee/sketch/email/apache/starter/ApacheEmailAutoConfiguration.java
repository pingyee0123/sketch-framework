package cn.com.pingyee.sketch.email.apache.starter;

import cn.com.pingyee.sketch.email.apache.ApacheEmailService;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author Yi Ping
 * @version 2.0
 * @date 2020/6/12 11:13
 */
@Configuration
@EnableConfigurationProperties(ApacheEmailProperty.class)
public class ApacheEmailAutoConfiguration {


    @Bean
    public ApacheEmailService apacheEmailService(ApacheEmailProperty property){
        return  new ApacheEmailService(property.getHostName(),
                property.getPort(),
                property.getAuthenticatorUserName(),
                property.getAuthenticatorPassword(),
                property.getFromEmail(),
                property.getFromName());
    }
}
