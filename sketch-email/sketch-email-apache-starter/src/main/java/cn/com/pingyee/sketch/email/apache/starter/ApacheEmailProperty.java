package cn.com.pingyee.sketch.email.apache.starter;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author Yi Ping
 * @version 2.0
 * @date 2020/6/12 11:04
 */

@ConfigurationProperties(prefix = "sketch.email.apache")
public class ApacheEmailProperty {
    /**
     * smtp 服务器
     */
    private String hostName ;
    /** 端口 **/
    private int port;
    /** 授权账号 **/
    private String authenticatorUserName ;
    /** 授权密码 **/
    private String authenticatorPassword ;
    /** 发送者邮箱 **/
    private String fromEmail;
    /** 发送这姓名 **/
    private String fromName;

    public String getHostName() {
        return hostName;
    }

    public void setHostName(String hostName) {
        this.hostName = hostName;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getAuthenticatorUserName() {
        return authenticatorUserName;
    }

    public void setAuthenticatorUserName(String authenticatorUserName) {
        this.authenticatorUserName = authenticatorUserName;
    }

    public String getAuthenticatorPassword() {
        return authenticatorPassword;
    }

    public void setAuthenticatorPassword(String authenticatorPassword) {
        this.authenticatorPassword = authenticatorPassword;
    }

    public String getFromEmail() {
        return fromEmail;
    }

    public void setFromEmail(String fromEmail) {
        this.fromEmail = fromEmail;
    }

    public String getFromName() {
        return fromName;
    }

    public void setFromName(String fromName) {
        this.fromName = fromName;
    }
}
