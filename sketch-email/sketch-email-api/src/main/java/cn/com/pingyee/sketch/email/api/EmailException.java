package cn.com.pingyee.sketch.email.api;

/**
 * @author Yi Ping
 * @version 2.0
 * @date 2020/6/12 9:12
 */
public class EmailException  extends Exception{

    public EmailException(String message) {
        super(message);
    }

    public EmailException(String message, Throwable cause) {
        super(message, cause);
    }

    public EmailException(Throwable cause) {
        super(cause);
    }

    public EmailException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
