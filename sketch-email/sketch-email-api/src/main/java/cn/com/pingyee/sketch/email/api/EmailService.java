package cn.com.pingyee.sketch.email.api;

/**
 * @author Yi Ping
 * @version 2.0
 * @date 2020/6/12 9:07
 */
public interface EmailService {

    /**
     * <p> 发送邮件  </p>
     * @param toEmailAddress   邮箱地址
     * @param subject 邮件标题
     * @param content 邮件内容
     * @throws EmailException 邮件发送失败将抛出异常
     */
    void send(String toEmailAddress, String subject, String content) throws EmailException;
}
