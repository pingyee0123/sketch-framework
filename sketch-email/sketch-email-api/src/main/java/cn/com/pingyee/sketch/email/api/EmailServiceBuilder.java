package cn.com.pingyee.sketch.email.api;

/**
 * <p> 抽象工厂模式 </p>
 * @author Yi Ping
 * @version 2.0
 * @date 2020/6/12 9:42
 */
public interface EmailServiceBuilder {


    EmailService build();
}
