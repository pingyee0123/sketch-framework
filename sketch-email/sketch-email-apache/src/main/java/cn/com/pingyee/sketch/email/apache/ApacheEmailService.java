package cn.com.pingyee.sketch.email.apache;

import cn.com.pingyee.sketch.email.api.EmailException;
import cn.com.pingyee.sketch.email.api.EmailService;
import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.Email;
import org.apache.commons.mail.SimpleEmail;

/**
 * @author Yi Ping
 * @version 2.0
 * @date 2020/6/12 9:24
 */
public class ApacheEmailService implements EmailService {

    private  String hostName = "smtp.yeah.net";
    private  int port = 465;
    private  String authenticatorUserName = "wild_ghost@yeah.net";
    private  String authenticatorPassword;
    private  String fromEmail = "wild_ghost@yeah.net";
    private  String fromName = "天津南开大学";

    /**
     * Just for test
     */
    public ApacheEmailService() {
    }

    public ApacheEmailService(String hostName, int port, String authenticatorUserName, String authenticatorPassword, String fromEmail, String fromName) {
        this.hostName = hostName;
        this.port = port;
        this.authenticatorUserName = authenticatorUserName;
        this.authenticatorPassword = authenticatorPassword;
        this.fromEmail = fromEmail;
        this.fromName = fromName;
    }


    @Override
    public void send(String toEmailAddress, String subject, String content) throws EmailException {
        Email email = new SimpleEmail();
        email.setHostName(hostName);
        email.setSmtpPort(port);
        email.setAuthenticator(new DefaultAuthenticator(authenticatorUserName, authenticatorPassword));
        email.setSSLOnConnect(true);
        email.setSubject(subject);

        try {
            email.setFrom(fromEmail, fromName);
            email.setMsg(content);
            email.addTo(toEmailAddress);
        } catch (org.apache.commons.mail.EmailException e) {
            throw new EmailException("email sender info error: " + e.getMessage(), e);
        }

        try {
            email.send();
        } catch (org.apache.commons.mail.EmailException e) {
            throw new EmailException("send email failure: "  + e.getMessage(), e);
        }
    }
}
