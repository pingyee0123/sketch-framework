package cn.com.pingyee.sketch.email.apache.test;

import cn.com.pingyee.sketch.email.apache.ApacheEmailService;
import cn.com.pingyee.sketch.email.api.EmailException;
import cn.com.pingyee.sketch.email.api.EmailService;

/**
 * @author Yi Ping
 * @version 2.0
 * @date 2020/6/12 10:00
 */

public class ApacheEmailServiceTest {

    public static void main(String[] args){
        EmailService emailService = new ApacheEmailService();
        try {
            emailService.send("635162839@qq.com", "test", "test content");
        } catch (EmailException e) {
            e.printStackTrace();
        }
    }
}
